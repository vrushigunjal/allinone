import 'package:flutter/material.dart';

final Color primaryColor = Colors.blue[800]; // #22CB0C, #583bb8, #421e15
final Color buttonColor = Colors.blue[800];

final Color accentColor = Colors.blue[600]; // #583bb8, #f39c12, #fc5f53

final Color tileColor = Colors.green[100];
