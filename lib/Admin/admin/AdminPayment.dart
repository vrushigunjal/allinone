import 'package:all_in_one_app/Admin/admin/Payment/ProductPayment.dart';
import 'package:all_in_one_app/Admin/admin/Payment/ServicePyment.dart';
import 'package:all_in_one_app/Admin/admin/ordertabs/ProductTab.dart';
import 'package:all_in_one_app/Admin/admin/ordertabs/ServiceTab.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';



class PaymentScreenAdmin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Orders"),
          bottom: TabBar(


            tabs: [
              Tab(
                icon: Icon(EvaIcons.shoppingCartOutline),
                text: "Service",
              ),

              Tab(
                icon: Icon(EvaIcons.carOutline),
                text: "Product",
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
           ServicePyment(),
            ProductPayment(),

          ],
        ),
      ),
    );
  }
}
