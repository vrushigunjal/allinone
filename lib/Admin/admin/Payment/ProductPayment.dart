import 'package:all_in_one_app/Admin/admin/PaymentDetailsScreenAdmin.dart';
import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/components/PaymentItem.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/Admin/helpers/order.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:all_in_one_app/Admin/admin/Payment/productPaymentItem.dart';

class ProductPayment extends StatefulWidget {
  @override
  _ProductPaymentState createState() => _ProductPaymentState();
}

class _ProductPaymentState extends State<ProductPayment> {


  bool _loading = true;
  bool _empty = true;
  List<DocumentSnapshot> orders;
  DocumentSnapshot lastDocument;

  ScrollController _scrollController;
  @override
  void initState() {
    super.initState();
    getOrder();
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more orders");
          getMoreOrder();
        }
      });
  }

  getOrder() async {
    try {
      QuerySnapshot qSnap = await getallproductPayment();
      if (qSnap.size > 0) {
        setState(() {
          _empty = false;
          orders = qSnap.docs;
          lastDocument = qSnap.docs.last;
          _loading = false;
        });
      } else {
        setState(() {
          _empty = true;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _empty = true;
        _loading = false;
      });
    }
  }

  getMoreOrder() async {
    try {
      QuerySnapshot qSnap = await getMoreproductPayment(lastDocument);
      if (qSnap.size > 0) {
        setState(() {
          _empty = false;
          orders.addAll(qSnap.docs);
          lastDocument = qSnap.docs.last;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      child: _loading == true
          ? Container(
        child: Center(
          child: SpinKitPulse(
            color: primaryColor,
            size: 50,
          ),
        ),
      )
          : _empty == true
          ? Container(
        child: Empty(
          text: "No orders!",
        ),
      )
          : RefreshIndicator(
        onRefresh: () async {
          return await getOrder();
        },
        child: ListView.builder(
          controller: _scrollController,
          itemCount: orders.length,
          itemBuilder: (ctx, i) {
            return productPaymentItem(
              orderSnapshot: orders[i],
              onTap: () async {
                /*String result = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => PaymentDetailsScreenAdmin(
                      orderSnap: orders[i],
                    ),
                  ),
                );
                if (result == "done") {
                  await getOrder();
                }*/
              },
            );
          },
        ),
      ),
    );
  }
}
