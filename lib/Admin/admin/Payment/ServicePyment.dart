
import 'package:all_in_one_app/Admin/admin/Payment/WeeklyPayment.dart';
import 'package:all_in_one_app/Admin/admin/PaymentDetailsScreenAdmin.dart';
import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/components/PaymentItem.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/User/helper/Payment.dart';
import 'package:all_in_one_app/carWasher/helper/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ServicePyment extends StatefulWidget {
  @override
  _ServicePymentState createState() => _ServicePymentState();
}

class _ServicePymentState extends State<ServicePyment> {

  bool _loading = true;
  bool _empty = true;
  String id = "";
  List<DocumentSnapshot> orders;
  DocumentSnapshot lastDocument;
  ScrollController _scrollController;
  @override
  void initState() {
    super.initState();
    getOrder();
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more orders");
          getMoreOrder();
        }
      });
  }

  getOrder() async {
    try {
      QuerySnapshot qSnap = await getWasharListData();
      if (qSnap.size > 0) {
        setState(() {
          _empty = false;
          orders = qSnap.docs;
          lastDocument = qSnap.docs.last;
          _loading = false;

        });
      } else {
        setState(() {
          _empty = true;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _empty = true;
        _loading = false;
      });
    }
  }

  getMoreOrder() async {
    try {
      QuerySnapshot qSnap = await getMoreprchasePayment(lastDocument);
      if (qSnap.size > 0) {
        setState(() {
          _empty = false;
          orders.addAll(qSnap.docs);
          lastDocument = qSnap.docs.last;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      child: _loading == true
          ? Container(
        child: Center(
          child: SpinKitPulse(
            color: primaryColor,
            size: 50,
          ),
        ),
      )
          : _empty == true
          ? Container(
        child: Empty(
          text: "No orders!",
        ),
      )
          : RefreshIndicator(
        onRefresh: () async {
          return await getOrder();
        },
        child: ListView.builder(
          controller: _scrollController,
          itemCount: orders.length,
          itemBuilder: (ctx, i) {
            return PaymentItem(
              orderSnapshot: orders[i],
              onTap: () async {
id = orders[i]["uid"];
                String result = await Navigator.push(
                  context,
                    MaterialPageRoute(
                      builder: (context) => WeeklyPayment(Id:id),

                ),
                );
                if (result == "done") {
                  await getOrder();
                }
              },
            );
          },
        ),
      ),
    );
  }
}
