
import 'package:all_in_one_app/User/helper/ServiceOrderConfirm.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';



class WeeklyPayment extends StatefulWidget {
  final String Id;

  WeeklyPayment({@required this.Id});

  @override
  _WeeklyPaymentState createState() => _WeeklyPaymentState();
}

class _WeeklyPaymentState extends State<WeeklyPayment> {
  String id = "";
  double total = 0;


  bool _loading = true;
  bool _empty = true;
  List<DocumentSnapshot> orders;
  DocumentSnapshot lastDocument;
  ScrollController _scrollController;
  String WasherPayment= "Paid";

  @override
  void initState() {
    super.initState();
    id = widget.Id;
    loadMyOrders();
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more orders");
        }
      });
  }

  loadMyOrders() async {
    try {
      QuerySnapshot qSnap = await getallServiceOrderforpayment(id);
      if (qSnap.size > 0) {
        setState(() {
          orders = qSnap.docs;
          _empty = false;
          _loading = false;
        });
      } else {
        setState(() {
          _empty = true;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _empty = true;
        _loading = false;
      });
    }
  }

  /* Future getimage() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore
        .collection("ordersService")
        .where("CenterUid", isEqualTo: id)
        .get();
    qn.documents.forEach((qn) {
      id = qn.documentID;
      total = qn["Washerprice"];



    });
    return qn.documents;
  }
*/
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    List<dynamic> cart = orders;
    total =
        cart.map<double>((m) =>m["Washerprice"]).reduce((a, b) => a + b) ??'';
    print(total);
    return Scaffold(
      appBar: AppBar(
        title: Text("Weekly Payment"),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            child: Column(
              children: <Widget>[
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(

                        '$total',
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 35,
                            fontWeight: FontWeight.bold),
                      ),

                      /* Text("100Rs", style: TextStyle(color: Colors.grey, fontSize: 35, fontWeight: FontWeight.bold),),*/
                      SizedBox(
                        height: 30,
                      ),
                      FloatingActionButton.extended(
                     //   backgroundColor: darkPrimary,

                        onPressed: () {

                          updatereasonWasherPayment(id,WasherPayment);
                          Navigator.pop(context, "done");

                        },
                        icon: Icon(Icons.save),
                        label: Text(
                          "Paid",
                          style: TextStyle(color: primaryColor, fontWeight: FontWeight.w700),
                        ),
                      ),

                      SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                  /* FloatingActionButton.extended(
              onPressed: () {},
              icon: Icon(Icons.save),
              label: Text(
                '$total',
                style: TextStyle(color: primaryColor),
              ),
            ),*/
                ),
                Container(
                  height: height,
                  padding: EdgeInsets.only(top: 30.0),
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    children: cart.map((c) {
                      return Container(
                        padding: EdgeInsets.all(fixPadding),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                color: whiteColor,
                                borderRadius: BorderRadius.circular(5.0),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                    blurRadius: 1.5,
                                    spreadRadius: 1.5,
                                    color: Colors.grey,
                                  ),
                                ],
                              ),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.all(fixPadding),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                //Text('Payment', style: lightGreyStyle),
                                                Text("Order  #$id",
                                                    style: headingWasherStyle),
                                                heightSpace,
                                                Row(children: [
                                                  Text("Service Name:",
                                                      style:
                                                          headingWasherStyle),
                                                  widthSpace,
                                                  Text(c['productName'],
                                                      style: rsStyle),
                                                ]),

                                                heightSpace,
                                                heightSpace,
                                                Text(
                                                    c["updated_at"]
                                                        .toDate()
                                                        .toString(),
                                                    maxLines: 2,
                                                    style: rsStyle),
                                                heightSpace,
                                                heightSpace,

                                                Row(
                                                  children: [
                                                    Text('${c['Washerprice']}',
                                                        style: rsStyle),
                                                  ],
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
