import 'package:all_in_one_app/Admin/admin/Reports/agentTab.dart';
import 'package:all_in_one_app/Admin/admin/Reports/userTab.dart';
import 'package:all_in_one_app/Admin/admin/Reports/washerTab.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';



class Reports extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Orders"),
          bottom: TabBar(

            tabs: [
              Tab(
                icon: Icon(EvaIcons.shoppingCartOutline),
                text: "User",
              ),
              Tab(
                icon: Icon(EvaIcons.carOutline),
                text: "Washer",
              ),
              Tab(
                icon: Icon(EvaIcons.giftOutline),
                text: "Agent",
              ),

            ],
          ),
        ),
        body: TabBarView(
          children: [
            userTab(),
            washerTab(),
            agentTab(),

          ],
        ),
      ),
    );
  }
}
