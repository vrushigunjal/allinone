import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';

import 'ordertabs/CancelledTab.dart';
import 'ordertabs/DeliveredTab.dart';
import 'ordertabs/DeliveringTab.dart';
import 'ordertabs/OrderedTab.dart';

class OrdersScreenAdmin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Orders"),
          bottom: TabBar(
            isScrollable: true,
            indicatorWeight: 6,
            tabs: [
              Tab(
                icon: Icon(EvaIcons.shoppingCartOutline),
                text: "confirm",
              ),
              Tab(
                icon: Icon(EvaIcons.carOutline),
                text: "Ship",
              ),
              Tab(
                icon: Icon(EvaIcons.giftOutline),
                text: "OnTheWay",
              ),
              Tab(
                icon: Icon(EvaIcons.arrowCircleRightOutline),
                text: "Diliverd",
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            OrderedTab(),
            DeliveredTab(),
            DeliveringTab(),
            CancelledTab(),
          ],
        ),
      ),
    );
  }
}
