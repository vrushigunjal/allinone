
import 'package:all_in_one_app/Admin/admin/Reports/ActiveUser.dart';
import 'package:all_in_one_app/Admin/admin/Reports/InActiveuser.dart';
import 'package:flutter/material.dart';

class userTab extends StatefulWidget {

  @override
  _userTabState createState() => _userTabState();
}

class _userTabState extends State<userTab> {
  int _radioValue1 = 0;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:
        Center(child: Column(children: <Widget>[
        Container(
        margin: EdgeInsets.all(10),
          child: FlatButton(
            child: Text('ActiveUser', style: TextStyle(fontSize: 20.0),),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ActiveTab()));
              },
          ),),


          Container(
            margin: EdgeInsets.all(10),
            child: FlatButton(
              child: Text('InActiveUser', style: TextStyle(fontSize: 20.0),),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => InActiveTab()));
              },
            ),),
        ],),),);
  }
}
