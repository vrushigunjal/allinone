import 'dart:async';

import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ActiveTab extends StatefulWidget {
  @override
  _ActiveTabState createState() => _ActiveTabState();
}

class _ActiveTabState extends State<ActiveTab> {
  String isActive = "";
  String phone = "";

  var id = '123';
  var uid = '123';

  StreamSubscription<DocumentSnapshot> subscription;
  final DocumentReference documentReference =
  Firestore.instance.document("ordersService/uid");


  @override
  void  initState()  {
    super.initState();
    doesNameAlreadyExist();
   // checkIfLikedOrNotnlt();

  }

      Future checkIfLikedOrNot() async {
        var firestore = Firestore.instance;
        QuerySnapshot qn = await firestore.collection("usersRegistration")
            .get();
        qn.documents.forEach((qn) {
          setState(() {
            id = qn.documentID;
            phone = qn["PhoneNumber"];
            print(id);

          });
        });
        return qn.documents;
      }

  Future checkIfLikedOrNotnlt() async {

    QuerySnapshot qn = await Firestore.instance.collection("ordersService")
        .where("uid",isEqualTo: id)
        .getDocuments();

    final List < DocumentSnapshot > documents = qn.documents;
    print(documents);

    if (documents.length > 0) {

      isActive = "Active";

        }else{

            isActive = "cccccccccccccActive";

        }
  }
doesNameAlreadyExist() async {
  final CollectionReference dbIDS = Firestore.instance.collection(
      'ordersService');

  QuerySnapshot _query = await dbIDS
      .where('uid', isEqualTo: "7gMaEf9Jj4VG8iiH69I8F3zg4HK2")
      .getDocuments();


  if ( _query.documents.length > 0) {
    // the ID exists

   isActive = "Active";
  } else if (_query.documents.length = null ) {
    isActive = "NotActive";
    print("No");
  }
  }


 /* checkIfLiked() async {
    final QuerySnapshot result =
    await Firestore.instance.collection('ordersService').getDocuments().then((doc)async{id = doc["uid"];});

    if (id = result["uid"]) {
        setState(() {
          isActive = "Active";
        });
      } else {
        setState(() {
          isActive = "NotActive";
        });
      }

  }
*/
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('ActiveUser'),
        ),
        body: Container(
          child: FutureBuilder(
              future: checkIfLikedOrNot(),
              builder: (_, snapshot) {
                return ListView.builder(
                  itemCount: snapshot.data.length,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    final item = snapshot.data[index];
                    return Container(
                        decoration: BoxDecoration(
                          color: whiteColor,
                          borderRadius: BorderRadius.circular(5.0),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              blurRadius: 1.5,
                              spreadRadius: 1.5,
                              color: Colors.grey,
                            ),
                          ],
                        ),
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(fixPadding),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text("Name:",
                                              style: headingWasherStyle),
                                          widthSpace,
                                          Text(item['uid'], style: rsStyle),
                                        ],
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text("adreess:",
                                                  style: headingWasherStyle),
                                              widthSpace,
                                              Text(item['PhoneNumber'],
                                                  style: rsStyle),
                                            ],
                                          ),
                                        ],
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text("Phone:",
                                                  style: headingWasherStyle),
                                              widthSpace,
                                              Text(item['adreess'],
                                                  style: rsStyle),
                                            ],
                                          ),
                                        ],
                                      ),

                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text("IsActive:",
                                                  style: headingWasherStyle),
                                              widthSpace,
                                              Text(isActive, style: rsStyle),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ));
                  },
                );
              }),
        ),
      ),
    );
  }
}
