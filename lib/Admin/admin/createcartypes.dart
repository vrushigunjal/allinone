import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/helpers/car.dart';
import 'package:all_in_one_app/Admin/helpers/category.dart';
import 'package:flutter/material.dart';


class CreateCarTypes extends StatefulWidget {
  @override
  _CreateCarTypesScreenState createState() => _CreateCarTypesScreenState();
}

class _CreateCarTypesScreenState extends State<CreateCarTypes> {
  final TextEditingController _titleController = TextEditingController();
  @override
  void dispose() {
    _titleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Types Of Car"),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                TextField(
                  controller: _titleController,
                  decoration: InputDecoration(
                    filled: true,
                    labelText: "Title",
                  ),
                  maxLength: 60,
                ),
                SizedBox(
                  height: 20,
                ),
                RaisedButton(
                  onPressed: () {
                    if (_titleController.text.length > 0) {
                      createCarType(_titleController.text).then((v) {
                        Navigator.pop(context, "added");
                      }).catchError((e) => print(e));
                    } else {
                      showMyDialog(
                        context: context,
                        title: "oops",
                        description: "Provide Car title",
                      );
                    }
                  },
                  child: Text("ADD"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
