import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/Admin/config/currency.dart';
import 'package:all_in_one_app/Admin/helpers/category.dart';
import 'package:all_in_one_app/Admin/helpers/coupans.dart';
import 'package:all_in_one_app/Admin/helpers/product.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class CoupanUpdateScreen extends StatefulWidget {
  final DocumentSnapshot document;

  CoupanUpdateScreen({@required this.document});

  @override
  _CoupanUpdateScreenState createState() => _CoupanUpdateScreenState();
}

class _CoupanUpdateScreenState extends State<CoupanUpdateScreen> {


  bool _loading = false;

  // product fields
  String category = '';
  TextEditingController productNameController = TextEditingController();
  TextEditingController productDescriptionController = TextEditingController();
  bool isFeatured = true;
  // product fields

  @override
  void dispose() {
    productNameController.dispose();
    productDescriptionController.dispose();
    super.dispose();
  }

  @override
  void initState() {

    productNameController.text = widget.document.data()["coupanCode"];
    productDescriptionController.text = widget.document.data()["flat"];
    super.initState();
  }


  onUpdateProductBtnTap() async {
    try {
      String coupancode = productNameController.text;
      String flat = productDescriptionController.text;

      if (coupancode.isNotEmpty && flat.isNotEmpty) {
        setState(() {
          _loading = true;
        });
        print(
            "$coupancode \n $flat");

        updateCoupan(
          widget.document.id,
          coupancode,
          flat,
        ).then((v) {
          Navigator.pop(context, "done");
        }).catchError((e) {
          Navigator.pop(context, "failed");
          print(e);
        });
      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }

  onDeleteProductBtnTap() async {
    await showDialog(
      context: context,
      builder: (ctx) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18),
          ),
          title: Text("Are you sure?"),
          content: Text(
              "Once you press delete, we will delete it from our storage, this process is irreversible."),
          actions: [
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(ctx);
              },
            ),
            FlatButton(
              textColor: Colors.red,
              child: Text("Delete Permanently"),
              onPressed: () {
                deleteCoupan(widget.document.id)
                    .then((value) {
                  Navigator.pop(ctx);
                }).catchError((e) {
                  print(e);
                  Navigator.pop(ctx);
                });
              },
            ),
          ],
        );
      },
    );
    Navigator.pop(context, "done");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Coupan"),
        actions: [
          IconButton(
            icon: Icon(Icons.delete_forever),
            onPressed: () {
              onDeleteProductBtnTap();
            },
          ),
        ],
      ),
      body: _loading == true
          ? Center(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      )
          : Container(
        padding: EdgeInsets.symmetric(
          horizontal: 12,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextField(
                decoration: InputDecoration(
                  filled: true,
                  labelText: "CoupanCode",
                ),
                maxLength: 120,
                controller: productNameController,
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                decoration: InputDecoration(
                  filled: true,
                  labelText: "Flat",
                ),
                maxLines: 1,
                controller: productDescriptionController,
                maxLength: 600,
              ),
              SizedBox(
                height: 20,
              ),



              RaisedButton(
                onPressed: () {
                  onUpdateProductBtnTap();
                },
                child: Text("SAVE"),
              ),
              SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
