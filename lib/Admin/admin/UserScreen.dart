
import 'dart:io';
import 'package:all_in_one_app/Admin/admin/UserUpdate.dart';
import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/components/UserCard.dart';
import 'package:all_in_one_app/Admin/components/UserSearch.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/User/helper/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';



class UserRegisterDetailsScreen extends StatefulWidget {
  @override
  _UserRegisterDetailsScreenState createState() => _UserRegisterDetailsScreenState();
}
enum SearchBy { email, phone, uid }
class _UserRegisterDetailsScreenState extends State<UserRegisterDetailsScreen> {

  SearchBy searchBy = SearchBy.email;
  bool _isLoading = false;
  bool _searchSuccess = false;
  QueryDocumentSnapshot snapshot;
  void search(String searchValue) {
    if (searchValue.isNotEmpty && searchValue.length > 4) {
      String key;

      if (searchBy == SearchBy.email) {
        key = "email";
      } else if (searchBy == SearchBy.phone) {
        key = "phone";
      } else if (searchBy == SearchBy.uid) {
        key = "uid";
      }

      setState(() {
        _isLoading = true;
      });
      searchUser(searchValue, key).then((qSnap) {
        if (qSnap.size == 0) {
          showMyDialog(
            context: context,
            title: "oops",
            description: "No user found!",
          );
          setState(() {
            _searchSuccess = false;
            _isLoading = false;
          });
        } else {
          setState(() {
            _searchSuccess = true;
            _isLoading = false;
            snapshot = qSnap.docs[0];
          });
        }
      }).catchError((e) {
        print(e);
        showMyDialog(
          context: context,
          title: "oops",
          description: "Something went wrong",
        );
        setState(() {
          _isLoading = false;
        });
      });
    } else {
      showMyDialog(
        context: context,
        title: "oops",
        description: "Please write search value",
      );
    }
  }

  bool _isEmpty = false;
  List<QueryDocumentSnapshot> products;
  QueryDocumentSnapshot lastDocument;

  ScrollController _scrollController;

  @override
  void initState() {
    loadProducts();
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more uploads");
          loadMoreProducts();
        }
      });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  loadProducts() async {
    setState(() {
      _isLoading = true;
    });

    QuerySnapshot qsnap = await getUserData();
    if (qsnap.size > 0) {
      setState(() {
        products = qsnap.docs;
        lastDocument = qsnap.docs.last;
        _isLoading = false;
        _isEmpty = false;
      });
    } else {
      setState(() {
        _isEmpty = true;
        _isLoading = false;
      });
    }
  }

  loadMoreProducts() async {
    QuerySnapshot qsnap = await getMoreUser(lastDocument);
    if (qsnap.size > 0) {
      setState(() {
        products.addAll(qsnap.docs);
        lastDocument = qsnap.docs.last;
        _isLoading = false;
        _isEmpty = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Search Washer"),
      ),
      body: Container(
      child: SingleChildScrollView(
      child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: SearchScreen(),
        ),
        // SearchScreen(),

  Container(
    height: 250.0,
        padding: EdgeInsets.only(top: 30.0),
        child: _isLoading == true
            ? Center(
          child: SpinKitChasingDots(
            color: primaryColor,
            size: 50,
          ),
        )
            : _isEmpty == true
            ? Empty(text: "No User found!")
            : RefreshIndicator(
          onRefresh: () async {
            return loadProducts();
          },
          child: ListView.separated(
            controller: _scrollController,
            itemCount: products.length,
            separatorBuilder: (ctx, i) {
              return Divider();
            },
            itemBuilder: (ctx, i) {
              return UserCard(
                product: products[i],
                onTap: () async {
                  String result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => UserUpdateScreen(
                        document: products[i],
                      ),
                    ),
                  );
                  if (result == "done") {
                    loadProducts();
                  }
                },
              );
            },
          ),
        ),
      ),
      ],
      ),
      ),
      ),
    );
  }
}
