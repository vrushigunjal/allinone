import 'package:all_in_one_app/Admin/admin/CoupansDetailsScreen.dart';
import 'package:all_in_one_app/Admin/admin/OffersDetailsScreen.dart';
import 'package:all_in_one_app/Admin/admin/Payment/Reports.dart';
import 'package:all_in_one_app/Admin/admin/AdminPayment.dart';
import 'package:all_in_one_app/Admin/admin/CarTypeList.dart';
import 'package:all_in_one_app/Admin/admin/UserScreen.dart';
import 'package:all_in_one_app/Admin/config/strings.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/Admin/helpers/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'UserDetailsScreen.dart';
import 'OrdersScreenAdmin.dart';
import 'CategoriesScreen.dart';
import 'ProductsScreen.dart';
import 'ShopDetails.dart';



class AdminHomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("$appName"),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              accountName: Text(FirebaseAuth.instance.currentUser.displayName),
              accountEmail: Text(FirebaseAuth.instance.currentUser.email),
              currentAccountPicture: CircleAvatar(
                backgroundImage:
                    NetworkImage(FirebaseAuth.instance.currentUser.photoURL),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: primaryColor,
              ),
            ),
         /*   ListTile(
              onTap: () {
                Navigator.pushNamed(context, "/home");
              },
              title: Text("Goto to Buyer Mode"),
              leading: Icon(EvaIcons.shoppingCartOutline),
            ),*/
            ListTile(
              onTap: () {
                signOut().then((e) {
                  Navigator.pushReplacementNamed(context, "/");
                }).catchError((e) {
                  print(e);
                });
              },
              title: Text("Logout"),
              leading: Icon(EvaIcons.logOutOutline),
            ),
          ],
        ),
      ),
      body: Container(
        child: GridView.count(
          crossAxisCount: 2,
          children: [
            Card(

              color: Colors.transparent,
              elevation: 10,
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ProductsScreen()));
                  // Function is executed on tap.
                },

                child: Container(

                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/adminproducts.png"),
                          height: 100.0,
                        width: 90.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("Products",textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            //End Product

            //Services Start
            Card(
              color: Colors.transparent,
              elevation: 10,
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ServicesScreen()));
                  // Function is executed on tap.
                },
                child: Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/adminservices.png"),
                          height: 100.0,
                        width: 90.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("Services",textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),

              ),
            ),
            //End Service
            //Start
            Card(
              color: Colors.transparent,
              elevation: 10,
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CarTypeScreen()));
                  // Function is executed on tap.
                },
                child: Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/images/two.jpg"),
                          fit: BoxFit.cover
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("CarTypes",textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),

              ),
            ),

            Card(
              color: Colors.transparent,
              elevation: 10,
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => OrdersScreenAdmin()));
                  // Function is executed on tap.
                },
                child: Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/images/two.jpg"),
                          fit: BoxFit.cover
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("Orders",textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),

              ),
            ),

/*
            Card(
              color: Colors.transparent,
              elevation: 10,
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ServiceRate()));
                  // Function is executed on tap.
                },
                child: Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/images/two.jpg"),
                          fit: BoxFit.cover
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("Service Rate",textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),

              ),
            ),*/

            Card(
              color: Colors.transparent,
              elevation: 10,
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => UserDetailsScreen()));
                  // Function is executed on tap.
                },
                child: Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/adminwashers.png"),
                          height: 100.0,
                        width: 90.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("Washers",textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),

              ),
            ),

            Card(
              color: Colors.transparent,
              elevation: 10,
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ShopDetailsScreen()));
                  // Function is executed on tap.
                },
                child: Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/adminwasherdetails.png"),
                          height: 100.0,
                        width: 90.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text('Washer Detailes',textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),

              ),
            ),

            Card(
              color: Colors.transparent,
              elevation: 10,
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => UserRegisterDetailsScreen()));
                  // Function is executed on tap.
                },
                child: Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/adminusers.png"),
                          width: 90.0,
                        height: 100.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("Users",textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),

              ),
            ),

            Card(
              color: Colors.transparent,
              elevation: 10,
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => CoupansDetailsScreen()));
                  // Function is executed on tap.
                },
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/admincoupons.png"),
                        width: 90.0,
                        height: 100.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0),
                        child: Text("Coupans",textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),

              ),
            ),

            Card(
              color: Colors.transparent,
              elevation: 10,
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => OffersDetailsScreen()));
                  // Function is executed on tap.
                },
                child: Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/adminoffers.png"),
                          height:100.0,
                        width: 90.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("Offers",textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),

              ),
            ),
            Card(
              color: Colors.transparent,
              elevation: 10,
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => PaymentScreenAdmin()));
                  // Function is executed on tap.
                },
                child: Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/adminpayment.png"),
                          height: 100.0,
                        width: 90.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("Payment",textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),

              ),
            ),

            Card(
              color: Colors.transparent,
              elevation: 10,
              child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Reports()));
                  // Function is executed on tap.
                },
                child: Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/admincoupons.png"),
                          fit: BoxFit.cover
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text("Reports",textAlign: TextAlign.center,),
                      ),
                    ],
                  ),
                ),

              ),
            ),
           /* AdminHomeGridItem(
              imageIcon:Image(image:AssetImage("assets/images/two.jpg")),

              text: "Products",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProductsScreen()));

                //     Navigator.pushNamed(context, "/manage_products");
              },
            ),
            AdminHomeGridItem(
             // imageIcon: AssetImage("assets/admincoupons.png"),
              imageIcon:  Image(
                  image: AssetImage("assets/admincoupons.png"),
                  fit: BoxFit.cover
              ),
              text: "Services",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ServicesScreen()));
            *//*    Navigator.pushNamed(context, "/categories_admin");*//*
              },

            ),*/


            /*AdminHomeGridItem(
              imageIcon: AssetImage("assets/adminsplash.jpg"),
              text: "CarTypes",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CarTypeScreen()));
                *//*    Navigator.pushNamed(context, "/categories_admin");*//*
              },
              iconColor: Colors.deepOrangeAccent,
            ),

            AdminHomeGridItem(
              imageIcon: AssetImage("assets/adminsplash.jpg"),
              text: "Orders",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => OrdersScreenAdmin()));
               *//* Navigator.pushNamed(context, "/admin_orders");*//*
              },
            ),


            AdminHomeGridItem(
              imageIcon: AssetImage("assets/adminsplash.jpg"),
              text: "Service Rate",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ServiceRate()));
                *//*    Navigator.pushNamed(context, "/categories_admin");*//*
              },
              iconColor: Colors.deepOrangeAccent,
            ),

           
            AdminHomeGridItem(
              imageIcon: AssetImage("assets/adminsplash.jpg"),
              text: "Washers",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => UserDetailsScreen()));
                *//*Navigator.pushNamed(context, "/user_details");*//*
              },
              iconColor: Colors.brown,
            ),
            AdminHomeGridItem(
              imageIcon: AssetImage("assets/adminsplash.jpg"),
              text: "Washer Detailes",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ShopDetailsScreen()));
             *//*   Navigator.pushNamed(context, "/edit_shop_details");*//*
              },
              iconColor: Colors.deepPurple,
            ),
            AdminHomeGridItem(
              imageIcon: AssetImage("assets/adminsplash.jpg"),
              text: "Users",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => UserRegisterDetailsScreen()));
                *//*   Navigator.pushNamed(context, "/edit_shop_details");*//*
              },
              iconColor: Colors.deepPurple,
            ),

            AdminHomeGridItem(
              imageIcon: AssetImage("assets/adminsplash.jpg"),
              text: "Coupans",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CoupansDetailsScreen()));
                *//*   Navigator.pushNamed(context, "/edit_shop_details");*//*
              },
              *//*iconColor: Colors.deepPurple,*//*
            ),

            AdminHomeGridItem(
              imageIcon: AssetImage("assets/adminsplash.jpg"),
              text: "Offers",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => OffersDetailsScreen()));
                *//*   Navigator.pushNamed(context, "/edit_shop_details");*//*
              },
              iconColor: Colors.deepPurple,
            ),
            AdminHomeGridItem(
              imageIcon: AssetImage("assets/adminsplash.jpg"),
              text: "Payment",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PaymentScreenAdmin()));

              },
              iconColor: Colors.deepPurple,
            ),
            AdminHomeGridItem(
              imageIcon: AssetImage("assets/adminsplash.jpg"),
              text: "Reports",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Reports()));

              },
              iconColor: Colors.deepPurple,
            ),
*/
          ],
        ),
      ),
    );
  }
}
