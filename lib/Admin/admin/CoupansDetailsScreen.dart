



import 'package:all_in_one_app/Admin/admin/CoupanUpdateScreen.dart';
import 'package:all_in_one_app/Admin/admin/CreateCoupansScreen.dart';
import 'package:all_in_one_app/Admin/components/CoupanCard.dart';
import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/Admin/helpers/coupans.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class CoupansDetailsScreen extends StatefulWidget {
  @override
  _CoupansDetailsScreenState createState() => _CoupansDetailsScreenState();
}

class _CoupansDetailsScreenState extends State<CoupansDetailsScreen> {


  bool _isLoading = false;
  bool _isEmpty = false;
  List<QueryDocumentSnapshot> products;
  QueryDocumentSnapshot lastDocument;

  ScrollController _scrollController;
  String randomnumber = "0";

  @override
  void initState() {
    loadProducts();
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more uploads");
          loadMoreProducts();
        }
      });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  loadProducts() async {
    setState(() {
      _isLoading = true;
    });

    QuerySnapshot qsnap = await getCoupan();
    if (qsnap.size > 0) {
      setState(() {
        products = qsnap.docs;
        lastDocument = qsnap.docs.last;
        _isLoading = false;
        _isEmpty = false;
      });
    } else {
      setState(() {
        _isEmpty = true;
        _isLoading = false;
      });
    }
  }

  loadMoreProducts() async {
    QuerySnapshot qsnap = await getMoreCoupan(lastDocument);
    if (qsnap.size > 0) {
      setState(() {
        products.addAll(qsnap.docs);
        lastDocument = qsnap.docs.last;
        _isLoading = false;
        _isEmpty = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        title: Text("CoupansList"),
    ),

      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async {
          try {
            String result = await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (ctx) => CreateCoupansScreen(RandomNumber:randomnumber),
                fullscreenDialog: true,
              ),
            );

            if (result == "done") {
              loadProducts();
            } else if (result == "failed") {
              showMyDialog(
                context: context,
                title: "oops",
                description: "Something went wrong",
              );
            }
          } catch (e) {
            print(e);
          }
        },
        label: Text("Create Coupan"),
        icon: Icon(Icons.add),
      ),
      body: Container(
        child: _isLoading == true
            ? Center(
          child: SpinKitChasingDots(
            color: primaryColor,
            size: 50,
          ),
        )
            : _isEmpty == true
            ? Empty(text: "Ooops! Coupan Not found!")
            : RefreshIndicator(
          onRefresh: () async {
            return loadProducts();
          },
          child: ListView.separated(
            controller: _scrollController,
            itemCount: products.length,
            separatorBuilder: (ctx, i) {
              return Divider();
            },
            itemBuilder: (ctx, i) {
              return CoupanCard(
                product: products[i],
                onTap: () async {
                  String result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CoupanUpdateScreen(
                        document: products[i],
                      ),
                    ),
                  );
                  if (result == "done") {
                    loadProducts();
                  }
                },
              );
            },
          ),
        ),
      ),

    );
  }
}
