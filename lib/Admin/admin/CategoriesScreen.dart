import 'package:all_in_one_app/Admin/admin/ServiceUpdate.dart';
import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/components/service.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/carWasher/helper/Services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'CreateCategoryScreen.dart';


class ServicesScreen extends StatefulWidget {
  @override
  _ServicesScreenState createState() => _ServicesScreenState();
}


class _ServicesScreenState extends State<ServicesScreen> {
  bool _isLoading = false;
  bool _isEmpty = false;
  List<QueryDocumentSnapshot> products;
  QueryDocumentSnapshot lastDocument;

  ScrollController _scrollController;

  @override
  void initState() {
    loadProducts();
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more uploads");
          loadMoreProducts();
        }
      });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  loadProducts() async {
    setState(() {
      _isLoading = true;
    });

    QuerySnapshot qsnap = await getCategories();
    if (qsnap.size > 0) {
      setState(() {
        products = qsnap.docs;
        lastDocument = qsnap.docs.last;
        _isLoading = false;
        _isEmpty = false;
      });
    } else {
      setState(() {
        _isEmpty = true;
        _isLoading = false;
      });
    }
  }

  loadMoreProducts() async {
    QuerySnapshot qsnap = await getMoreCategories(lastDocument);
    if (qsnap.size > 0) {
      setState(() {
        products.addAll(qsnap.docs);
        lastDocument = qsnap.docs.last;
        _isLoading = false;
        _isEmpty = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Manage Services"),
      ),

      body: Container(
        child: _isLoading == true
            ? Center(
          child: SpinKitChasingDots(
            color: primaryColor,
            size: 50,
          ),
        )
            : _isEmpty == true
            ? Empty(text: "Ooops! Services Not found!")
            : RefreshIndicator(
          onRefresh: () async {
            return loadProducts();
          },
          child: ListView.separated(
            controller: _scrollController,
            itemCount: products.length,
            separatorBuilder: (ctx, i) {
              return Divider();
            },
            itemBuilder: (ctx, i) {
              return ServiceCard(
                product: products[i],
                onTap: () async {
                  String result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>ServiceUpdateScreen(
                        document: products[i],
                      ),
                    ),
                  );
                  if (result == "done") {
                    loadProducts();
                  }
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
