import 'dart:io';

import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/Admin/helpers/offers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;

class CreateOffersScreen extends StatefulWidget {
  @override
  _CreateOffersScreenState createState() => _CreateOffersScreenState();
}

enum ImagePickCategory { camera, phone }

class _CreateOffersScreenState extends State<CreateOffersScreen> {
  File _productImage;

  List<QueryDocumentSnapshot> _categories;

  bool _loading = false;
  TextEditingController productNameController = TextEditingController();
  @override
  void dispose() {
    productNameController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }
  onCreateProductBtnTap() async {
    try {
      String productName = productNameController.text;

      if (productName.isNotEmpty != null) {
        String productImageName = path.basename(_productImage.path);
        uploadOffersImage(
            productImageName,
            _productImage,
          ).then((downloadURL) {
            addOffers(
              downloadURL,
              productName,
            ).then((v) {
              Navigator.pop(context, "done");
            });
          }).catchError((e) {
            print(e);
            Navigator.pop(context, "failed");
          });

      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }

  _pickImage() async {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10.0),
          topRight: Radius.circular(10.0),
        ),
      ),
      builder: (ctx) {
        return Container(
          height: 200,
          color: Colors.transparent,
          child: ListView(
            children: [
              ListTile(
                leading: Icon(Icons.camera_alt),
                title: Text("Take a Picture from Camera"),
                onTap: () {
                  _takeImage(ImagePickCategory.camera);

                  Navigator.pop(ctx);
                },
              ),
              ListTile(
                leading: Icon(Icons.image),
                title: Text("Select a Picture from Device"),
                onTap: () {
                  _takeImage(ImagePickCategory.phone);
                  Navigator.pop(ctx);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  _takeImage(ImagePickCategory selection) async {
    try {
      final picker = ImagePicker();

      File _tempFile;

      // pick image
      if (selection == ImagePickCategory.camera) {
        final pickedFile = await picker.getImage(source: ImageSource.camera);
        _tempFile = File(pickedFile.path);
      } else if (selection == ImagePickCategory.phone) {
        final pickedFile = await picker.getImage(source: ImageSource.gallery);
        _tempFile = File(pickedFile.path);
      }

      // crop image

      File croppedFile = await ImageCropper.cropImage(
        sourcePath: _tempFile.path,
        compressQuality: 30,
        compressFormat: ImageCompressFormat.jpg,
        maxHeight: 1920,
        maxWidth: 1920,
      );

      print(croppedFile.lengthSync());

      setState(() {
        _productImage = croppedFile;
      });
    } catch (e) {
      print(e);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        title: Text("Create Product"),
    ),
      body: _loading == true
          ? Center(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      )
          : Container(
        padding: EdgeInsets.symmetric(
          horizontal: 12,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              InkWell(
                onTap: () {
                  _pickImage();
                },
                child: _productImage == null
                    ? Container(
                  height: 280,
                  color: Colors.grey,
                  child: Icon(Icons.add_a_photo),
                )
                    : Image.file(
                  _productImage,
                  height: 280,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                height: 20,
              ),

              TextField(
                decoration: InputDecoration(
                  filled: true,
                  labelText: "Offer Name",
                ),
                maxLength: 120,
                controller: productNameController,
              ),

              SizedBox(
                height: 20,
              ),
              RaisedButton(
                onPressed: () {
                  onCreateProductBtnTap();
                },
                child: Text("CREATE PRODUCT"),
              ),
              SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
