import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/Admin/helpers/coupans.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class CreateCoupansScreen extends StatefulWidget {
  var RandomNumber;

  CreateCoupansScreen({@required this.RandomNumber});

  @override
  _CreateCoupansScreenState createState() => _CreateCoupansScreenState();
}

class _CreateCoupansScreenState extends State<CreateCoupansScreen> {
  bool _loading = false;
  String _selectedText = "SDD";

  TextEditingController coupanCodeController = TextEditingController();
  TextEditingController coupancodeflatController = TextEditingController();
  TextEditingController firstflatController = TextEditingController();

  onCreateProductBtnTap() async {
    try {
      String coupancode = coupanCodeController.text;
      String flat = coupancodeflatController.text;
      String first = firstflatController.text;

      if (coupancode.isNotEmpty && flat.isNotEmpty != null) {
        setState(() {
          _loading = true;
        });
        print("$coupancode \n $flat");

        addCoupan(
            widget.RandomNumber,
          coupancode,
          flat,
            first
        ).then((v) {
          Navigator.pop(context, "done");
        }).catchError((e) {
          print(e);
          Navigator.pop(context, "failed");
        });
      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create Coupan"),
      ),
      body: _loading == true
          ? Center(
              child: SpinKitChasingDots(
                color: primaryColor,
                size: 50,
              ),
            )
          : Container(
              padding: EdgeInsets.symmetric(
                horizontal: 20,

              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [



                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        labelText: "CoupanCode",
                      ),
                      maxLength: 120,
                      controller: coupanCodeController,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        labelText: "flat",
                      ),
                      maxLines: 1,
                      controller: coupancodeflatController,
                    ),
                    SizedBox(
                      height: 20,
                    ),

                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        labelText: "ForFirstInstallar",
                      ),
                      maxLines: 1,
                      controller: firstflatController,
                    ),
              SizedBox(
                      height: 20,
                    ),


                    RaisedButton(
                      onPressed: () {
                        print(widget.RandomNumber);
                        onCreateProductBtnTap();
                      },
                      child: Text("CREATE Coupan"),
                    ),
                    SizedBox(
                      height: 100,
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
