import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/Admin/config/currency.dart';
import 'package:all_in_one_app/Admin/helpers/car.dart';
import 'package:all_in_one_app/User/helper/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class UserUpdateScreen extends StatefulWidget {
  final DocumentSnapshot document;

  UserUpdateScreen({@required this.document});

  @override
  _UserUpdateScreenState createState() => _UserUpdateScreenState();
}

class _UserUpdateScreenState extends State<UserUpdateScreen> {
  List<QueryDocumentSnapshot> _categories;

  bool _loading = false;

  // product fields
  String cartype = '';
  TextEditingController NameController = TextEditingController();
  TextEditingController DateOfBirthController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController adreessPriceController = TextEditingController();
  TextEditingController acarNoController = TextEditingController();
  bool isFeatured = true;

  // product fields

  @override
  void dispose() {
    NameController.dispose();
    emailController.dispose();
    adreessPriceController.dispose();
    DateOfBirthController.dispose();
    acarNoController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    loadCategories();
    NameController.text = widget.document.data()["name"];
    DateOfBirthController.text = widget.document.data()["dateOfBirth"];
    emailController.text = widget.document.data()["email"].toString();
    adreessPriceController.text = widget.document.data()["adreess"].toString();
    acarNoController.text = widget.document.data()["carNo"].toString();

    super.initState();
  }

  loadCategories() async {
    try {
      setState(() {
        _loading = true;
      });

      QuerySnapshot qsnap = await getCarType();
      setState(() {
        _categories = qsnap.docs;
        cartype = widget.document.data()["TypesOfCar"];
        _loading = false;
      });
    } catch (e) {
      print(e);
    }
  }

  onUpdateProductBtnTap() async {
    try {
      String Name = NameController.text;
      String dateofBirth = DateOfBirthController.text;
      String email = emailController.text;
      String adreess = adreessPriceController.text;
      String carno = acarNoController.text;

      if (Name.isNotEmpty && dateofBirth.isNotEmpty && email.isNotEmpty) {
        setState(() {
          _loading = true;
        });
        print("$Name \n $dateofBirth \n $email $carno \n $cartype");

        updateUser(
          widget.document.id,
          Name,
          dateofBirth,
          email,
          adreess,
          cartype,
          carno,
        ).then((v) {
          Navigator.pop(context, "done");
        }).catchError((e) {
          Navigator.pop(context, "failed");
          print(e);
        });
      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }

  onDeleteProductBtnTap() async {
    await showDialog(
      context: context,
      builder: (ctx) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18),
          ),
          title: Text("Are you sure?"),
          content: Text(
              "Once you press delete, we will delete it from our storage, this process is irreversible."),
          actions: [
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(ctx);
              },
            ),
            FlatButton(
              textColor: Colors.red,
              child: Text("Delete Permanently"),
              onPressed: () {
                /*deleteProduct(widget.document.id,
                    widget.document.data()["user_images"])
                    .then((value) {
                  Navigator.pop(ctx);
                }).catchError((e) {
                  print(e);
                  Navigator.pop(ctx);
                });*/
              },
            ),
          ],
        );
      },
    );
    Navigator.pop(context, "done");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Product"),
        actions: [
          IconButton(
            icon: Icon(Icons.delete_forever),
            onPressed: () {
              onDeleteProductBtnTap();
            },
          ),
        ],
      ),
      body: _loading == true
          ? Center(
              child: SpinKitChasingDots(
                color: primaryColor,
                size: 50,
              ),
            )
          : Container(
              padding: EdgeInsets.symmetric(
                horizontal: 12,
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    FadeInImage(
                      placeholder: AssetImage("assets/admin/placeholder.png"),
                      image: NetworkImage(widget.document.data()["userImage"]),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        labelText: "User Name",
                      ),
                      maxLength: 120,
                      controller: NameController,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        labelText: "Date Of Birth",
                      ),
                      maxLines: 1,
                      controller: DateOfBirthController,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        labelText: "Email",
                        prefixIcon: Icon(FontAwesomeIcons.mailBulk),
                      ),
                      keyboardType: TextInputType.numberWithOptions(
                        decimal: true,
                      ),
                      controller: emailController,
                    ),
                    Text(
                      "Price in $currencySymbol $currencyCode",
                      textAlign: TextAlign.end,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        labelText: "Adress",
                        prefixIcon: Icon(FontAwesomeIcons.addressBook),
                      ),
                      keyboardType: TextInputType.numberWithOptions(
                        decimal: true,
                      ),
                      controller: adreessPriceController,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    if (_categories != null && _categories.length > 0) ...[
                      DropdownButtonFormField(
                        decoration: InputDecoration(
                          filled: true,
                          labelText: "Category",
                        ),
                        items: _categories.map((c) {
                          return DropdownMenuItem(
                            value: c.id,
                            child: Text(c.id),
                          );
                        }).toList(),
                        value: cartype,
                        onChanged: (v) {
                          setState(() {
                            cartype = v;
                          });
                        },
                      ),
                    ],
                    SizedBox(
                      height: 20,
                    ),
                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        labelText: "CarNo",
                        prefixIcon: Icon(FontAwesomeIcons.car),
                      ),
                      keyboardType: TextInputType.text,
                      controller: acarNoController,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    RaisedButton(
                      onPressed: () {
                        onUpdateProductBtnTap();
                      },
                      child: Text("SAVE"),
                    ),
                    SizedBox(
                      height: 100,
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
