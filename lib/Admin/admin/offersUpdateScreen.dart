import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/Admin/helpers/offers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';


class OffersUpdateScreen extends StatefulWidget {
  final DocumentSnapshot document;

  OffersUpdateScreen({@required this.document});

  @override
  _OffersUpdateScreenState createState() => _OffersUpdateScreenState();
}

class _OffersUpdateScreenState extends State<OffersUpdateScreen> {
  List<QueryDocumentSnapshot> _categories;

  bool _loading = false;

  // product fields
  String category = '';
  TextEditingController productNameController = TextEditingController();
  // product fields

  @override
  void dispose() {
    productNameController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    productNameController.text = widget.document.data()["OfferName"];
    super.initState();
  }


  onUpdateProductBtnTap() async {
    try {
      String productName = productNameController.text;


      if (productName.isNotEmpty) {

          print(
              "$productName");

          updateOffers(
            widget.document.id,
            productName,
          ).then((v) {
            Navigator.pop(context, "done");
          }).catchError((e) {
            Navigator.pop(context, "failed");
            print(e);
          });
      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }

  onDeleteProductBtnTap() async {
    await showDialog(
      context: context,
      builder: (ctx) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18),
          ),
          title: Text("Are you sure?"),
          content: Text(
              "Once you press delete, we will delete it from our storage, this process is irreversible."),
          actions: [
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(ctx);
              },
            ),
            FlatButton(
              textColor: Colors.red,
              child: Text("Delete Permanently"),
              onPressed: () {
                deleteOffers(widget.document.id,
                    widget.document.data()["OfferImage"])
                    .then((value) {
                  Navigator.pop(ctx);
                }).catchError((e) {
                  print(e);
                  Navigator.pop(ctx);
                });
              },
            ),
          ],
        );
      },
    );
    Navigator.pop(context, "done");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Product"),
        actions: [
          IconButton(
            icon: Icon(Icons.delete_forever),
            onPressed: () {
              onDeleteProductBtnTap();
            },
          ),
        ],
      ),
      body: _loading == true
          ? Center(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      )
          : Container(
        padding: EdgeInsets.symmetric(
          horizontal: 12,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              FadeInImage(
                placeholder: AssetImage("assets/admin/placeholder.png"),
                image:
                NetworkImage(widget.document.data()["OfferImage"]),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                decoration: InputDecoration(
                  filled: true,
                  labelText: "Offers Name",
                ),
                maxLength: 120,
                controller: productNameController,
              ),
              SizedBox(
                height: 20,
              ),


              RaisedButton(
                onPressed: () {
                  onUpdateProductBtnTap();
                },
                child: Text("SAVE"),
              ),
              SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
