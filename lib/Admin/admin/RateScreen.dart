import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/Admin/config/currency.dart';
import 'package:all_in_one_app/Admin/helpers/Rate.dart';
import 'package:all_in_one_app/Admin/helpers/car.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class RateScreen extends StatefulWidget {
  final DocumentSnapshot document;

  RateScreen({@required this.document});

  @override
  _RateScreenState createState() => _RateScreenState();
}

class _RateScreenState extends State<RateScreen> {
  List<QueryDocumentSnapshot> _categories;

  bool _loading = false;

  // product fields
  String category = '';
  TextEditingController productNameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  // product fields

  @override
  void dispose() {
    productNameController.dispose();
    priceController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    loadCategories();
    productNameController.text = widget.document.data()["productName"];
    priceController.text = widget.document.data()["price"].toString();
    super.initState();
  }

  loadCategories() async {
    try {
      setState(() {
        _loading = true;
      });

      QuerySnapshot qsnap = await getCarType();
      setState(() {
        _categories = qsnap.docs;
        category = widget.document.data()["category"];
        _loading = false;
      });
    } catch (e) {
      print(e);
    }
  }

  onUpdateProductBtnTap() async {
    try {
      String productName = productNameController.text;


      if (productName.isNotEmpty &&
          priceController.text.isNotEmpty ) {
        double price = double.parse(priceController.text);

        updateProduct(
          widget.document.id,
          productName,
          price,
          category,
        ).then((v) {
          Navigator.pop(context, "done");
        });

      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }

  onDeleteProductBtnTap() async {
    await showDialog(
      context: context,
      builder: (ctx) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18),
          ),
          title: Text("Are you sure?"),
          content: Text(
              "Once you press delete, we will delete it from our storage, this process is irreversible."),
          actions: [
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(ctx);
              },
            ),
            FlatButton(
              textColor: Colors.red,
              child: Text("Delete Permanently"),
              onPressed: () {
                deleteProduct(widget.document.id,)
                    .then((value) {
                  Navigator.pop(ctx);
                }).catchError((e) {
                  print(e);
                  Navigator.pop(ctx);
                });
              },
            ),
          ],
        );
      },
    );
    Navigator.pop(context, "done");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Product"),
        actions: [
          IconButton(
            icon: Icon(Icons.delete_forever),
            onPressed: () {
              onDeleteProductBtnTap();
            },
          ),
        ],
      ),
      body: _loading == true
          ? Center(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      )
          : Container(
        padding: EdgeInsets.symmetric(
          horizontal: 12,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextField(
                decoration: InputDecoration(
                  filled: true,
                  labelText: "Product Name",
                ),
                maxLength: 120,
                controller: productNameController,
              ),
              SizedBox(
                height: 20,
              ),


              TextField(
                decoration: InputDecoration(
                  filled: true,
                  labelText: "Price",
                  prefixIcon: Icon(FontAwesomeIcons.moneyBillAlt),
                ),
                keyboardType: TextInputType.numberWithOptions(
                  decimal: true,
                ),
                controller: priceController,
              ),
              Text(
                "Price in $currencySymbol $currencyCode",
                textAlign: TextAlign.end,
              ),
              SizedBox(
                height: 20,
              ),
              if (_categories != null && _categories.length > 0) ...[
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    filled: true,
                    labelText: "Category",
                  ),
                  items: _categories.map((c) {
                    return DropdownMenuItem(
                      value: c.id,
                      child: Text(c.id),
                    );
                  }).toList(),
                  value: category,
                  onChanged: (v) {
                    setState(() {
                      category = v;
                    });
                  },
                ),
              ],
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                onPressed: () {
                  onUpdateProductBtnTap();
                },
                child: Text("SAVE"),
              ),
              SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
