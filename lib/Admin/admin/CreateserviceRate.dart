import 'dart:io';

import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/Admin/config/currency.dart';
import 'package:all_in_one_app/Admin/helpers/Rate.dart';
import 'package:all_in_one_app/Admin/helpers/car.dart';
import 'package:all_in_one_app/Admin/helpers/category.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'package:path/path.dart' as path;

class CreateserviceRate extends StatefulWidget {
  @override
  _CreateserviceRateState createState() => _CreateserviceRateState();
}



class _CreateserviceRateState extends State<CreateserviceRate> {


  List<QueryDocumentSnapshot> _categories;

  bool _loading = false;

  // product fields
  String category = '';
  TextEditingController productNameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  // product fields

  @override
  void dispose() {
    productNameController.dispose();
    priceController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    loadCategories();
    super.initState();
  }

  loadCategories() async {
    try {
      setState(() {
        _loading = true;
      });

      QuerySnapshot qsnap = await getCarType();
      setState(() {
        _categories = qsnap.docs;
        category = qsnap.docs[0].id;
        _loading = false;
      });
    } catch (e) {
      print(e);
    }
  }

  onCreateProductBtnTap() async {
    try {
      String productName = productNameController.text;


      if (productName.isNotEmpty &&
          priceController.text.isNotEmpty != null) {
        double price = double.parse(priceController.text);

        addProduct(
          productName,
          price,
          category,
        ).then((v) {
          Navigator.pop(context, "done");
        });


      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create Rates"),
      ),
      body:
      Container(
        padding: EdgeInsets.symmetric(
          horizontal: 12,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextField(
                decoration: InputDecoration(
                  filled: true,
                  labelText: "Product Name",
                ),
                maxLength: 120,
                controller: productNameController,
              ),
              SizedBox(
                height: 20,
              ),

              TextField(
                decoration: InputDecoration(
                  filled: true,
                  labelText: "Price",
                  prefixIcon: Icon(FontAwesomeIcons.moneyBillAlt),
                ),
                keyboardType: TextInputType.numberWithOptions(
                  decimal: true,
                ),
                controller: priceController,
              ),


              SizedBox(
                height: 20,
              ),
              if (_categories != null && _categories.length > 0) ...[
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    filled: true,
                    labelText: "Car Type",
                  ),
                  items: _categories.map((c) {
                    return DropdownMenuItem(
                      value: c.id,
                      child: Text(c.id),
                    );
                  }).toList(),
                  value: category,
                  onChanged: (v) {
                    setState(() {
                      category = v;
                    });
                  },
                ),
              ],
              SizedBox(
                height: 20,
              ),

              RaisedButton(
                onPressed: () {
                  onCreateProductBtnTap();
                },
                child: Text("CREATE PRODUCT"),
              ),
              SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
