import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:shimmer/shimmer.dart';

class CoupanCard extends StatelessWidget {
  final QueryDocumentSnapshot product;
  final Function onTap;

  CoupanCard({@required this.product, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: <Widget>[
            Container(
              width: 100,
              height: 100,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Image(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/codeback.png'),
                  ),
                  Positioned(
                    bottom: 25,
                    left: 10,
                    child: Column(
                      children: <Widget>[
                        Text(
                          product.data()["coupanCode"],
                          style: TextStyle(
                              fontFamily: 'AirbnbCerealBold',
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                        Text(
                            product.data()["flat"].toString(),
                          style: TextStyle(
                              fontFamily: 'AirbnbCerealBook',
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey),
                        ),


                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
