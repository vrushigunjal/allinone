import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

void showMyLoadingModal(BuildContext context, String text) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) {
      return SimpleDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18),
        ),
        title: Text(text),
        children: [
          SpinKitChasingDots(
            color: primaryColor,
            size: 50,
          ),
        ],
      );
    },
  );
}
