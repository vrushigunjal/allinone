
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';



class OfferCard extends StatelessWidget {
  final QueryDocumentSnapshot product;
  final Function onTap;

  OfferCard({@required this.product, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Stack(
        children: [
          Container(
            constraints: new BoxConstraints.expand(
              height: 150.0,
              width: 250.0
            ),
            padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new NetworkImage(product.data()["OfferImage"]),
                fit: BoxFit.cover,
              ),
            ),
            child: new Stack(
              children: <Widget>[

                new Positioned(
                  left: 0.0,
                  top: 5.0,
                  child: new Text("WashOnClick",
                      style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15.0,
                          color: Colors.white
                      )
                  ),
                ),
            new Positioned(
            left: 0.0,
              top: 40.0,
              child: new Text(product.data()["OfferName"],
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 30.0,
                    color: Colors.white
                  )
              ),
            ),
                new Positioned(
                  left: 0.0,
                  top: 80.0,
                  child: new Text("On tyre dressing and \n polishing of your car",
                      style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15.0,
                          color: Colors.white
                      )
                  ),
                ),
                new Positioned(
                  right: 0.0,
                  top: 0.0,
                  child: Image(
                    image: AssetImage("assets/admincoupons.png"),
                    height: 150.0,
                    width: 40.0,
                  ),

                ),
              ],
            )
         /*   child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: FadeInImage(
                    image: NetworkImage(product.data()["OfferImage"]),
                    placeholder: AssetImage("assets/admin/placeholder.png"),
                    width: 200,
                    height: 150,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  width: 14,
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        product.data()["OfferName"],
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),*/
          ),
        ],
      ),
    );
  }
}
