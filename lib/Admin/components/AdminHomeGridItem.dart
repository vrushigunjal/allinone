import 'package:flutter/material.dart';
import 'package:flutter/material.dart';

class AdminHomeGridItem extends StatelessWidget {
  final Image imageIcon;
  final Color iconColor;
  final String text;

  final Function onTap;

  AdminHomeGridItem(
      {@required this.imageIcon,
      this.iconColor = Colors.amber,
      this.text = "",
      @required this.onTap, int size,});

  @override
  Widget build(BuildContext context) {
    return InkWell(

      onTap: onTap,
      child: Container(
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.circular(16),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
         //   Image(image: null, size: 50, color: iconColor),
            SizedBox(
              height: 5,
            ),
            Text(text),
          ],
        ),
      ),
    );
  }
}
