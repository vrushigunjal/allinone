import 'package:all_in_one_app/Admin/admin/Payment/WeeklyPayment.dart';
import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/config/currency.dart';
import 'package:all_in_one_app/Admin/helpers/AdiminToWasherPayment.dart';
import 'package:all_in_one_app/User/helper/ServiceOrderConfirm.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

class PaymentItem extends StatefulWidget {
  final DocumentSnapshot orderSnapshot;
  final Function onTap;

  PaymentItem({@required this.orderSnapshot, @required this.onTap});

  @override
  _PaymentItemState createState() => _PaymentItemState();
}

class _PaymentItemState extends State<PaymentItem> {



  @override
  Widget build(BuildContext context) {
    var order = widget.orderSnapshot.data();
    var id = widget.orderSnapshot.id;

    DateTime createdAt = order['created_at'].toDate();
    String formattedDate =
        "${createdAt.year}/${createdAt.month}/${createdAt.day} ${createdAt.hour}:${createdAt.minute}:${createdAt.second}";

      return InkWell(

      onTap: widget.onTap,

      child: Card(
        elevation: 10.0,

        margin: EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 10,
        ),
        child:Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: Colors.grey[100],
            width: 10,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
              color: Color(0x11000000),
              blurRadius: 16,
              offset: Offset(0, 4),
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.all(8.0),
            ),
            Text(
              "Order# $id",
              style: TextStyle(
                color: blackColor,
                fontWeight: FontWeight.w700,
                fontSize: 12.0,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [


                Text(
                  "Washer name:  ${order['Centername']} ",
                  style: TextStyle(
                    color: Colors.grey[800],
                    fontSize: 12.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),

                Text(
                  "User Phone:  ${order['PhoneNumber']} ",
                  style: TextStyle(
                    color: Colors.grey[800],
                    fontSize: 12.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                /*Text(
                  "User Payment:  $currencySymbol${order['result']}",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),*/
              /*  SizedBox(
                  height: 5,
                ),
                Text(
                  "Center Name:  ${order['centerName']}",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  "Center Payment:  $currencySymbol${order['_Washerprice']}",
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),*/
            Text(
              formattedDate,
              style: TextStyle(
                color: Colors.grey[800],
                fontSize: 12.0,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(
              height: 25,
            ),
                Divider(),
              ],
        ),
      ],
    ),
    ),
    ),
    );
  }
}
