import 'package:all_in_one_app/Admin/admin/CoupansDetailsScreen.dart';
import 'package:all_in_one_app/Admin/admin/CreateCoupansScreen.dart';
import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/User/helper/user.dart';
import 'package:all_in_one_app/constant/constant.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

enum SearchBy { uid, phone, randomnumber }

class _SearchScreenState extends State<SearchScreen> {
  SearchBy searchBy = SearchBy.uid;

  bool _isLoading = false;
  bool _searchSuccess = false;

  QueryDocumentSnapshot snapshot;

  void search(String searchValue) {
    if (searchValue.isNotEmpty && searchValue.length > 0) {
      String key;

      if (searchBy == SearchBy.uid) {
        key = "uid";
      } else if (searchBy == SearchBy.phone) {
        key = "PhoneNumber";
        print(key);
      } else if (searchBy == SearchBy.randomnumber) {
        key = "randomnumber";
        print(key);
      }

      setState(() {
        _isLoading = true;
      });
      searchUser(searchValue, key).then((qSnap) {
        if (qSnap.size == 0) {
          showMyDialog(
            context: context,
            title: "oops",
            description: "No user found!",
          );
          setState(() {
            _searchSuccess = false;
            _isLoading = false;
          });
        } else {
          setState(() {
            _searchSuccess = true;
            _isLoading = false;
            snapshot = qSnap.docs[0];
          });
        }
      }).catchError((e) {
        print(e);
        showMyDialog(
          context: context,
          title: "oops",
          description: "Something went wrong",
        );
        setState(() {
          _isLoading = false;
        });
      });
    } else {
      showMyDialog(
        context: context,
        title: "oops",
        description: "Please write search value",
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      height: 500.0,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Radio(
                  value: SearchBy.uid,
                  groupValue: searchBy,
                  onChanged: (value) {
                    setState(() {
                      searchBy = value;
                    });
                  },
                ),
                Text("Uid"),
                Radio(
                  value: SearchBy.phone,
                  groupValue: searchBy,
                  onChanged: (value) {
                    setState(() {
                      searchBy = value;
                    });
                  },
                ),
                Text("Phone"),
                Radio(
                  value: SearchBy.randomnumber,
                  groupValue: searchBy,
                  onChanged: (value) {
                    setState(() {
                      searchBy = value;
                    });
                  },
                ),
                Text("Randomnumber"),
              ],
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 10,
              ),
              margin: EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 10,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.grey[200],
              ),
              child: TextField(
                decoration: InputDecoration.collapsed(
                  hintText: "Search",
                ),
                textInputAction: TextInputAction.search,
                onSubmitted: (searchValue) {
                  search(searchValue);
                },
              ),
            ),
            SizedBox(
              height: 20,
            ),
            if (_isLoading == true) ...[
              SpinKitChasingDots(
                color: Colors.yellow,
                size: 50,
              ),
            ],
            if (_searchSuccess == true) ...[
              Container(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: CircleAvatar(
                        backgroundColor: Colors.yellow,
                        backgroundImage: NetworkImage(
                          snapshot.data()["userImage"],
                        ),
                        radius: 50,
                      ),
                    ),
                    DataTable(
                      columns: [
                        DataColumn(label: Text("Column")),
                        DataColumn(label: Text("Data")),
                      ],
                      rows: [
                        DataRow(
                          cells: [
                            DataCell(Text("User id")),
                            DataCell(SelectableText(snapshot.data()["uid"])),
                          ],
                        ),
                        DataRow(
                          cells: [
                            DataCell(Text("Phone")),
                            DataCell(SelectableText(
                                snapshot.data()["PhoneNumber"] ?? "")),
                          ],
                        ),
                        DataRow(
                          cells: [
                            DataCell(Text("Randomnumber")),
                            DataCell(SelectableText(
                                snapshot.data()["randomnumber"] ?? ""))
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),

            Container(
              child: Column(children: [
                Padding(
                    padding: const EdgeInsets.all(12.0),
    child: SizedBox(
    height: 50.0,
    width: width - (fixPadding * 2.0),
                    child: RaisedButton(
                      elevation: 10.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      onPressed: () async {
                      String randomnumber = snapshot.data()["randomnumber"] ?? "" ;
                      SharedPreferences prefs = await SharedPreferences.getInstance();
                      prefs.setString('RandomNUMBER', randomnumber);

                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => CreateCoupansScreen(RandomNumber:randomnumber)));
                      },
                      color: Colors.yellow,
                      child: Text(
                        'Ganerate Coupan',
                        style: wbuttonWhiteTextStyle,
                      ),
                    )),
                ),]),
            ),
            ],
          ],
        ),
      ),
    );
  }
}
