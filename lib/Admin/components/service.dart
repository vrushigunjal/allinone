
import 'package:all_in_one_app/Admin/config/currency.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class ServiceCard extends StatelessWidget {
  final QueryDocumentSnapshot product;
  final Function onTap;

  ServiceCard({@required this.product, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 10,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: FadeInImage(
                    image: NetworkImage(product.data()["serviceImage"]),
                    placeholder: AssetImage("assets/admin/placeholder.png"),
                    width: 100,
                    height: 100,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  width: 14,
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        product.data()["centerName"],
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),

                      Text(
                        product.data()["title"],
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),

                      Text(
                        //"$\Rs$product.data()["salePrice"].toString()}",
                        "$currencySymbol${product.data()['Total'].toString()}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),

                    ],
                  ),
                ),
              ],
            ),
          ),

        ],
      ),
    );
  }
}
