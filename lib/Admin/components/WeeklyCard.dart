
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class WeeklyCard extends StatelessWidget {
  final QueryDocumentSnapshot product;
  final Function onTap;
  WeeklyCard({@required this.product, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    var total = product["Washerprice"];
    return InkWell(
      onTap: onTap,
      child: Card(
        elevation: 10.0,
        child: Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 10,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
               /* ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: FadeInImage(
                    image: NetworkImage(product.data()["userImage"]),
                    placeholder: AssetImage("assets/admin/placeholder.png"),
                    width: 100,
                    height: 100,
                    fit: BoxFit.cover,
                  ),
                ),*/
                SizedBox(
                  width: 14,
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        product.data()["productName"],
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                      Row(
                        children: [
                          Text(
                            "${product.data()['Phone'].toString()}",
                            softWrap: true,
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.green[600],
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          /*if (product.data()["price"] !=
                              product.data()["salePrice"]) ...[
                            Text(
                              product.data()["price"].toString(),
                              softWrap: true,
                              style: TextStyle(
                                color: Colors.grey[700],
                                decoration: TextDecoration.lineThrough,
                              ),
                            ),
                          ]*/
                        ],
                      ),

                      Text(
                        "${product.data()['Washerprice']}",
                        softWrap: true,
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          /*  Positioned(
              top: 5,
              left: 5,
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 5,
                  vertical: 0,
                ),
                decoration: BoxDecoration(
                  color: primaryColor,
                  borderRadius: BorderRadius.circular(4),
                ),
                child: Shimmer.fromColors(
                  baseColor: Colors.white,
                  highlightColor: primaryColor,
                  child: Row(
                    children: [
                      Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                      Text("Featured"),
                    ],
                  ),
                ),
              ),
            ),
*/

        ],
      ),

    ),
    );
  }
}
