import 'package:all_in_one_app/constant/constant.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


import '../mainsplashscreen.dart';
import 'admin/AdminHomeScreen.dart';
import 'admin/CategoriesScreen.dart';
import 'admin/CreateProductScreen.dart';
import 'admin/OrdersScreenAdmin.dart';
import 'admin/ProductsScreen.dart';
import 'admin/ShopDetails.dart';
import 'admin/UpdateProductScreen.dart';
import 'admin/UserDetailsScreen.dart';
import 'config/theme.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  final FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown],
    );
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarBrightness: Brightness.dark,
      statusBarColor: primaryColor,
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));

    return MaterialApp(
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: firebaseAnalytics),
      ],
      debugShowCheckedModeBanner: false,
      theme: lightTheme,
      initialRoute: "/",
      routes: {
        "/": (context) => SplashScreen(),
        "/admin": (context) => AdminHomeScreen(),
        "/admin_orders": (context) => OrdersScreenAdmin(),
       /* "/home": (context) => HomeScreen(),*/
       // "/about": (context) => AboutShopDetails(),
      //  "/edit_my_phone_number": (context) => EditMyPhoneScreen(),
        //"/edit_my_address": (context) => EditMyAddressScreen(),
        "/edit_shop_details": (context) => ShopDetailsScreen(),
        "/user_details": (context) => UserDetailsScreen(),
        "/manage_products": (context) => ProductsScreen(),
        "/create_product": (context) => CreateProductScreen(),
        "/update_product": (context) => UpdateProductScreen(),
        "/categories_admin": (context) => ServicesScreen(),
      },
    );
  }
}
