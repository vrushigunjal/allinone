import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

final FirebaseFirestore firestore = FirebaseFirestore.instance;
final FirebaseStorage storage = FirebaseStorage();

Future<String> uploadOffersImage(String imageName, File image) async {
  final StorageReference storageReference =
  FirebaseStorage().ref().child("offers_images").child(imageName);

  final StorageUploadTask uploadTask = storageReference.putFile(image);

  StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
  return await storageTaskSnapshot.ref.getDownloadURL();
}

Future<dynamic> addOffers(
    String offersImage,
    String offersName) async {
  return await firestore.collection("Offers").add({
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "OfferImage": offersImage,
    "OfferName": offersName,
  });
}

Future<void> updateOffers(String docid, String offersName) async {
  return await firestore.collection("Offers").doc(docid).update({
    "updated_at": FieldValue.serverTimestamp(),
    "OfferName": offersName,


  });
}

Future<void> deleteOffers(String docid, String image) async {
  StorageReference imgRef = await storage.getReferenceFromUrl(image);
  await imgRef.delete();
  return await firestore.collection("Offers").doc(docid).delete();
}

Future<QuerySnapshot> getOffers() async {
  return await firestore
      .collection("Offers")
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}

Future<QuerySnapshot> getMoreOffers(DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("Offers")
      .orderBy("created_at", descending: true)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}
/*
Future<QuerySnapshot> searchOffers(String searchValue) async {
  return await firestore
      .collection("Offers")
      .where("productName", isGreaterThanOrEqualTo: searchValue)
      .where("productName", isLessThanOrEqualTo: searchValue + '\uf8ff')
      .limit(20)
      .get();
}*/



