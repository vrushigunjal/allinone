import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

final FirebaseFirestore firestore = FirebaseFirestore.instance;
final FirebaseStorage storage = FirebaseStorage();



Future<dynamic> addCoupan(

    String randomnumber,
    String coupancode,
    String flat,
    String CoupanType,
   ) async {
  return await firestore.collection("coupans").add({
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "RandomNumber": randomnumber,
    "coupanCode": coupancode,
    "flat": flat,
    "First": CoupanType,
  });
}

Future<void> updateCoupan(String docid, String coupancode, String flat) async {
  return await firestore.collection("coupans").doc(docid).update({
    "updated_at": FieldValue.serverTimestamp(),
    "coupanCode": coupancode,
    "flat": flat,
  });
}

Future<void> deleteCoupan(String docid) async {
 /* StorageReference imgRef = await storage.getReferenceFromUrl(image);
  await imgRef.delete();*/
  return await firestore.collection("coupans").doc(docid).delete();
}

Future<QuerySnapshot> getCoupan() async {
  return await firestore
      .collection("coupans")
      .orderBy("created_at", descending: true)
      .get();
}

Future<QuerySnapshot> getAllCoupan( String Uid) async {
  return await firestore
      .collection("ASSS")
      .where('Uid', isEqualTo: Uid)
      .get();
}
Future<QuerySnapshot> getuserMoreCoupan(DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("ASSS")
      .orderBy("created_at", descending: true)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}
Future<dynamic> AddUserCoupan(

    String Uid,
    String flat
    ) async {
  return await firestore.collection("ASSS").add({
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "Uid": Uid,
    "flat": flat,

  });
}



/*
Future<QuerySnapshot>getCoupan(
    String id) async {
  print(id);
  return await firestore
      .collection("coupans")
      .where("uid", isEqualTo: id)
      .limit(20)
      .get();
}
*/

Future<QuerySnapshot> getMoreCoupan(DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("coupans")
      .orderBy("created_at", descending: true)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}

Future<QuerySnapshot> searchCoupan(String searchValue) async {
  return await firestore
      .collection("coupans")
      .where("coupanCode", isGreaterThanOrEqualTo: searchValue)
      .where("coupanCode", isLessThanOrEqualTo: searchValue + '\uf8ff')
      .limit(20)
      .get();
}

/*Future<QuerySnapshot> geCoupanByCategory(String category) async {
  return await firestore
      .collection("coupans")
      .where("category", isEqualTo: category)
      .limit(20)
      .get();
}*/

Future<QuerySnapshot> getMoreCoupanByCategory(
    String category, DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("coupans")
      .where("category", isEqualTo: category)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}
