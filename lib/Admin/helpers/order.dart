import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';

final FirebaseFirestore firestore = FirebaseFirestore.instance;
final FirebaseAuth auth = FirebaseAuth.instance;
final User user = auth.currentUser;
final FirebaseStorage storage = FirebaseStorage();

Future<DocumentReference> addOrderPayOnDelivery(
    List cart,
    double cartItemsTotal,
    int tax,
    double taxAmount,
    double total,
    String addressType,
    String address,
    String phone,
    String name) async {
  return await firestore.collection("orders").add({
    "uid": user.uid,
    "paymentMethod": "payOnDelivery",
    "orderStatus": "confirm", // ordered, delivering, delivered, cancelled
    "paymentStatus": "notPaid", // notPaid, paid
    "addressType": addressType,
    "address": address,
    "phone": phone,
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "cart": cart,
    "cartItemsTotal": cartItemsTotal,
    "tax": tax,
    "taxAmount": taxAmount,
    "total": total,
    "name": name
  });
}

Future<DocumentReference> addOrderSelfPickup(
  List cart,
  double cartItemsTotal,
  int tax,
  double taxAmount,
  double total,
) async {
  return await firestore.collection("orders").add({
    "uid": user.uid,
    "paymentMethod": "selfPickup",
    "orderStatus": "ordered", // ordered, delivering, delivered, cancelled
    "paymentStatus": "notPaid", // notPaid, paid
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "cart": cart,
    "cartItemsTotal": cartItemsTotal,
    "tax": tax,
    "taxAmount": taxAmount,
    "total": total
  });
}

Future<QuerySnapshot> getAllOrdersOfUser() async {
  return await firestore
      .collection("orders")
      .where("uid", isEqualTo: user.uid)
      .orderBy("created_at", descending: true)
      .get();
}
Future<QuerySnapshot> getallproductPayment() async {
  return await firestore
      .collection("orders")
      .get();
}
Future<QuerySnapshot> getMoreproductPayment(
    DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("orders")
      .startAfterDocument(lastDocument)

      .get();
}

Future<QuerySnapshot> getOrders(String orderStatus) async {
  return await firestore
      .collection("orders")
      .where("orderStatus", isEqualTo: orderStatus)
      .get();
}

Future<QuerySnapshot> getMoreOrders(
    String orderStatus, DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("orders")
      .where("orderStatus", isEqualTo: orderStatus)
      .startAfterDocument(lastDocument)
      .orderBy("created_at", descending: true)
      .get();
}

Future<void> deleteCart(String docid) async {
  return await firestore.collection("orders").doc(docid).delete();
}

Future<void> updateOrderPrice(String docid, double price) async {
  return await firestore.collection("products").doc(docid).update({
    "salePrice": price,

  });
}

/*
Future<void>checkuid(String id) async {
  if ("uid" == id) {
    return await firestore.collection("orders").doc(id).update({
      "paymentStatus": "paid",
      "orderStatus": status,
    });
  } else {
    return await firestore.collection("orders").doc(id).update({
      "orderStatus": status,
    });
  }
*/

/*Future<void> updateOrderStatus(String id, String status) async {
  if (status == "delivered") {
    return await firestore.collection("orders").doc(id).update({
      "paymentStatus": "paid",
      "orderStatus": status,
    });
  } else {
    return await firestore.collection("orders").doc(id).update({
      "orderStatus": status,
    });
  }
}*/
