import 'package:cloud_firestore/cloud_firestore.dart';

final FirebaseFirestore firestore = FirebaseFirestore.instance;

/*
void createRecord() async {
  await firestore.collection("books")
      .document("1")
      .setData({
    'title': 'Mastering Flutter',
    'description': 'Programming Guide for Dart'
  });

  DocumentReference ref = await firestore.collection("books")
      .add({
    'title': 'Flutter in Action',
    'description': 'Complete Programming Guide to learn Flutter'
  });
  print(ref.documentID);
}*/

Future<void> createCarType(String title) async {
  return await FirebaseFirestore.instance.collection("TypesOfCar").doc(title).set({
    "title": title,
  }, SetOptions(merge: true));
}

Future<QuerySnapshot> getCarType() async {
  return await firestore.collection("TypesOfCar").get();
}

Future<void> deleteCarType(String id) async {
  return await firestore.collection("TypesOfCar").doc(id).delete();
}
