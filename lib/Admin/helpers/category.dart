import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

final FirebaseFirestore firestore = FirebaseFirestore.instance;

final FirebaseStorage storage = FirebaseStorage();


Future<String> uploadServiceImage(String imageName, File image) async {
  final StorageReference storageReference =
  FirebaseStorage().ref().child("Service_images").child(imageName);

  final StorageUploadTask uploadTask = storageReference.putFile(image);

  StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
  return await storageTaskSnapshot.ref.getDownloadURL();
}

Future<void> createCategory( String serviceImage,String title) async {
  return await firestore.collection("categories").doc(title).set({
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "serviceImage": serviceImage,
    "title": title,
  }, SetOptions(merge: true));
}
Future<void> updateCategory(String docid, String title) async {
  return await firestore.collection("categories").doc(docid).update({
    "updated_at": FieldValue.serverTimestamp(),
    "title": title,

  });
}//Categories
Future<QuerySnapshot> getCategories() async {
  return await firestore
      .collection("categories")
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}
/*
Future<QuerySnapshot> getWashCategories(String id) async {
  return await firestore
      .collection("categories")
      .where("uid", isEqualTo: id)
      .get();
}
*/

Future<QuerySnapshot> getWashCategories(
    String id) async {
  print(id);
  return await firestore
      .collection("categories")
      .where("uid", isEqualTo: id)
      .limit(20)
      .get();
}



Future<QuerySnapshot> getMoreCategories(DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("categories")
      .orderBy("created_at", descending: true)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}
Future<void> deleteCategory(String id,String image) async {
  StorageReference imgRef = await storage.getReferenceFromUrl(image);
  await imgRef.delete();
  return await firestore.collection("categories").doc(id).delete();
}
