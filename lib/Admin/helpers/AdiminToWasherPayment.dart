import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseFirestore firestore = FirebaseFirestore.instance;
final FirebaseAuth auth = FirebaseAuth.instance;
final User user = auth.currentUser;


/*Future<String> uploadProductImage(String imageName, File image) async {
  final StorageReference storageReference =
  FirebaseStorage().ref().child("product_images").child(imageName);

  final StorageUploadTask uploadTask = storageReference.putFile(image);

  StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
  return await storageTaskSnapshot.ref.getDownloadURL();
}*/

Future<dynamic> addAdminPayment(
    String id,
    String phone,
    String productName,
    double _Washerprice,
    String order,
    ) async {
  return await firestore.collection("AdminToWasherPaymnet").add({
    "uid": user.uid,
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "Phone": phone,
    "productName": productName,
    "_Washerprice": _Washerprice,
    "order": order,
  });
}



Future<QuerySnapshot> getAllpaymentOfAdmin() async {
  return await firestore
      .collection("AdminToWasherPaymnet")
      .where("uid", isEqualTo: user.uid)
      .get();
}

/*Future<QuerySnapshot> getAllOrdersOfUser() async {
  return await firestore
      .collection("orders")
      .where("uid", isEqualTo: user.uid)
      .orderBy("created_at", descending: true)
      .get();
}*/




/*Future<QuerySnapshot> getprchaseOrder(String orderStatus) async {
  return await firestore
      .collection("orders")
      .where("order", isEqualTo: orderStatus)
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}*/

/*Future<QuerySnapshot> getAllOrdersOfUser() async {
  return await firestore
      .collection("orders")
      .orderBy("created_at", descending: true)
      .get();
}*/
Future<QuerySnapshot> getallAdminPayment() async {
  return await firestore
      .collection("AdminToWasherPaymnet")
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}

Future<QuerySnapshot> getprchaseAdminPayment(String orderStatus) async {
  return await firestore
      .collection("AdminToWasherPaymnet")
      .orderBy("created_at", descending: true)
      .get();
}
Future<QuerySnapshot> getMoreprchaseAdminPayment(
    DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("AdminToWasherPaymnet")
      .startAfterDocument(lastDocument)
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}

Future<void> updateAdminPaymentStatus(String id, String status) async {
  if (status == "Done") {
    return await firestore.collection("ordersService").doc(id).update({
      /* "paymentStatus": "paid",*/
      "order": status,
    });
  } else {
    return await firestore.collection("ordersService").doc(id).update({
      "order": status,
    });
  }
}

/*
Future<QuerySnapshot> getMoreprchaseOrder(DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("orders")
      .orderBy("created_at", descending: true)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}
*/

Future<QuerySnapshot> searchAdminOrder(String searchValue) async {
  return await firestore
      .collection("AdminToWasherPaymnet")
      .where("productName", isGreaterThanOrEqualTo: searchValue)
      .where("productName", isLessThanOrEqualTo: searchValue + '\uf8ff')
      .limit(20)
      .get();
}

