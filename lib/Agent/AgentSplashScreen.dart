import 'dart:async';



import 'package:all_in_one_app/Agent/helper/user.dart';
import 'package:all_in_one_app/Agent/login_signup/login.dart';
import 'package:all_in_one_app/Agent/pages/Home.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class AgentSplashScreen extends StatefulWidget {
  @override
  _AgentSplashScreenState createState() => _AgentSplashScreenState();
}

class _AgentSplashScreenState extends State<AgentSplashScreen> {

  bool _initialized = false;
  bool _error = false;


  @override
  void initState() {
    initializeFlutterFire();
    super.initState();

    Timer(
        Duration(seconds: 3),
            () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Login()),
        ));
  }

  void initializeFlutterFire() async {
    try {
      // Wait for Firebase to initialize and set `_initialized` state to true
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
      });
      checkUserAuth();
    } catch (e) {
      // Set `_error` state to true if Firebase initialization fails
      setState(() {
        _error = true;
      });
    }
  }

  void checkUserAuth() async {
    final auth = FirebaseAuth.instance;
    auth.authStateChanges().listen((User user) {
      if (user != null) {
        getUserData().then((u) {
          final bool isAgent = u.data()["isAgent"];

          if (isAgent == true) {
            Timer(
                Duration(seconds: 3),
                    () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomeAgent()),
                ));
          } else {
            Timer(
                Duration(seconds: 3),
                    () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Login()),
                ));
          }
        }).catchError((e) {
          print(e);
        });
      } else {
        // user is signed out
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: scaffoldBgColor,
      body: Padding(
        padding: EdgeInsets.all(fixPadding),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/washer/delivery.png',
                width: 200.0,
                fit: BoxFit.fitWidth,
              ),
              heightSpace,
              heightSpace,
              heightSpace,
              heightSpace,
              heightSpace,
              SpinKitPulse(
                color: primaryColor,
                size: 50.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
