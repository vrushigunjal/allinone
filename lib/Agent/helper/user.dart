import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseAuth auth = FirebaseAuth.instance;
final FirebaseFirestore firestore = FirebaseFirestore.instance;

final User user = auth.currentUser;

/*editagentNumber(String phone, String email, String password,bool agentregistration) {
  return firestore
      .collection("agents")
      .doc(user.uid)
      .set({"phone": phone, "email": email, "password": password}, SetOptions(merge: true));
}*/


Future<String>editagentNumber(String name,String email,String userphone, bool userRegister) async{
  firestore.collection("agents").doc(user.uid).set({
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "name": name,
    "PhoneNumber": userphone,
    "email": email,
    "uid": user.uid,
    "isAgent":userRegister
  }, SetOptions(merge: true));
  return user.uid;
}
editAddress(String homeAddress, String officeAddress) {
  return firestore.collection("agents").doc(user.uid).set(
      {"homeAddress": homeAddress, "officeAddress": officeAddress},
      SetOptions(merge: true));
}

Future<DocumentSnapshot> getUserData() {
  return firestore.collection("agents").doc(user.uid).get();
}

Future<DocumentSnapshot> getUserDataById(String uid) {
  return firestore.collection("agents").doc(uid).get();
}

Future<QuerySnapshot> searchUser(String searchValue, String searchBy) async {
  return await firestore
      .collection("agents")
      .where(searchBy, isEqualTo: searchValue)
      .limit(1)
      .get();
}
