
import 'package:all_in_one_app/Agent/pages/MyHistory.dart';
import 'package:all_in_one_app/Agent/pages/UserRegister.dart';
import 'package:all_in_one_app/Agent/pages/WasherRegister.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter/material.dart';


class HomeAgent extends StatefulWidget {
  @override
  _HomeAgentState createState() => _HomeAgentState();
}

class _HomeAgentState extends State<HomeAgent> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: scaffoldBgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: whiteColor,
          title: Text(
            'Orders',
            style: bigHeadingStyle,
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.notifications, color: blackColor),
              onPressed: (){
              //  Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: Notifications()));
              },
            ),
          ],
          bottom: TabBar(
            unselectedLabelColor: Colors.grey.withOpacity(0.3),
            labelColor: primaryColor,
            indicatorColor: primaryColor,
            tabs: [
              Tab(text: 'History'),
              Tab(text: 'UserRegister'),
              Tab(text: 'WasherRegister'),

            ],
          ),
        ),
        body: TabBarView(
          children: [
            MyHistory(),
            UserRegister(),
            WasherRegister(),
          ],
        ),
      ),
    );
  }
}