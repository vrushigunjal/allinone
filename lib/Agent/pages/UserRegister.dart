import 'dart:io';
import 'dart:math';

import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/helpers/car.dart';
import 'package:all_in_one_app/Agent/pages/MyHistory.dart';
import 'package:all_in_one_app/User/helper/messagemodel.dart';
import 'package:all_in_one_app/User/helper/user.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path/path.dart' as path;

class UserRegister extends StatefulWidget {
  @override
  _UserRegisterState createState() => _UserRegisterState();
}

enum ImagePickCategory { camera, phone }

class _UserRegisterState extends State<UserRegister> {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  String agentType = "agentTypeagentType";

  bool _loading = false;
  List<QueryDocumentSnapshot> _categories;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final List<Message> messages = [];
  File _productImage;
  String name, dateofBirth, email, currentAddress, carno;
  String cartypes = '';
  String _phone = '';
  int randomnumber = 123;
  String finalrandom = "123";

  _UserRegisterState({
    Key key,
    this.name,
    this.dateofBirth,
    this.email,
    this.currentAddress,
    this.carno,
  });

  @override
  void initState() {
    _getCurrentLocation();
    loadCategories();
    getshared();
    super.initState();
  }

  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() async {
        currentAddress =
            "${place.locality}, ${place.postalCode}, ${place.country}";
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('CURRENTADRESS', currentAddress);
      });
    } catch (e) {
      print(e);
    }
  }

  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _phone = (prefs.getString('PHONENUMBER') ?? '');
    });
  }

  loadCategories() async {
    try {
      setState(() {
        _loading = true;
      });

      QuerySnapshot qsnap = await getCarType();
      setState(() {
        _categories = qsnap.docs;
        cartypes = qsnap.docs[0].id;
        _loading = false;
      });
    } catch (e) {
      print(e);
    }
  }

  onCreateuserBtnTap() async {
    try {
      if (name.isNotEmpty &&
          dateofBirth.isNotEmpty &&
          email.isNotEmpty &&
          currentAddress.isNotEmpty &&
          carno.isNotEmpty &&
          _productImage != null) {
        String productImageName = path.basename(_productImage.path);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('FULLNAME', name);
        prefs.setString('EMAIL', email);
        prefs.setString('CARNO', carno);

        uploadProductImage(
          productImageName,
          _productImage,
        ).then((downloadURL) {
          editPhoneNumber(
            downloadURL,
            finalrandom,
            name,
            _phone,
            dateofBirth,
            email,
            currentAddress,
            agentType,
            true,
          ).then((v) {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => MyHistory()));
          });
        }).catchError((e) {
          print(e);
          Navigator.pop(context, "failed");
        });
      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }

  _pickImage() async {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10.0),
          topRight: Radius.circular(10.0),
        ),
      ),
      builder: (ctx) {
        return Container(
          height: 200,
          color: Colors.transparent,
          child: ListView(
            children: [
              ListTile(
                leading: Icon(Icons.camera_alt),
                title: Text("Take a Picture from Camera"),
                onTap: () {
                  _takeImage(ImagePickCategory.camera);

                  Navigator.pop(ctx);
                },
              ),
              ListTile(
                leading: Icon(Icons.image),
                title: Text("Select a Picture from Device"),
                onTap: () {
                  _takeImage(ImagePickCategory.phone);
                  Navigator.pop(ctx);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  _takeImage(ImagePickCategory selection) async {
    try {
      final picker = ImagePicker();

      File _tempFile;

      // pick image
      if (selection == ImagePickCategory.camera) {
        final pickedFile = await picker.getImage(source: ImageSource.camera);
        _tempFile = File(pickedFile.path);
      } else if (selection == ImagePickCategory.phone) {
        final pickedFile = await picker.getImage(source: ImageSource.gallery);
        _tempFile = File(pickedFile.path);
      }

      // crop image

      File croppedFile = await ImageCropper.cropImage(
        sourcePath: _tempFile.path,
        compressQuality: 30,
        compressFormat: ImageCompressFormat.jpg,
        maxHeight: 1920,
        maxWidth: 1920,
      );

      print(croppedFile.lengthSync());

      setState(() {
        _productImage = croppedFile;
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    int MAX = 100;
    randomnumber = (new Random().nextInt(MAX));
    finalrandom = '$randomnumber';
    print(finalrandom);
    return Scaffold(
      backgroundColor: scaffoldBgColor,
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(fixPadding),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text('Register User account', style: greyHeadingStyle),
                heightSpace,
                heightSpace,
                heightSpace,

                Container(
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        blurRadius: 1.5,
                        spreadRadius: 1.5,
                        color: Colors.grey[200],
                      ),
                    ],
                  ),
                  child: TextField(
                    onChanged: (val) {
                      setState(() => name = val);
                    },
                    style: headingStyle,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(15.0),
                      hintText: 'Car Image',
                      hintStyle: greyHeadingStyle,
                      border: InputBorder.none,
                    ),
                  ),
                ),
                heightSpace,
                heightSpace,
                InkWell(
                  onTap: () {
                    _pickImage();
                  },
                  child: _productImage == null
                      ? Container(
                          height: 280,
                          width: 200.0,
                          color: Colors.grey,
                          child: Icon(Icons.add_a_photo),
                        )
                      : Image.file(
                          _productImage,
                          height: 280,
                          fit: BoxFit.cover,
                        ),
                ),
                heightSpace,
                heightSpace,
                // Full Name TextField Start
                Container(
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        blurRadius: 1.5,
                        spreadRadius: 1.5,
                        color: Colors.grey[200],
                      ),
                    ],
                  ),
                  child: TextField(
                    onChanged: (val) async {
                      setState(() => name = val);
                    },
                    style: headingStyle,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(15.0),
                      hintText: 'Full Name',
                      hintStyle: greyHeadingStyle,
                      border: InputBorder.none,
                    ),
                  ),
                ),
// email  TextField End
                heightSpace,
                heightSpace,
                Container(
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        blurRadius: 1.5,
                        spreadRadius: 1.5,
                        color: Colors.grey[200],
                      ),
                    ],
                  ),
                  child: TextField(
                    onChanged: (val) {
                      setState(() => dateofBirth = val);
                    },
                    style: headingStyle,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(15.0),
                      hintText: 'Date Of Birth',
                      hintStyle: greyHeadingStyle,
                      border: InputBorder.none,
                    ),
                  ),
                ),
// email  TextField End

                heightSpace,
                heightSpace,
                Container(
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        blurRadius: 1.5,
                        spreadRadius: 1.5,
                        color: Colors.grey[200],
                      ),
                    ],
                  ),
                  child: TextField(
                    onChanged: (val) {
                      setState(() => email = val);
                    },
                    style: headingStyle,
                    keyboardType: TextInputType.emailAddress,
                    obscureText: true,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(15.0),
                      hintText: 'Email Address',
                      hintStyle: greyHeadingStyle,
                      border: InputBorder.none,
                    ),
                  ),
                ),
                heightSpace,
                heightSpace,
//  Address TextField Start
                Container(
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        blurRadius: 1.5,
                        spreadRadius: 1.5,
                        color: Colors.grey[200],
                      ),
                    ],
                  ),
                  child: Column(children: <Widget>[
                    Row(children: <Widget>[
                      Icon(Icons.location_on),
                      SizedBox(
                        width: 8,
                      ),
                      Expanded(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                            Text(
                              'Location',
                              style: headingStyle,
                            ),
                            if (_currentPosition != null &&
                                currentAddress != null)
                              Text(
                                currentAddress,
                                style: headingStyle,
                              )
                          ])),
                    ])
                  ]),

/*TextField(
                    onChanged: (val){
                      setState(() => currentAddress = val);
                    },


                    style: headingStyle,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(15.0),
                      hintText: 'Location',
                      hintStyle: greyHeadingStyle,
                      border: InputBorder.none,
                    ),
                  ),*/
                ),
// Email Address TextField End
                heightSpace,
                heightSpace,
// car Type TextField Start
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    filled: true,
                    labelText: "Car Types",
                  ),
                  items: _categories.map((c) {
                    return DropdownMenuItem(
                      value: c.id,
                      child: Text(c.id),
                    );
                  }).toList(),
                  value: cartypes,
                  onChanged: (v) {
                    setState(() {
                      cartypes = v;
                    });
                  },
                ),
//car no
                heightSpace,
                heightSpace,
// Email Address TextField Start
                Container(
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        blurRadius: 1.5,
                        spreadRadius: 1.5,
                        color: Colors.grey[200],
                      ),
                    ],
                  ),
                  child: TextField(
                    onChanged: (val) {
                      setState(() => carno = val);
                    },
                    style: headingStyle,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(15.0),
                      hintText: 'Car No',
                      hintStyle: greyHeadingStyle,
                      border: InputBorder.none,
                    ),
                  ),
                ),
//adreess
                heightSpace,
                heightSpace,
                Container(
                  child: SizedBox(
                    height: 50.0,
                    width: width - (fixPadding * 2.0),
                    child: RaisedButton(
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      onPressed: () async {
                        onCreateuserBtnTap();
/*   String productImageName = path.basename(_productImage.path);

                        */ /*SharedPreferences prefs = await SharedPreferences.getInstance();
                        prefs.setString('NAME', name);
                        prefs.setString('EMAIL', email);
                        prefs.setString('PASSWORD', password);*/ /*
                        if (name.length > 0 && dateofBirth.length > 0 && email.length > 0 && adress.length > 0 && carno.length > 0)  {

                          uploadProductImage(
                            productImageName,
                            _productImage,
                          ).then((downloadURL) {
                            editPhoneNumber(
                                name, dateofBirth, email, adress, carno, true)
                                .then((v) {
                              Navigator.pop(context, "added");
                            });
                          }).catchError((e) => print(e));

                        } else {
                          showMyDialog(
                            context: context,
                            title: "oops",
                            description: "Provide All Category ",
                          );
                        }*/
                      },
                      color: primaryColor,
                      child: Text(
                        'Continue',
                        style: wbuttonWhiteTextStyle,
                      ),
                    ),
                  ),
                ),
                heightSpace,
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildMessage(Message message) => ListTile(
        title: Text(message.title),
        subtitle: Text(message.body),
      );
}
