import 'package:all_in_one_app/Agent/AgentSplashScreen.dart';
import 'package:all_in_one_app/carWasher/pages/splashScreen.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/User/pages/onboarding/onboarding.dart';
import 'Admin/SplashScreen.dart';

class MainTab extends StatefulWidget {
  @override
  _MainTabState createState() => _MainTabState();
}

class _MainTabState extends State<MainTab> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:InkWell(
        child: Stack(
            children: [
            Container(
              constraints: new BoxConstraints.expand(
              ),
              padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/homepage.png"),
                  fit: BoxFit.cover,
                ),
              ),
          child: new Stack(
              children: <Widget>[
            new Positioned(
              left: 10.0,
              top: 0.0,
              child: Column(
                children: <Widget>[
                Container(
                height: 300.0,
                padding: EdgeInsets.only(top: 150.0, right: 100.0),
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => OnBoarding()),
                    );
                  },
                  child: Container(
                    height: 150.0,
                    width: 150.0,
                    padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image(
                            image: AssetImage("assets/images/two.jpg"),
                            fit: BoxFit.cover),
                        Padding(
                          padding: const EdgeInsets.only(top: 6.0),
                          child: Text(
                            'User',
                            textAlign: TextAlign.center,
                            style: UserTextStyle,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

              ],
          ),
            ),
              new Positioned(
                right:10.0,
                bottom:10.0,
                child:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    Container(
                    height: 80.0,
                    padding: EdgeInsets.only(left: 230.0),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SplashScreen()));
                      },
                      child: Container(
                        height: 30.0,
                        width: 30.0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Image(
                              image: AssetImage("assets/admin.png"),
                              fit: BoxFit.fill,
                            ),
                            Text(
                              'Admin',
                              textAlign: TextAlign.center,
                              style: MainTextStyle,
                            ),
                          ],
                        ),
                      ),
                    )),
             ], ),
              ),
                new Positioned(
                  right:120.0,
                  bottom:10.0,
                  child:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          height: 80.0,
                          padding: EdgeInsets.only(left: 230.0),
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => WasherSplashScreen()));
                            },
                            child: Container(
                              height: 30.0,
                              width: 40.0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Image(
                                    image: AssetImage("assets/carwasher.png"),
                                    fit: BoxFit.fill,
                                  ),
                                  Text(
                                    'Washer',
                                    textAlign: TextAlign.center,
                                    style: MainTextStyle,
                                  ),
                                ],
                              ),
                            ),
                          )),
                    ], ),
                ),
                new Positioned(
                  right:65.0,
                  bottom:10.0,
                  child:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          height: 80.0,
                          padding: EdgeInsets.only(left: 230.0),
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AgentSplashScreen()));
                            },
                            child: Container(
                              height: 30.0,
                              width: 30.0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Image(
                                    image: AssetImage("assets/agent.png"),
                                    fit: BoxFit.fill,
                                  ),
                                  Text(
                                    'Agent',
                                    textAlign: TextAlign.center,
                                    style: MainTextStyle,
                                  ),
                                ],
                              ),
                            ),
                          )),
                    ], ),
                ),
            ]
        ),
      ),

     /* body: Container(
        padding: EdgeInsets.all(40),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: Colors.black,
            width: 7, //                   <--- border width here
          ),
          image: DecorationImage(
            image: AssetImage("assets/changerollsplash.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: <Widget>[
            Container(
              height: 300.0,
              padding: EdgeInsets.only(top: 150.0, right: 100.0),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => OnBoarding()),
                  );
                },
                child: Container(
                  height: 150.0,
                  width: 150.0,
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                          image: AssetImage("assets/images/two.jpg"),
                          fit: BoxFit.cover),
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text(
                          'User',
                          textAlign: TextAlign.center,
                          style: UserTextStyle,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    height: 80.0,
                    padding: EdgeInsets.only(left: 230.0),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SplashScreen()));
                      },
                      child: Container(
                        height: 30.0,
                        width: 30.0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Image(
                              image: AssetImage("assets/admin.png"),
                              fit: BoxFit.fill,
                            ),
                            Text(
                              'Admin',
                              textAlign: TextAlign.center,
                              style: MainTextStyle,
                            ),
                          ],
                        ),
                      ),
                    )),
                Container(
                    height: 80.0,
                    padding: EdgeInsets.only(left: 230.0),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => WasherSplashScreen()));
                      },
                      child: Container(
                        height: 50.0,
                        width: 50.0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Image(
                              image: AssetImage("assets/carwasher.png"),
                              fit: BoxFit.fitWidth,
                            ),
                            Text(
                              'Washer',
                              textAlign: TextAlign.center,
                              style: MainTextStyle,
                            ),
                          ],
                        ),
                      ),
                    )),
                Container(
                    height: 70.0,
                    padding: EdgeInsets.only(left: 230.0),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AgentSplashScreen()));
                      },
                      child: Container(
                        height: 30.0,
                        width: 30.0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Image(
                              image: AssetImage("assets/agent.png"),
                              fit: BoxFit.fitWidth,
                            ),
                            Text(
                              'Agent',
                              textAlign: TextAlign.center,
                              style: MainTextStyle,
                            ),
                          ],
                        ),
                      ),
                    )),
              ],
            )
          ],
        ),

        *//*   child: Column(
            children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      gradient: LinearGradient(
                          begin: Alignment.bottomRight,
                          colors: [
                            Colors.black.withOpacity(.4),
                            Colors.black.withOpacity(.2),
                          ]
                      )
                  ),

              ),
              SizedBox(height: 40,),
              Expanded(
                  child: GridView.count(
                    childAspectRatio: 1.0,
                    padding: EdgeInsets.only(top:200.0,left: 16, right: 16),
                    crossAxisCount: 2,
                    crossAxisSpacing: 18,
                    mainAxisSpacing: 18,
                    children:<Widget>[
                      Card(

                      color: Colors.transparent,
                      elevation: 0,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => SplashScreen()));


                          // Function is executed on tap.
                        },

                        child: Container(

                          width: double.infinity,
                          padding: EdgeInsets.all(20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                             Image(
                                  image: AssetImage("assets/images/two.jpg"),
                                  fit: BoxFit.cover
                              ),
                            Padding(
                              padding: const EdgeInsets.only(top: 6.0),
                              child: Text('Admin',textAlign: TextAlign.center,),
                              ),
                              ],
                            ),
                          ),

                      ),
                    ),
                      Card(

                        color: Colors.transparent,
                        elevation: 0,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) => UserSplashScreen()));
                          },

                          child: Container(

                            width: double.infinity,
                            padding: EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Image(
                                    image: AssetImage("assets/images/five.jpg"),
                                    fit: BoxFit.fitWidth,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 6.0),
                                  child: Text('User',textAlign: TextAlign.center,),
                                ),
                              ],
                            ),
                          ),

                        ),
                      ),
                      Card(
                        color: Colors.transparent,
                        elevation: 0,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) => WasherSplashScreen()));
                          },

                          child: Container(

                            width: double.infinity,
                            padding: EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Image(
                                    image: AssetImage("assets/images/two.jpg"),
                                    fit: BoxFit.cover
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 6.0),
                                  child: Text('Washer'),
                                ),
                              ],
                            ),
                          ),

                        ),
                      ),
                      Card(

                        color: Colors.transparent,
                        elevation: 0,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) => AgentSplashScreen()));
                          },


                          child: Container(

                            width: double.infinity,
                            padding: EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Image(
                                    image: AssetImage("assets/images/five.jpg"),
                                    fit: BoxFit.cover
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Text('Agent'),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
              )
            ],
          ),*//*
      ),
      */
      ],),)
    );
  }
}
