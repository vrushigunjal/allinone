
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'constant/constant.dart';
import 'maintab.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();

    Timer(
        Duration(seconds: 3),
            () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => MainTab()),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FittedBox(
        child: Image.asset("assets/user/userprofile/loginicon.png"),
        fit: BoxFit.fill,
      ),

     /* backgroundColor: scaffoldBgColor,
      body: Padding(
        padding: EdgeInsets.all(fixPadding),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
          Image.asset(
            'assets/user/userprofile/loginicon.png',
            fit: BoxFit.cover,
          ),
             *//* Image.asset(
                'assets/user/userprofile/loginicon.png',
                fit: BoxFit.cover,
              ),*//*

            ],
          ),
        ),
      ),*/
    );
  }
}
