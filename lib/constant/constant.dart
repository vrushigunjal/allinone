import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Color scaffoldBgColor = Color(0xFFF4F4F4);
Color primaryColor = Color(0xFFF1C40F);
Color darkPrimaryColor = Color(0xFF2E4053);
Color greyColor = Colors.grey;
Color whiteColor = Colors.white;
Color blackColor = Color(0xFF000000);
Color lightGreyColor = Color(0xFFF1C40F);

double fixPadding = 10.0;
double fixContanierpadding = 5.0;

SizedBox heightSpace = SizedBox(height: 10.0);
SizedBox widthSpace = SizedBox(width: 10.0);

TextStyle bottomBarItemStyle = TextStyle(
  color: greyColor,
  fontSize: 12.0,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w500,
);

TextStyle bigHeadingStyle = TextStyle(
  fontSize: 22.0,
  color: darkPrimaryColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w600,
);

TextStyle headingStyle = TextStyle(
  fontSize: 22.0,
  color: primaryColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w500,
);
TextStyle headingwasherprofileStyle = TextStyle(
  fontSize: 22.0,
  color: blackColor,
  fontFamily: 'Lato-Bold',
  fontWeight: FontWeight.w500,
);

TextStyle rsStyle = TextStyle(
  fontSize: 14.0,
  color: Colors.black,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w400,
);
TextStyle boldStyle = TextStyle(
  fontSize: 14.0,
  color: Colors.black,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w700,
);

TextStyle priseStyle = TextStyle(
  fontSize: 14.0,
  color: Colors.green,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w700,
);
TextStyle dashpriseStyle = TextStyle(
  fontSize: 14.0,
  color: blackColor,
  decoration: TextDecoration.lineThrough,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w700,
);

TextStyle headingWasherStyle = TextStyle(
  fontSize: 14.0,
  color: Colors.black,
  fontFamily: 'Lato-Bold',
  fontWeight: FontWeight.w900,
);

TextStyle greyHeadingStyle = TextStyle(
  fontSize: 14.0,
  color: greyColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w500,
);
TextStyle greyHeadingStyleaa = TextStyle(
  fontSize: 16.0,
  color: greyColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w500,
);
TextStyle WasherearnHeadingStyle = TextStyle(
  fontSize: 18.0,
  color: blackColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w700,
);
TextStyle blueTextStyle = TextStyle(
  fontSize: 18.0,
  color: Colors.blue,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w400,
);

TextStyle UserTextStyle = TextStyle(
  fontSize: 18.0,
  color: Colors.black,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w700,
);
TextStyle MainTextStyle = TextStyle(
  fontSize: 10.0,
  color: Colors.yellow,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w700,
);
TextStyle whiteHeadingStyle = TextStyle(
  fontSize: 22.0,
  color: darkPrimaryColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w500,
);

TextStyle whiteSubHeadingStyle = TextStyle(
  fontSize: 14.0,
  color: darkPrimaryColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.normal,
);

TextStyle wbuttonWhiteTextStyle = TextStyle(
  fontSize: 16.0,
  color: darkPrimaryColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w500,
);

TextStyle wwasherbuttonWhiteTextStyle = TextStyle(
  fontSize: 14.0,
  color: darkPrimaryColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w700,
);

TextStyle buttonBlackTextStyle = TextStyle(
  fontSize: 16.0,
  color: darkPrimaryColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w700,
);

TextStyle moreStyle = TextStyle(
  fontSize: 14.0,
  color: primaryColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w500,
);

TextStyle priceStyle = TextStyle(
  fontSize: 18.0,
  color: primaryColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.bold,
);

TextStyle lightGreyStyle = TextStyle(
  fontSize: 15.0,
  color: darkPrimaryColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w500,
);

// List Item Style Start
TextStyle listItemTitleStyle = TextStyle(
  fontSize: 14.0,
  color: blackColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w700,
);

/*Text('View all', style: TextStyle(
fontWeight: FontWeight.w700,color: Colors.black,
fontSize: 16),),*/
TextStyle listItemSubTitleStyle = TextStyle(
  fontSize: 12.0,
  color: blackColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.normal,
);

// List Item Style End

// AppBar Style Start
TextStyle appbarHeadingStyle = TextStyle(
  color: blackColor,
  fontSize: 14.0,
  fontFamily:  'Lato',
  fontWeight: FontWeight.bold,
);



TextStyle appbarSubHeadingStyle = TextStyle(
  color: darkPrimaryColor,
  fontSize: 13.0,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w500,
);

TextStyle bigWhiteHeadingStyle = TextStyle(
  fontSize: 24.0,
  color: darkPrimaryColor,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w600,
);
// AppBar Style End

// Search text style start
TextStyle searchTextStyle = TextStyle(
  color: darkPrimaryColor,
  fontSize: 16.0,
  fontFamily: 'Lato',
  fontWeight: FontWeight.w500,
);
