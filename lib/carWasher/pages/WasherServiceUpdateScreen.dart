import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/Admin/config/currency.dart';
import 'package:all_in_one_app/Admin/helpers/category.dart';
import 'package:all_in_one_app/Admin/helpers/product.dart';
import 'package:all_in_one_app/carWasher/helper/Services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class WasherServiceUpdateScreen extends StatefulWidget {
  final DocumentSnapshot document;

  WasherServiceUpdateScreen({@required this.document});

  @override
  _WasherServiceUpdateScreenState createState() => _WasherServiceUpdateScreenState();
}

class _WasherServiceUpdateScreenState extends State<WasherServiceUpdateScreen> {
  List<QueryDocumentSnapshot> _categories;

  bool _loading = false;
  String category = '';
  String Total = "0" ;
  TextEditingController productNameController = TextEditingController();
  TextEditingController productDescriptionController = TextEditingController();
  TextEditingController salePriceController = TextEditingController();
  @override
  void dispose() {
    productNameController.dispose();
    productDescriptionController.dispose();
    salePriceController.dispose();

    super.dispose();
  }
  @override
  void initState() {
    productNameController.text = widget.document.data()["title"];
    productDescriptionController.text = widget.document.data()["decription"];
    salePriceController.text = widget.document.data()["salePrice"].toString();
    super.initState();
  }

  onUpdateProductBtnTap() async {
    try {
      String productName = productNameController.text;
      String description = productDescriptionController.text;
      if (productName.isNotEmpty != null) {
        double salePrice = double.parse(salePriceController.text);
        double lastprice = double.parse(Total);





        setState(() {
          _loading = true;
        });
        print("$productName");

        updatewasherCategory(
          widget.document.id,
          productName,
            description,
            salePrice,
            lastprice,
            category
        ).then((v) {
          Navigator.pop(context, "done");
        }).catchError((e) {
          Navigator.pop(context, "failed");
          print(e);
        });
      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }

  onDeleteProductBtnTap() async {
    await showDialog(
      context: context,
      builder: (ctx) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18),
          ),
          title: Text("Are you sure?"),
          content: Text(
              "Once you press delete, we will delete it from our storage, this process is irreversible."),
          actions: [
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(ctx);
              },
            ),
            FlatButton(
              textColor: Colors.red,
              child: Text("Delete Permanently"),
              onPressed: () {
                deleteProduct(widget.document.id,
                    widget.document.data()["serviceImage"])
                    .then((value) {
                  Navigator.pop(ctx);
                }).catchError((e) {
                  print(e);
                  Navigator.pop(ctx);
                });
              },
            ),
          ],
        );
      },
    );
    Navigator.pop(context, "done");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Product"),
        actions: [
          IconButton(
            icon: Icon(Icons.delete_forever),
            onPressed: () {
              onDeleteProductBtnTap();
            },
          ),
        ],
      ),
      body: _loading == true
          ? Center(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      )
          : Container(
        padding: EdgeInsets.symmetric(
          horizontal: 12,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              FadeInImage(
                placeholder: AssetImage("assets/admin/placeholder.png"),
                image:
                NetworkImage(widget.document.data()["serviceImage"]),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                decoration: InputDecoration(
                  filled: true,
                  labelText: "Service Name",
                ),
                maxLength: 120,
                controller: productNameController,
              ),
              TextField(
                decoration: InputDecoration(
                  filled: true,
                  labelText: "Price",
                  prefixIcon: Icon(FontAwesomeIcons.moneyBillAlt),
                ),
                keyboardType: TextInputType.numberWithOptions(
                  decimal: true,
                ),
                controller: salePriceController,
              ),

              TextField(
                decoration: InputDecoration(
                  filled: true,
                  labelText: "Description",
                ),
                maxLines: 6,
                controller: productDescriptionController,
                maxLength: 600,
              ),
              SizedBox(
                height: 20,
              ),

              if (_categories != null && _categories.length > 0) ...[
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    filled: true,
                    labelText: "Types Of Car",
                  ),
                  items: _categories.map((c) {
                    return DropdownMenuItem(
                      value: c.id,
                      child: Text(c.id),
                    );
                  }).toList(),
                  value: category,
                  onChanged: (v) {
                    setState(() {
                      category = v;
                    });
                  },
                ),
              ],
              SizedBox(
                height: 20,
              ),

              RaisedButton(
                onPressed: () {
                  onUpdateProductBtnTap();
                },
                child: Text("SAVE"),
              ),
              SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
