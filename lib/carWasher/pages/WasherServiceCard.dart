import 'package:all_in_one_app/Admin/config/currency.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class WasherServiceCard extends StatelessWidget {
  final QueryDocumentSnapshot product;
  final Function onTap;

  WasherServiceCard({@required this.product, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    DateTime createdAt = product['created_at'].toDate();
    String formattedDate =
        "${createdAt.year}/${createdAt.month}/${createdAt.day} ${createdAt.hour}:${createdAt.minute}:${createdAt.second}";
    return InkWell(
      onTap: onTap,
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              color: Colors.grey[100],
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  blurRadius: 1.5,
                  spreadRadius: 1.5,
                  color: Colors.grey,
                ),
              ],
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: FadeInImage(
                    image: NetworkImage(product.data()["serviceImage"]),
                    placeholder: AssetImage("assets/admin/placeholder.png"),
                    width: 100,
                    height: 100,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  width: 14,
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        product.data()["title"],
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                      Text(
                        product.data()["decription"],
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                      Text(
                        //"$\Rs$product.data()["salePrice"].toString()}",
                        "$currencySymbol${product.data()['salePrice'].toString()}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                      Text(
                        formattedDate,
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
