


import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/carWasher/helper/Services.dart';
import 'package:all_in_one_app/carWasher/pages/CreateServicesScreen.dart';
import 'package:all_in_one_app/carWasher/pages/WasherServiceCard.dart';
import 'package:all_in_one_app/carWasher/pages/WasherServiceUpdateScreen.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddService extends StatefulWidget {
  @override
  _AddServiceState createState() => _AddServiceState();
}


class _AddServiceState extends State<AddService> {
  bool _isLoading = false;
  bool _isEmpty = false;
  String Uid = "123";
  List<QueryDocumentSnapshot> products;
  QueryDocumentSnapshot lastDocument;

  ScrollController _scrollController;

  @override
  void initState() {
    getshared();

    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more uploads");
          loadMoreProducts();
        }
      });
    super.initState();
  }
  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      Uid = (prefs.getString('UID') ?? '');
      print(Uid);
      loadProducts();
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  loadProducts() async {
    setState(() {
      _isLoading = true;
    });

    QuerySnapshot qsnap = await getwasgerCategories(Uid);
    if (qsnap.size > 0) {
      setState(() {
        products = qsnap.docs;
        lastDocument = qsnap.docs.last;
        _isLoading = false;
        _isEmpty = false;
      });
    } else {
      setState(() {
        _isEmpty = true;
        _isLoading = false;
      });
    }
  }

  loadMoreProducts() async {
    QuerySnapshot qsnap = await getMoreCategories(lastDocument);
    if (qsnap.size > 0) {
      setState(() {
        products.addAll(qsnap.docs);
        lastDocument = qsnap.docs.last;
        _isLoading = false;
        _isEmpty = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Manage Services"),
      ),
      floatingActionButton: FloatingActionButton.extended(
        elevation: 20.0,
        onPressed: () async {
          try {
            String result = await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (ctx) => CreateServiceScreen(),
                fullscreenDialog: true,
              ),
            );

            if (result == "done") {
              loadProducts();
            } else if (result == "failed") {
              showMyDialog(
                context: context,
                title: "oops",
                description: "Something went wrong",
              );
            }
          } catch (e) {
            print(e);
          }
        },
        label: Text("Create Services"),
        icon: Icon(Icons.add),
      ),
      body: Container(
        child: _isLoading == true
            ? Center(
          child: SpinKitChasingDots(
            color: primaryColor,
            size: 50,
          ),
        )
            : _isEmpty == true
            ? Empty(text: "Ooops! No Service found! ")
            : RefreshIndicator(
          onRefresh: () async {
            return loadProducts();
          },
          child: ListView.separated(
            controller: _scrollController,
            itemCount: products.length,
            separatorBuilder: (ctx, i) {
              return Divider();
            },
            itemBuilder: (ctx, i) {
              return WasherServiceCard(
                product: products[i],
                onTap: () async {
                  String result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => WasherServiceUpdateScreen(
                        document: products[i],
                      ),
                    ),
                  );
                  if (result == "done") {
                    loadProducts();
                  }
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
