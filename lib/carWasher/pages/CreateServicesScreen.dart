import 'dart:io';

import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/Admin/helpers/car.dart';
import 'package:all_in_one_app/carWasher/helper/Services.dart';
import 'package:all_in_one_app/carWasher/helper/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:shared_preferences/shared_preferences.dart';

class CreateServiceScreen extends StatefulWidget {
  @override
  _CreateServiceScreenState createState() => _CreateServiceScreenState();
}

enum ImagePickCategory { camera, phone }

class _CreateServiceScreenState extends State<CreateServiceScreen> {
  File _productImage;

  List<QueryDocumentSnapshot> _categories;

  bool _loading = false;

  // product fields
  String category = '';
  String centerName = 'test';
  String Total = "0";

  TextEditingController productNameController = TextEditingController();
  TextEditingController centerNameController = TextEditingController();
  TextEditingController productDescriptionController = TextEditingController();
  TextEditingController priceController = TextEditingController();

  // product fields

  @override
  void dispose() {
    productNameController.dispose();
    priceController.dispose();
    productDescriptionController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    loadCategories();
    loadUserDetails();
    super.initState();
  }
  loadUserDetails() async {
    try {
      DocumentSnapshot user = await getUser();

      centerName = user.data()["Centername"] ?? '';

      setState(() {

        centerNameController.text = centerName;

      });
    } catch (e) {
      print(e);
    }
  }
  loadCategories() async {
    try {
      setState(() {
        _loading = true;
      });

      QuerySnapshot qsnap = await getCarType();
      setState(() {
        _categories = qsnap.docs;
        category = qsnap.docs[0].id;
        _loading = false;
      });
    } catch (e) {
      print(e);
    }
  }

  onCreateProductBtnTap() async {
    try {
      String productName = productNameController.text;
      String centerName = centerNameController.text;
      String description = productDescriptionController.text;
      if (productName.isNotEmpty != null) {
        String productImageName = path.basename(_productImage.path);
        double salePrice = double.parse(priceController.text);
        double total = double.parse(Total);

        // showMyLoadingModal(
        //   context,
        //   "Please wait...",
        // );
        setState(() {
          _loading = true;
        });
        print("$productName ");

        uploadServiceImage(
          productImageName,
          _productImage,
        ).then((downloadURL) {
          createCategory(downloadURL, productName, description, salePrice,
                  total, centerName, category)
              .then((v) {
            Navigator.pop(context, "done");
          });
        }).catchError((e) {
          print(e);
          Navigator.pop(context, "failed");
        });
      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }

  _pickImage() async {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10.0),
          topRight: Radius.circular(10.0),
        ),
      ),
      builder: (ctx) {
        return Container(
          height: 200,
          color: Colors.transparent,
          child: ListView(
            children: [
              ListTile(
                leading: Icon(Icons.camera_alt),
                title: Text("Take a Picture from Camera"),
                onTap: () {
                  _takeImage(ImagePickCategory.camera);

                  Navigator.pop(ctx);
                },
              ),
              ListTile(
                leading: Icon(Icons.image),
                title: Text("Select a Picture from Device"),
                onTap: () {
                  _takeImage(ImagePickCategory.phone);
                  Navigator.pop(ctx);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  _takeImage(ImagePickCategory selection) async {
    try {
      final picker = ImagePicker();

      File _tempFile;

      // pick image
      if (selection == ImagePickCategory.camera) {
        final pickedFile = await picker.getImage(source: ImageSource.camera);
        _tempFile = File(pickedFile.path);
      } else if (selection == ImagePickCategory.phone) {
        final pickedFile = await picker.getImage(source: ImageSource.gallery);
        _tempFile = File(pickedFile.path);
      }

      // crop image

      File croppedFile = await ImageCropper.cropImage(
        sourcePath: _tempFile.path,
        compressQuality: 30,
        compressFormat: ImageCompressFormat.jpg,
        maxHeight: 1920,
        maxWidth: 1920,
      );

      print(croppedFile.lengthSync());

      setState(() {
        _productImage = croppedFile;
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create Service"),
      ),
      body: _loading == true
          ? Center(
              child: SpinKitChasingDots(
                color: primaryColor,
                size: 50,
              ),
            )
          : Container(
              padding: EdgeInsets.symmetric(
                horizontal: 12,
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    InkWell(
                      onTap: () {
                        _pickImage();
                      },
                      child: _productImage == null
                          ? Container(
                              height: 280,
                              color: Colors.grey,
                              child: Icon(Icons.add_a_photo),
                            )
                          : Image.file(
                              _productImage,
                              height: 280,
                              fit: BoxFit.cover,
                            ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        labelText: "Service Name",
                      ),
                      maxLength: 120,
                      controller: productNameController,
                    ),
                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        labelText: "You Car wash center Name",
                      ),
                      maxLength: 120,
                      controller: centerNameController,
                    ),
                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        labelText: "Price",
                        prefixIcon: Icon(FontAwesomeIcons.moneyBillAlt),
                      ),
                      keyboardType: TextInputType.numberWithOptions(
                        decimal: true,
                      ),
                      controller: priceController,
                    ),
                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        labelText: "Description",
                      ),
                      maxLines: 6,
                      controller: productDescriptionController,
                      maxLength: 600,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    if (_categories != null && _categories.length > 0) ...[
                      DropdownButtonFormField(
                        decoration: InputDecoration(
                          filled: true,
                          labelText: "Types Of Car",
                        ),
                        items: _categories.map((c) {
                          return DropdownMenuItem(
                            value: c.id,
                            child: Text(c.id),
                          );
                        }).toList(),
                        value: category,
                        onChanged: (v) {
                          setState(() {
                            category = v;
                          });
                        },
                      ),
                    ],
                    SizedBox(
                      height: 20,
                    ),
                    RaisedButton(
                      onPressed: () {
                        onCreateProductBtnTap();
                      },
                      child: Text("CREATE SERVICE"),
                    ),
                    SizedBox(
                      height: 100,
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
