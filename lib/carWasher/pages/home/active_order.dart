
import 'package:all_in_one_app/User/helper/ServiceOrderConfirm.dart';
import 'package:all_in_one_app/carWasher/pages/home/history.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/carWasher/pages/map.dart';
import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ActiveOrder extends StatefulWidget {


  @override
  _ActiveOrderState createState() => _ActiveOrderState();
}

class _ActiveOrderState extends State<ActiveOrder> {


  List<QueryDocumentSnapshot>products;
  QueryDocumentSnapshot lastDocument;

  ScrollController _scrollController;
  String tab = 'Accepted';
  List<DocumentSnapshot> orders;
  var id = '123';
  var CenterUid = '123';

  @override
  void initState() {
    super.initState();
    getshared();
  }
  Future getid() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore
        .collection("WasherRegistration")
        .where("uid", isEqualTo: CenterUid)
        .get();
    qn.documents.forEach((qn) {
      CenterUid = qn.documentID;
      print(CenterUid);

    });
    return qn.documents;
  }

  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      CenterUid = (prefs.getString('UID') ?? '');

    });
  }


  Future getprchaseServiceOrder() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection("ordersService")
        .where("order", isEqualTo: "Accepted")
        .where("CenterUid", isEqualTo: CenterUid)
        .get();
    qn.documents.forEach((qn){
      id = qn.documentID;
    });
    return qn.documents;
  }

  final deliveryList = [
    {
      'orderId': 'OID123456789',
      'paymentMode': 'Cash on Delivery',
      'payment': '8.50',
      'restaurantAddress': '28 Mott Stret',
      'deliveryAddress': '56 Andheri East'
    },
    {
      'orderId': 'OID123789654',
      'paymentMode': 'Payed',
      'payment': '12.50',
      'restaurantAddress': '91 Opera Street',
      'deliveryAddress': '231 Abc Circle'
    },
    {
      'orderId': 'OID957546521',
      'paymentMode': 'Payed',
      'payment': '15.00',
      'restaurantAddress': '28 Mott Stret',
      'deliveryAddress': '91 Yogi Circle'
    },
    {
      'orderId': 'OID652347952',
      'paymentMode': 'Cash on Delivery',
      'payment': '7.90',
      'restaurantAddress': '28 Mott Stret',
      'deliveryAddress': '54 Xyz Residency'
    },
    {
      'orderId': 'OID658246972',
      'paymentMode': 'Cash on Delivery',
      'payment': '19.50',
      'restaurantAddress': '29 Bar Street',
      'deliveryAddress': '56 Andheri East'
    }
  ];



  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;


    viewOrder(docId) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          // return object of type Dialog
          return Dialog(
            elevation: 0.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Wrap(
              children: <Widget>[
                Container(
                  width: width,
                  height: height/1.2,
                  child: ListView(
                    children: <Widget>[
                      Container(
                        width: width,
                        padding: EdgeInsets.all(fixPadding),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10.0),
                            topLeft: Radius.circular(10.0),
                          ),
                        ),
                        child: Text(
                          '$id',
                          style: wbuttonWhiteTextStyle,
                        ),
                      ),

                      // Order Start
                      Container(
                        margin: EdgeInsets.all(fixPadding),
                        decoration: BoxDecoration(
                          color: whiteColor,
                          borderRadius: BorderRadius.circular(5.0),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              blurRadius: 1.5,
                              spreadRadius: 1.5,
                              color: Colors.grey[200],
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(fixPadding),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'ServiceName',
                                        style: listItemTitleStyle,
                                      ),
                                      Text(
                                        docId["productName"],
                                        style: listItemTitleStyle,
                                      ),
                                    ],
                                  ),
                                  heightSpace,
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Price',
                                        style: listItemTitleStyle,
                                      ),
                                      Text(
                                        '${docId['Washerprice']}',
                                        style: listItemTitleStyle,
                                      ),

                                    ],
                                  ),
                                  heightSpace,
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Date',
                                        style: listItemTitleStyle,
                                      ),
                                      Text(docId["updated_at"].toDate().toString(),
                                          maxLines: 2,
                                          style: listItemTitleStyle),
                                    ],
                                  ),

                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      //Order End
                      //serviceimage
                      Container(
                        margin: EdgeInsets.all(fixPadding),
                        decoration: BoxDecoration(
                          color: whiteColor,
                          borderRadius: BorderRadius.circular(5.0),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              blurRadius: 1.5,
                              spreadRadius: 1.5,
                              color: Colors.grey[200],
                            ),
                          ],
                        ),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[

                              Padding(
                                padding: EdgeInsets.all(fixPadding),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[

                                        ClipRRect(
                                          borderRadius: BorderRadius.circular(8),
                                          child: FadeInImage(
                                            image: NetworkImage(docId["Image"]),
                                            placeholder: AssetImage("assets/placeholder.png"),
                                            width: 100,
                                            height: 100,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            ]
                        ),
                      ),
                      //ImageEnd

                      // Customer Start
                      Container(
                        margin: EdgeInsets.all(fixPadding),
                        decoration: BoxDecoration(
                          color: whiteColor,
                          borderRadius: BorderRadius.circular(5.0),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              blurRadius: 1.5,
                              spreadRadius: 1.5,
                              color: Colors.grey[200],
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(fixPadding),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(5.0),
                                    topLeft: Radius.circular(5.0),
                                  )),
                              child: Text(
                                'Customer Detailes',
                                style: buttonBlackTextStyle,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(fixPadding),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Name',
                                        style: listItemTitleStyle,
                                      ),
                                      Text(
                                        docId["Name"],
                                        style: listItemTitleStyle,
                                      ),
                                    ],
                                  ),
                                  heightSpace,
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      /*  Icon(
                                        EvaIcons.giftOutline,
                                        color: Colors.green,
                                      ),*/
                                      Text(
                                        'Phone',
                                        style: listItemTitleStyle,
                                      ),
                                      Text(
                                        docId["Phone"],
                                        style: listItemTitleStyle,
                                      ),

                                    ],
                                  ),
                                  heightSpace,
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Location',
                                        style: listItemTitleStyle,
                                      ),
                                      widthSpace,
                                      Container(
                                        width: ((width - 2 * 13) / 4.0),
                                        child: Text(
                                          docId["Address"],
                                          maxLines: 6,
                                          overflow: TextOverflow.ellipsis,
                                          style: listItemTitleStyle,
                                        ),
                                      ),
                                      /* Container(
                                              width:
                                              ((width - fixPadding * 13) / 4.0),
                                              child: Text(
                                                docId["Address"],
                                                maxLines: 6,
                                                overflow: TextOverflow.ellipsis,
                                                style: listItemTitleStyle,
                                              ),
                                            ),*/
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      //Customer End


                      InkWell(
                        onTap: () async{
                          var status = "Done";
                          if (status != "") {
                            await updateServiceOrderStatus(id, status);
                            Navigator.pop(context, "done");
                          }
String Code = "+91";
                          FlutterOpenWhatsapp.sendSingleMessage(Code+docId["Phone"], "Complete Your Car");
          MaterialPageRoute(
          builder: (context) => History());
          },
                          // Navigator.push(context, MaterialPageRoute(builder: (context)=>Map()));

                        child: Container(
                          width: width,
                          alignment: Alignment.center,
                          margin: EdgeInsets.all(fixPadding),
                          padding: EdgeInsets.all(fixPadding),
                          decoration: BoxDecoration(
                            color: primaryColor,
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Text(
                            'Finish',
                            style: wbuttonWhiteTextStyle,
                          ),
                        ),
                      ),
                      heightSpace,
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      );
    }

    return FutureBuilder(
        future: getprchaseServiceOrder(),
        builder: (_, snapshot) {
          return ListView.builder(
            itemCount: snapshot.data.length,
            physics: BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final item = snapshot.data[index];
        return Container(
          padding: EdgeInsets.all(fixPadding),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        blurRadius: 1.5,
                        spreadRadius: 1.5,
                        color: Colors.grey,
                      ),
                    ],
                  ),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(fixPadding),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[

                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('$id', style: headingWasherStyle),
                                    heightSpace,
                                    Row(children: [
                                      Text("Service:",
                                          style: headingWasherStyle),
                                      widthSpace,
                                      Text(item['productName'],
                                          style: rsStyle),
                                    ]),
                                    heightSpace,
                                    heightSpace,
                                    Text(item["updated_at"].toDate().toString(),
                                        maxLines: 2,
                                        style: rsStyle),
                                    heightSpace,
                                    heightSpace,
                                    Row(
                                      children: [
                                        getDot(),
                                        Text(item['order'],
                                            style: rsStyle),
                                      ],
                                    )
                                  ],
                                ),
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('\Rs ${item['Washerprice']}',
                                    style: rsStyle),
                                heightSpace,
                                heightSpace,
                                heightSpace,
                                heightSpace,
                                Container(
                                  child: SizedBox(
                                    height: 40.0,
                                    width: 80.0,
                                    child: RaisedButton(
                                      elevation: 5.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(5.0),
                                      ),
                                      onPressed: () {

                                        var docId = snapshot.data[index];
                                        viewOrder(docId);
                                      },
                                      color: primaryColor,
                                      child: Text(
                                        'Finish',
                                        style:
                                        wwasherbuttonWhiteTextStyle,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),

                    ],
                  ),
                ),
              ],
            ),
          );}
        );
      },
    );
  }

  getDot() {
    return Container(
      margin: EdgeInsets.only(left: 2.0, right: 2.0),
      width: 4.0,
      height: 4.0,
      decoration: BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.circular(2.0),
      ),
    );
  }
}
