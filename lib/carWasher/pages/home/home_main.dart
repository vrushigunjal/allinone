import 'package:all_in_one_app/carWasher/pages/home/active_order.dart';
import 'package:all_in_one_app/carWasher/pages/home/history.dart';
import 'package:all_in_one_app/carWasher/pages/home/new_order.dart';
import 'package:all_in_one_app/carWasher/pages/notification.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter/material.dart';


class HomeMain extends StatefulWidget {
  @override
  _HomeMainState createState() => _HomeMainState();
}

class _HomeMainState extends State<HomeMain> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: scaffoldBgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: whiteColor,
          title: Text(
            'Orders',
            style: bigHeadingStyle,
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.notifications, color: blackColor),
              onPressed: (){
                Navigator.push(context,  MaterialPageRoute(builder: (context) => Notifications()));

              },
            ),
          ],
          bottom: TabBar(
            unselectedLabelColor: Colors.grey.withOpacity(0.3),
            labelColor: primaryColor,
            indicatorColor: primaryColor,
            tabs: [
              Tab(text: 'New'),
              Tab(text: 'Active'),
              Tab(text: 'History'),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            NewOrder(),
            ActiveOrder(),
            History(),
          ],
        ),
      ),
    );
  }
}