import 'dart:io';

import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/carWasher/helper/user.dart';
import 'package:all_in_one_app/carWasher/pages/home.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:shared_preferences/shared_preferences.dart';


class Register extends StatefulWidget {


  @override
  _RegisterState createState() => _RegisterState();
}
enum ImagePickCategory { camera, phone }
class _RegisterState extends State<Register> {
  final Geolocator geolocator = Geolocator()
    ..forceAndroidLocationManager;
  Position _currentPosition;

  bool _loading = false;
  List<QueryDocumentSnapshot> _categories;
  File _productImage;
  String Name, centername, email, currentAddress, pan,adhar,shopact,gstno;
  String rating = "1";
  String fav = "notFavourite";

  String _phone = '';
  String agentType  = 'Agent';
  _RegisterState(
      {Key key, this.Name, this.centername, this.email, this.currentAddress, this.pan,this.adhar,this.shopact,this.gstno,});


  @override
  void initState() {
    _getCurrentLocation();
    getshared();
    super.initState();
  }

  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() async {
        currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('CURRENTADRESS', currentAddress);
        prefs.setString('CenterName', centername);
      });
    } catch (e) {
      print(e);
    }
  }

  _pickImage() async {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10.0),
          topRight: Radius.circular(10.0),
        ),
      ),
      builder: (ctx) {
        return Container(
          height: 200,
          color: Colors.transparent,
          child: ListView(
            children: [
              ListTile(
                leading: Icon(Icons.camera_alt),
                title: Text("Take a Picture from Camera"),
                onTap: () {
                  _takeImage(ImagePickCategory.camera);

                  Navigator.pop(ctx);
                },
              ),
              ListTile(
                leading: Icon(Icons.image),
                title: Text("Select a Picture from Device"),
                onTap: () {
                  _takeImage(ImagePickCategory.phone);
                  Navigator.pop(ctx);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  _takeImage(ImagePickCategory selection) async {
    try {
      final picker = ImagePicker();

      File _tempFile;

      // pick image
      if (selection == ImagePickCategory.camera) {
        final pickedFile = await picker.getImage(source: ImageSource.camera);
        _tempFile = File(pickedFile.path);
      } else if (selection == ImagePickCategory.phone) {
        final pickedFile = await picker.getImage(source: ImageSource.gallery);
        _tempFile = File(pickedFile.path);
      }

      // crop image

      File croppedFile = await ImageCropper.cropImage(
        sourcePath: _tempFile.path,
        compressQuality: 30,
        compressFormat: ImageCompressFormat.jpg,
        maxHeight: 1920,
        maxWidth: 1920,
      );

      print(croppedFile.lengthSync());

      setState(() {
        _productImage = croppedFile;
      });
    } catch (e) {
      print(e);
    }
  }

  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _phone = (prefs.getString('PHONENUMBER') ?? '');
    });
  }


  onCreateuserBtnTap() async {
    try {
//name, centername, email, currentAddress, pan,adhar,shopact,gstno;
      if (centername.isNotEmpty  != null) {
        String productImageName = path.basename(_productImage.path);
        /*SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('CENTERNAME', centername);
        prefs.setString('EMAIL', email);
        prefs.setString('Image', _productImage.path);*/

        /*SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('FULLNAME', name);
        prefs.setString('EMAIL', email);
        prefs.setString('CARNO', carno);*/

        uploadProductImage(
          productImageName,
          _productImage,
        ).then((downloadURL) {
          editPhoneNumber(
            downloadURL,
            Name,
            centername,
            _phone,
            email,
            currentAddress,
            pan,
            adhar,
            shopact,
            gstno,
              agentType,
              rating,
              fav,
            true
          ).then((v) {
            Navigator.push(context,  MaterialPageRoute(builder: (context) => Home()));
          });
        }).catchError((e) {
          print(e);
          Navigator.pop(context, "failed");
        });

      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }


  @override
  Widget build(BuildContext context) {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    return Scaffold(
      backgroundColor: scaffoldBgColor,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: ListView(
          children: <Widget>[
      Padding(
      padding: EdgeInsets.all(fixPadding),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
        heightSpace,
        heightSpace,
        heightSpace,
        heightSpace,
        heightSpace,
        heightSpace,
        Image.asset(
          'assets/loginicon.png',
          width: 200.0,
          fit: BoxFit.fitWidth,
        ),
        heightSpace,
        heightSpace,
        heightSpace,
        heightSpace,
        Text('Register your account', style: greyHeadingStyle),
        heightSpace,
        heightSpace,
        heightSpace,
          Container(
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  blurRadius: 1.5,
                  spreadRadius: 1.5,
                  color: Colors.grey[200],
                ),
              ],
            ),

        child: TextField(
          style: headingStyle,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(15.0),
            hintText: 'Car Image',
            hintStyle: greyHeadingStyle,
            border: InputBorder.none,
          ),
        ),
      ),
      heightSpace,
      heightSpace,
      InkWell(
        onTap: () {
          _pickImage();
        },
        child: _productImage == null
            ? Container(
          height: 280,
          width: 200.0,
          color: Colors.grey,
          child: Icon(Icons.add_a_photo),
        )
            : Image.file(
          _productImage,
          height: 280,
          fit: BoxFit.cover,
        ),
      ),
      heightSpace,
      heightSpace,
      // Full Name TextField Start
      Container(
        decoration: BoxDecoration(
          color: whiteColor,
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
              blurRadius: 1.5,
              spreadRadius: 1.5,
              color: Colors.grey[200],
            ),
          ],
        ),
        child: TextField(

          style: headingStyle,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(15.0),
            hintText: 'Full Name',
            hintStyle: greyHeadingStyle,
            border: InputBorder.none,
          ),
        ),
      ),
      // Full Name TextField End
      heightSpace,
      heightSpace,
          Container(
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  blurRadius: 1.5,
                  spreadRadius: 1.5,
                  color: Colors.grey[200],
                ),
              ],
            ),
            child: TextField(
              onChanged: (val) async {
                setState(() => centername = val);

              },
              style: headingStyle,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(15.0),
                hintText: 'Car Wash Center Name',
                hintStyle: greyHeadingStyle,
                border: InputBorder.none,
              ),
            ),
          ),
          // Full Name TextField End
          heightSpace,
          heightSpace,

      // Email Address TextField Start
      Container(
        decoration: BoxDecoration(
          color: whiteColor,
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
              blurRadius: 1.5,
              spreadRadius: 1.5,
              color: Colors.grey[200],
            ),
          ],
        ),
        child: TextField(
          onChanged: (val) {
            setState(() => email = val);
          },
          style: headingStyle,
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(15.0),
            hintText: 'Email Address',
            hintStyle: greyHeadingStyle,
            border: InputBorder.none,
          ),
        ),


      ),
      // Email Address TextField End
      heightSpace,
      heightSpace,

          Container(
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  blurRadius: 1.5,
                  spreadRadius: 1.5,
                  color: Colors.grey[200],
                ),
              ],
            ),
            child: Column(

                children: <Widget>[

                  Row(
                      children: <Widget>[
                        Icon(Icons.location_on),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Location',
                                    style: headingStyle,
                                  ),
                                  if (_currentPosition != null &&
                                      currentAddress != null)
                                    Text(currentAddress,
                                      style:
                                      headingStyle,)

                                ]
                            )
                        ),
                      ]
                  )
                ]
            ),




          ),
          // Email Address TextField End
          heightSpace,
          heightSpace,

          // Email Address TextField Start
          Container(
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  blurRadius: 1.5,
                  spreadRadius: 1.5,
                  color: Colors.grey[200],
                ),
              ],
            ),
            child: TextField(
              onChanged: (val) {
                 setState(() => pan = val);
              },
              style: headingStyle,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(15.0),
                hintText: 'Pan Number',
                hintStyle: greyHeadingStyle,
                border: InputBorder.none,
              ),
            ),
          ),
          // Email Address TextField End
          heightSpace,
          heightSpace,
          // Email Address TextField Start
          Container(
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  blurRadius: 1.5,
                  spreadRadius: 1.5,
                  color: Colors.grey[200],
                ),
              ],
            ),
            child: TextField(
              onChanged: (val) {
                 setState(() => adhar = val);
              },
              style: headingStyle,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(15.0),
                hintText: 'Adhar Card Number',
                hintStyle: greyHeadingStyle,
                border: InputBorder.none,
              ),
            ),
          ),
          // Email Address TextField End
          heightSpace,
          heightSpace,

          // Email Address TextField Start
          Container(
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  blurRadius: 1.5,
                  spreadRadius: 1.5,
                  color: Colors.grey[200],
                ),
              ],
            ),
            child: TextField(
              onChanged: (val) {
                 setState(() => shopact = val);
              },
              style: headingStyle,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(15.0),
                hintText: 'Shopact License',
                hintStyle: greyHeadingStyle,
                border: InputBorder.none,
              ),
            ),
          ),
          // Email Address TextField End
          heightSpace,
          heightSpace,
          // Email Address TextField Start
          Container(
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  blurRadius: 1.5,
                  spreadRadius: 1.5,
                  color: Colors.grey[200],
                ),
              ],
            ),
            child: TextField(
              onChanged: (val) {
                 setState(() => gstno = val);
              },
              style: headingStyle,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(15.0),
                hintText: 'GST Number',
                hintStyle: greyHeadingStyle,
                border: InputBorder.none,
              ),
            ),
          ),
          // Email Address TextField End
          heightSpace,
          heightSpace,
      Container(
        child: SizedBox(
          height: 50.0,
          width: width - (fixPadding * 2.0),
          child: RaisedButton(
            elevation: 0.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            onPressed: () async {
              onCreateuserBtnTap();
            },
            color: primaryColor,
            child: Text(
              'Continue',
              style: wbuttonWhiteTextStyle,
            ),
          ),
        ),
      ),
      heightSpace,

      ],
    ),)
    ,
    ]
    ,
    )
    ,
    );
  }
}
