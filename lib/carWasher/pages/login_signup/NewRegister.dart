import 'dart:io';

import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/carWasher/helper/user.dart';
import 'package:all_in_one_app/carWasher/pages/home.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:shared_preferences/shared_preferences.dart';
class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}
enum ImagePickCategory { camera, phone }
enum BestTutorSite { NormalUser, OlaUser, UberUser }
class _SignUpScreenState extends State<SignUpScreen> {

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  String userType = "UserSelf";
  int randomnumber = 123;
  String finalrandom = "123";
  File _productImage;
  String rating = "1";
  String fav = "notFavourite";
  String agentType  = 'Agent';
  String name,centername,_phone,email,adhar,pan,gst,shopact,currentAddress;
  TextEditingController _nameEditingController = TextEditingController();
  TextEditingController _centernameEditingController = TextEditingController();
  TextEditingController _phoneEditingController = TextEditingController();
  TextEditingController _emailEditingController = TextEditingController();
  TextEditingController _adharEditingController = TextEditingController();
  TextEditingController _panEditingController = TextEditingController();
  TextEditingController _gstEditingController = TextEditingController();
  TextEditingController _shopactEditingController = TextEditingController();


  @override
  void initState() {
    _getCurrentLocation();
    getshared();
    super.initState();
  }

  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() async {
        currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('CURRENTADRESS', currentAddress);
      });
    } catch (e) {
      print(e);
    }
  }
  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _phone = (prefs.getString('PHONENUMBER') ?? '');
    });
  }

  onCreateuserBtnTap() async {
    try {
       name = _nameEditingController.text;
       centername = _centernameEditingController.text;
       _phone = _phoneEditingController.text;
      email = _emailEditingController.text;
      adhar = _adharEditingController.text;
      pan = _panEditingController.text;
      gst = _gstEditingController.text;
      shopact = _shopactEditingController.text;
      if (name.isNotEmpty &&
          centername.isNotEmpty&& _phone.isNotEmpty &&
          email.isNotEmpty && adhar.isNotEmpty && pan.isNotEmpty && gst.isNotEmpty && shopact.isNotEmpty != null) {
        String productImageName = path.basename(_productImage.path);


        uploadProductImage(
          productImageName,
          _productImage,
        ).then((downloadURL) {
          editPhoneNumber(
            downloadURL,
            name,
            centername,
            _phone,
            email,
            currentAddress,
            pan,
            adhar,
            shopact,
            gst,
            agentType,
            rating,
            fav,
            true,
          ).then((v) {
            Navigator.push(context,  MaterialPageRoute(builder: (context) => Home()));
          });
        }).catchError((e) {
          print(e);
          Navigator.pop(context, "failed");
        });

      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }


  _pickImage() async {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10.0),
          topRight: Radius.circular(10.0),
        ),
      ),
      builder: (ctx) {
        return Container(
          height: 200,
          color: Colors.transparent,
          child: ListView(
            children: [
              ListTile(
                leading: Icon(Icons.camera_alt),
                title: Text("Take a Picture from Camera"),
                onTap: () {
                  _takeImage(ImagePickCategory.camera);

                  Navigator.pop(ctx);
                },
              ),
              ListTile(
                leading: Icon(Icons.image),
                title: Text("Select a Picture from Device"),
                onTap: () {
                  _takeImage(ImagePickCategory.phone);
                  Navigator.pop(ctx);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  _takeImage(ImagePickCategory selection) async {
    try {
      final picker = ImagePicker();

      File _tempFile;

      // pick image
      if (selection == ImagePickCategory.camera) {
        final pickedFile = await picker.getImage(source: ImageSource.camera);
        _tempFile = File(pickedFile.path);
      } else if (selection == ImagePickCategory.phone) {
        final pickedFile = await picker.getImage(source: ImageSource.gallery);
        _tempFile = File(pickedFile.path);
      }

      // crop image

      File croppedFile = await ImageCropper.cropImage(
        sourcePath: _tempFile.path,
        compressQuality: 30,
        compressFormat: ImageCompressFormat.jpg,
        maxHeight: 1920,
        maxWidth: 1920,
      );

      print(croppedFile.lengthSync());

      setState(() {
        _productImage = croppedFile;
      });
    } catch (e) {
      print(e);
    }
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[

          SizedBox(
            height: 20,
          ),
      InkWell(
      onTap: (){
        _pickImage();
      },
      child: _productImage == null
          ? Container(
        height: 22.0,
        width: 22.0,
        margin: EdgeInsets.all(fixPadding / 2),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(11.0),
          border: Border.all(
              width: 1.0, color: whiteColor.withOpacity(0.7)),
          color: Colors.orange,
        ),
        child: Icon(Icons.add, color: whiteColor, size: 15.0),
      ): Image.file(
        _productImage,
        height: 280,
        fit: BoxFit.cover,
      ),
      ),

          Padding(
            padding: const EdgeInsets.only(top:10.0,right:20.0,left: 20.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.person), onPressed: null),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 20, left: 10),
                        child: TextField(
                          controller: _nameEditingController,
                          decoration: InputDecoration(hintText: 'Username'),
                        )))
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top:10.0,right:20.0,left: 20.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.person), onPressed: null),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 20, left: 10),
                        child: TextField(
                          controller: _centernameEditingController,
                          decoration: InputDecoration(hintText: 'Centername'),
                        )))
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top:10.0,right:20.0,left: 20.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.mail), onPressed: null),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 20, left: 10),
                        child: TextField(
                          controller: _emailEditingController,
                          decoration: InputDecoration(hintText: 'Email'),
                        )))
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top:10.0,right:20.0,left: 20.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.add_location), onPressed: null),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 20, left: 10),
                        child: TextField(
                          decoration: InputDecoration(
                              hintText: currentAddress,
                          ),
                        )))
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top:10.0,right:20.0,left: 20.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.confirmation_number), onPressed: null),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 20, left: 10),
                        child: TextField(
                            controller: _panEditingController,
                          decoration: InputDecoration(hintText: 'PanNumber'),
                        )))
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top:10.0,right:20.0,left: 20.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.confirmation_number), onPressed: null),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 20, left: 10),
                        child: TextField(
                          controller: _adharEditingController,
                          decoration: InputDecoration(hintText: 'AdharNumber'),
                        )))
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top:10.0,right:20.0,left: 20.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.confirmation_number), onPressed: null),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 20, left: 10),
                        child: TextField(
                          controller: _shopactEditingController,
                          decoration: InputDecoration(hintText: 'ShopactNumber'),
                        )))
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top:10.0,right:20.0,left: 20.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.confirmation_number), onPressed: null),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 20, left: 10),
                        child: TextField(
                          controller: _gstEditingController,
                          decoration: InputDecoration(hintText: 'GstNumber'),
                        )))
              ],
            ),
          ),

          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: Container(
                height: 60,
                child: RaisedButton(
                  onPressed: () {
                    onCreateuserBtnTap();
                   // Navigator.pushNamed(context, 'Home');
                  },
                  color: Color(0xFF00a79B),
                  child: Text(
                    'SIGN UP',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),

    );

  }
}

