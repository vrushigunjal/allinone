

import 'package:all_in_one_app/carWasher/pages/login_signup/NewRegister.dart';
import 'package:all_in_one_app/carWasher/pages/login_signup/login.dart';
import 'package:all_in_one_app/carWasher/pages/login_signup/register.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
final FirebaseAuth auth = FirebaseAuth.instance;
class AuthService {
  //Handles Auth
  handleAuth() {
    return StreamBuilder(
        stream: FirebaseAuth.instance.onAuthStateChanged,
        builder: (BuildContext context, snapshot) {
          if (snapshot.hasData) {
            return SignUpScreen();
          } else {
            return Login();
          }
        });
  }

  //Sign out
  signOut() {
    FirebaseAuth.instance.signOut();
  }

  //SignIn
  signIn(AuthCredential authCreds) {
    Firebase.initializeApp();
    FirebaseAuth.instance.signInWithCredential(authCreds);

  }

  signInWithOTP(smsCode, verId) async {
    AuthCredential authCreds = PhoneAuthProvider.getCredential(
        verificationId: verId, smsCode: smsCode);
    signIn(authCreds);

  }
}