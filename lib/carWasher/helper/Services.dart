import 'dart:io';


import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'auth.dart';



final FirebaseFirestore firestore = FirebaseFirestore.instance;

final FirebaseStorage storage = FirebaseStorage();
final User user = auth.currentUser;


Future<String> uploadServiceImage(String imageName, File image) async {
  final StorageReference storageReference =
  FirebaseStorage().ref().child("Service_images").child(imageName);

  final StorageUploadTask uploadTask = storageReference.putFile(image);

  StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
  return await storageTaskSnapshot.ref.getDownloadURL();
}

Future<dynamic> createCategory(
    String serviceImage,
    String title,
    String decription,
    double salePrice,
    double Total,
    String centerName,
    String category)
async {
  return await firestore.collection("categories").add({
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "serviceImage": serviceImage,
    "title": title,
    "decription": decription,
    "salePrice": salePrice,
    "Total": Total,
    "centerName": centerName,
    "category": category,
    "uid": user.uid

  });
}
Future<void> updatewasherCategory(String docid, String title,String decription,double salePrice,double lastprice,String category) async {
  return await firestore.collection("categories").doc(docid).update({
    "updated_at": FieldValue.serverTimestamp(),
    "title": title,
    "decription": decription,
    "salePrice": salePrice,
    "Total": lastprice,
    "category": category,

  });
}//Categories
Future<QuerySnapshot> getCategories() async {
  return await firestore
      .collection("categories")
      .limit(20)
      .get();
}
Future<QuerySnapshot> getwasgerCategories(String uid) async {
  return await firestore
      .collection("categories")
      .where("uid", isEqualTo: uid)
      .get();
}


Future<QuerySnapshot> getMoreCategories(DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("categories")
      .where("uid", isEqualTo: user.uid)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}
Future<void> deleteCategory(String id,String image) async {
  StorageReference imgRef = await storage.getReferenceFromUrl(image);
  await imgRef.delete();
  return await firestore.collection("categories").doc(id).delete();
}
