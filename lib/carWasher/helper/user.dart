import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

final FirebaseAuth auth = FirebaseAuth.instance;
final FirebaseFirestore firestore = FirebaseFirestore.instance;

final User user = auth.currentUser;
String UID;

Future<String> uploadProductImage(String imageName, File image) async {
  final StorageReference storageReference =
  FirebaseStorage().ref().child("user_images").child(imageName);

  final StorageUploadTask uploadTask = storageReference.putFile(image);

  StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
  return await storageTaskSnapshot.ref.getDownloadURL();
}

/*
Future<String>editPhoneNumber(
    String userImage,
    String name,
    String centerName,
    String userphone,
    String email,
    String adrress,
    String pan,
    String adhar,
    String shopact,
    String gstno,
    String status,
    bool userRegister) {
  firestore.collection("usersRegistration").doc(user.uid).set({
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "userImage": userImage,
    "name": name,
    "Centername": centerName,
    "PhoneNumber": userphone,
   "email": email,
    "adreess": adrress,
    "Pan": pan,
    "adhar": adhar,
    "shopact": shopact,
    "gstno": gstno,
    "status": status,
    "uid": user.uid,
    "isWasher": userRegister
  }, SetOptions(merge: true));
  return user.uid;
}
*/


Future<String>editPhoneNumber(
    String userImage,
    String name,
    String centerName,
    String userphone,
    String email,
    String adrress,
    String pan,
    String adhar,
    String shopact,
    String gstno,
    String agentType,
    String rating,
    String fav,
    bool userRegister) async{
  firestore.collection("WasherRegistration").doc(user.uid).set({
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "userImage": userImage,
    "name": name,
    "Centername": centerName,
    "PhoneNumber": userphone,
    "email": email,
    "adreess": adrress,
    "Pan": pan,
    "adhar": adhar,
    "shopact": shopact,
    "gstno": gstno,
    "isWasher": userRegister,
    "agentType": agentType,
    "rating": rating,
    "Favourite": fav,
    "uid": user.uid
  }, SetOptions(merge: true));
  return user.uid;
}
Future<void> updateWasher(
    String docid,
    String image,
    String name,
    String centername,
    String phone,
    String email,
    String adrress,
    String adhar,
    String Pan,
    String gstno,
    String shopact,
    ) async {
  return await firestore.collection("WasherRegistration").doc(docid).update({
    "updated_at": FieldValue.serverTimestamp(),
    "userImage": image,
    "name": name,
    "name": centername,
    "PhoneNumber": phone,
    "email": email,
    "adreess": adrress,
    "dateOfBirth": adhar,
    "dateOfBirth": Pan,
    "dateOfBirth": gstno,
    "dateOfBirth": shopact,
  });
}
Future<String> AddRating(String id,String rating,String Username,String phone,String image,String centerName) async {
  firestore.collection("Rating").doc(user.uid).set({
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "id": id,
    "rating": rating,
    "Username": Username,
    "phone": phone,
    "image": image,
    "centerName": centerName,
  });

}
Future<QuerySnapshot> getRating(
    String id) async {
  print(id);
  return await firestore
      .collection("Rating")
      .where("id", isEqualTo: id)
      .limit(20)
      .get();
}


editAddress(String homeAddress, String officeAddress) {
  return firestore.collection("WasherRegistration").doc(user.uid).set(
      {"homeAddress": homeAddress, "officeAddress": officeAddress},
      SetOptions(merge: true));
}


Future<void> updateUser(
    String docid, String fav) async {
  print(docid);
  return await firestore
      .collection("WasherRegistration").doc(docid).update(
      {
        "Favourite": fav,
      }
  );
}
  Future<QuerySnapshot> Favouritelist(
   String fav) async {
  return await firestore
      .collection("WasherRegistration")
      .where("Favourite", isEqualTo: fav)
  .limit(20)
      .get();
}

/*editstatus( String status) {
  return firestore
      .collection("WasherUsers")
      .doc(user.uid)
      .set({ "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),"status": status}, SetOptions(merge: true));
}*/

Future<void> editstatus(String docid, String status,String centername,String adreess) async {
  return await firestore.collection("WasherRegistration").doc(docid).update({
    "updated_at": FieldValue.serverTimestamp(),
    "status": status,
    "Centername": centername,
    "adreess": adreess,

  });
}




/*Future<void> updateUser(String docid, String name,
    String dateofBirth, String email, String adrress, String carno,String cartype) async {
  return await firestore.collection("users").doc(docid).update({
    "name": name,
    "dateOfBirth": dateofBirth, "email": email, "adreess": adrress, "carNo": carno,"cartype": cartype,
  });
}*/
Future<DocumentSnapshot> getUser() {
  return firestore.collection("WasherRegistration").doc(user.uid).get();
}

Future<QuerySnapshot> getUserData() async {
  return await firestore
      .collection("WasherRegistration")
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}
Future<QuerySnapshot> getWasharListData() async {
  return await firestore
      .collection("WasherRegistration")
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}


Future<QuerySnapshot> getMoreUser(DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("WasherRegistration")
      .orderBy("created_at", descending: true)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}

Future<DocumentSnapshot> getUserDataById(String uid) {
  return firestore.collection("WasherRegistration").doc(uid).get();
}

Future<QuerySnapshot> searchUser(String searchValue, String searchBy) async {
  return await firestore
      .collection("WasherRegistration")
      .where(searchBy, isEqualTo: searchValue)
      .limit(1)
      .get();
}
