import 'package:all_in_one_app/User/pages/search/get_search_results.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:geolocator/geolocator.dart';


class NearBy extends StatefulWidget {
  @override
  _NearByState createState() => _NearByState();
}

class _NearByState extends State<NearBy> {

  bool address1 = true;
  bool address2 = false;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  String currentAddress;

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }

  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }


  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        backgroundColor: scaffoldBgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: whiteColor,
          title: Text(
            'Nearby',
            style: bigHeadingStyle,
          ),
          actions: <Widget>[
            InkWell(
              onTap: () {
               // _addressBottomSheet(context, width);
              },
              child: Container(
                width: width / 1.2,
                padding: EdgeInsets.all(fixPadding),
                margin: EdgeInsets.only(top: 5.0, bottom: 5.0, right: 5.0),
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.location_on,
                      color: greyColor,
                      size: 14.0,
                    ),
                    Text(currentAddress ?? "", style: TextStyle(
                        fontWeight: FontWeight.w700,color: Colors.black,
                        fontSize: 16),),
                    Icon(
                      Icons.arrow_drop_down,
                      color: greyColor,
                      size: 16.0,
                    ),
                  ],
                ),
              ),
            ),
          ],
          bottom: TabBar(
            isScrollable: true,
            unselectedLabelColor: Colors.grey.withOpacity(0.3),
            labelColor: primaryColor,
            indicatorColor: primaryColor,
            tabs: [
              Tab(text: 'Center'),
             
            ],
          ),
        ),
        body: TabBarView(
          children: [
            GetSearchResults(),
           
          ],
        ),
      ),
    );
  }

  // Bottom Sheet for Address Starts Here
  void _addressBottomSheet(context, width) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Material(
            elevation: 7.0,
            child: Container(
              color: Colors.white,
              child: Wrap(
                children: <Widget>[
                  Container(
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: width,
                            padding: EdgeInsets.all(fixPadding),
                            alignment: Alignment.center,
                            child: Text(
                              'Select Address'.toUpperCase(),
                              style: headingStyle,
                            ),
                          ),
                          Divider(),
                          InkWell(
                            onTap: () {
                            /*  setState(() {
                                currentAddress ?? "";
                                address1 = true;
                                address2 = false;
                              });
                              Navigator.pop(context);*/
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: 26.0,
                                  height: 26.0,
                                  decoration: BoxDecoration(
                                      color: (address1)
                                          ? primaryColor
                                          : whiteColor,
                                      borderRadius: BorderRadius.circular(13.0),
                                      border: Border.all(
                                          width: 1.0,
                                          color: greyColor.withOpacity(0.7))),
                                  child: Icon(Icons.check,
                                      color: whiteColor, size: 15.0),
                                ),
                                widthSpace,
                                Text(
                                  '76A, New York, US.',
                                  style: listItemTitleStyle,
                                ),
                              ],
                            ),
                          ),
                          heightSpace,
                          InkWell(
                           /* onTap: () {
                              setState(() {
                                currentAddress ;
                                address1 = false;
                                address2 = true;
                              });
                              Navigator.pop(context);
                            },*/
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: 26.0,
                                  height: 26.0,
                                  decoration: BoxDecoration(
                                      color: (address2)
                                          ? primaryColor
                                          : whiteColor,
                                      borderRadius: BorderRadius.circular(13.0),
                                      border: Border.all(
                                          width: 1.0,
                                          color: greyColor.withOpacity(0.7))),
                                  child: Icon(Icons.check,
                                      color: whiteColor, size: 15.0),
                                ),
                                widthSpace,
                                Text(
                                  '55C, California, US.',
                                  style: listItemTitleStyle,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
  // Bottom Sheet for Address Ends Here
}
