import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/User/helper/favourite.dart';
import 'package:all_in_one_app/User/pages/restaurant/restaurant.dart';
import 'package:all_in_one_app/carWasher/helper/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';


class RestaurantsTabData extends StatefulWidget {
  @override
  _RestaurantsTabDataState createState() => _RestaurantsTabDataState();
}

class _RestaurantsTabDataState extends State<RestaurantsTabData> {

  bool _loading = true;
  bool _empty = true;
  List<DocumentSnapshot> orders;
  DocumentSnapshot lastDocument;
  ScrollController _scrollController;
  String phone = "1234550";
  String id = "1234550";
  String fav = "Favourite";
  @override
  void initState() {
    getshared();
    super.initState();
  }
  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      phone = (prefs.getString('PHONENUMBER') ?? '');
      id = (prefs.getString('id') ?? '');
      loadMyOrders();
    });
  }
  loadMyOrders() async {
    try {
      print(id);
      QuerySnapshot qSnap = await Favouritelist(fav);
      if (qSnap.size > 0) {
        setState(() {
          orders = qSnap.docs;
          _empty = false;
          _loading = false;
        });
      } else {
        setState(() {
          _empty = true;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _empty = true;
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (_loading == true) {
      return Container(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      );
    }
    if (_empty == true) {
      return Empty(text: "No orders!");
    }
    return ListView.builder(
            itemCount: orders.length,
            physics: BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final item = orders[index];
              return Slidable(
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.25,
                secondaryActions: <Widget>[
                  Container(
                    margin: EdgeInsets.only(
                      top: (index == 0) ? fixPadding : 0.0,
                      bottom: fixPadding,
                    ),
                    child: IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () {
                        setState(() {
                          orders.removeAt(index);
                        });

                        // Then show a snackbar.
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text('Item Removed'),
                        ));
                      },
                    ),
                  ),
                ],
                child: InkWell(
                  onTap: () {
                    Navigator.push(context,  MaterialPageRoute(builder: (context) => Restaurant(doc: orders[index],)));



                  },
                  child: Container(
                    width: width,
                    height: 100.0,
                    margin: (index == 0)
                        ? EdgeInsets.all(fixPadding)
                        : EdgeInsets.only(
                        right: fixPadding,
                        left: fixPadding,
                        bottom: fixPadding),
                    decoration: BoxDecoration(
                      color: whiteColor,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 100.0,
                          width: 90.0,
                          alignment: Alignment.topRight,
                          padding: EdgeInsets.all(fixPadding),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.horizontal(
                                left: Radius.circular(5.0)),
                            image: DecorationImage(
                              image: NetworkImage(item['userImage']),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Container(
                          width: width - ((fixPadding * 2) + 100.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.all(fixPadding),
                                child: Text(
                                  item['Centername'],
                                  style: listItemTitleStyle,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: fixPadding, right: fixPadding),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.location_on,
                                      color: greyColor,
                                      size: 18.0,
                                    ),
                                    Text(
                                      item['adreess'],
                                      style: listItemSubTitleStyle,
                                      maxLines: 4,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                ),
                              ),

                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );



  }
}
