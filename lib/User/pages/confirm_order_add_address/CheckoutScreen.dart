import 'package:all_in_one_app/Admin/config/payment_options.dart';
import 'package:all_in_one_app/Admin/helpers/coupans.dart';
import 'package:all_in_one_app/User/pages/confirm_order_add_address/PayOnDeliveryOrderScreen.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:scratcher/scratcher.dart';
import 'package:shared_preferences/shared_preferences.dart';


class CheckoutScreen extends StatefulWidget {
  final List cart;

  CheckoutScreen({@required this.cart});

  @override
  _CheckoutScreenState createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  DocumentSnapshot shopDetails;
  static const platform = const MethodChannel("razorpay_flutter");
  bool _loading = true;
  double cartItemsTotal = 0;
  double total = 0;
  double taxAmount = 0;
  int tax = 0;
  var randomflat = '123';
  double _opacity = 0.0;
  Razorpay _razorpay;
  String Uid = "";
  var flat = '123';
  String randomNumber = '';
  var coupandocid = '123';



  @override
  void initState() {
    getshared();

    super.initState();
   // getimage();
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
    preparePayment();
  }

  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      Uid = (prefs.getString('Uid') ?? '');
      randomNumber = (prefs.getString('Randomnumber') ?? '');
      print(Uid);
      getrandomcoupancode();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }
  void getapplycode() {
    total = cartItemsTotal  - int.parse(applycoupan);
  }
/*  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      Uid = (prefs.getString('Uid') ?? '');
      randomNumber = (prefs.getString('Randomnumber') ?? '');
      print(randomNumber);
      print("UID"+Uid);
      getrandomcoupancode();
    });
  }*/


/*
  Future getimage() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection("coupans")
        .where("First", isEqualTo: "yes")
        .where("RandomNumber", isEqualTo: randomNumber)
        .get();
    qn.documents.forEach((qn) {
      coupandocid = qn.documentID;
      flat = qn["flat"];
      print(flat);
    });
    return qn.documents;
  }*/
  String applycoupan = "0";
  Future getrandomcoupancode() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection("coupans")
        .where("RandomNumber", isEqualTo: randomNumber)
        .get();
    qn.documents.forEach((qn) {

      randomflat = qn["flat"];
    });
    return qn.documents;
  }

  void openCheckout() async {
    var options = {
      'key': 'rzp_test_NNbwJ9tmM0fbxj',
      'amount': 100.00,
      'name': 'Shaiq',
      'description': 'Payment',
      'prefill': {'contact': '8888888888', 'email': 'test@razorpay.com'},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint(e);
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    checkIfLikedOrNot();
    Fluttertoast.showToast(
        msg: "SUCCESS: " + response.paymentId, timeInSecForIos: 4);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Fluttertoast.showToast(
        msg: "ERROR: " + response.code.toString() + " - " + response.message,
        timeInSecForIos: 4);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName, timeInSecForIos: 4);
  }
  preparePayment() async {
    // calculate total
    double cartTotal = 0;
    List cart = widget.cart;

    cart.forEach((c) {
      double salePrice = c["price"];
      cartTotal += salePrice;
    });

    cartTotal = double.parse(cartTotal.toStringAsFixed(2));

   // var shopDetailsFromDB = await getShopDetails();
    int taxInt = 0;
    double payableAmount = cartTotal;
    double payableTaxAmount = 0;

  /*  if (shopDetailsFromDB.exists) {
      if (shopDetailsFromDB.data()["tax"] != null) {
        double taxDouble =
        double.parse(shopDetailsFromDB.data()["tax"].toString());
        taxInt = taxDouble.toInt();

        // calculate percentage of tax
        payableTaxAmount = (cartTotal * taxInt) / 100;
        payableTaxAmount = double.parse(payableTaxAmount.toStringAsFixed(2));

        payableAmount += payableTaxAmount;
      }
    }*/

    payableAmount = double.parse(payableAmount.toStringAsFixed(2));

    setState(() {
      cartItemsTotal = cartTotal;


    //  shopDetails = shopDetailsFromDB;
      tax = taxInt;
      taxAmount = payableTaxAmount;
      total = payableAmount;

      _loading = false;
    });
  }

  onBtnPayOnDeliveryTap() async {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PayOnDeliveryOrderScreen(
          cart: widget.cart,
          cartItemsTotal: cartItemsTotal,
          tax: tax,
          taxAmount: taxAmount,
          total: total,
        ),
      ),
    );
  }

  onBtnSelfPickupTap() async {
/*    try {
      DocumentReference ref = await addOrderSelfPickup(
        widget.cart,
        cartItemsTotal,
        tax,
        taxAmount,
        total,
      );
      await clearCart();
      String orderId = ref.id;
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (context) => OrderPlacedScreen(
            orderId: orderId,
            orderMethod: "selfPickup",
          ),
        ),
        ModalRoute.withName("/home"),
      );
    } catch (e) {
      print(e);
      showMyDialog(
        context: context,
        title: "oops",
        description: "Something went wrong, try after some time!",
      );
    }*/
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Checkout"),
      ),
      body: _loading == true
          ? Container(
        child: Center(
          child: SpinKitChasingDots(
            color: primaryColor,
            size: 50,
          ),
        ),
      )
          : Container(
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 40,
                ),
                Text("Total Payable Amount"),

                SizedBox(
                  height: 20,
                ),
                Card(
                  elevation: 10.0,
            child: Container(
                  padding: EdgeInsets.all(8.0),
                  child:Text(
                    "$total",
                    style: TextStyle(
                      fontSize: 52,
                      fontWeight: FontWeight.bold,
                      color: primaryColor,
                    ),
                  ),
                ),
                ),
                SizedBox(
                  height: 20,
                ),
                // total amout
            Column(

              children: <Widget>[
                Container(
                  width: width,
                  padding: EdgeInsets.all(fixPadding),
                  color: Colors.grey.withOpacity(0.1),
                  child: Text(
                    'Add Voucher',
                    style: greyHeadingStyle,
                  ),
                ),
                Container(
                  width: width,
                  padding: EdgeInsets.all(fixPadding),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: width - ((fixPadding * 2) + 75.0),
                        child: TextField(
                          onChanged: (val) {
                            setState(() => applycoupan = val);
                          },
                          style: moreStyle,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(15.0),
                            hintText: 'Add Voucher Code',

                            hintStyle: moreStyle,
                            prefixIcon: Icon(
                              Icons.local_activity,
                              color: primaryColor,
                            ),
                          ),
                        ),
                      ),
                      widthSpace,
                      InkWell(
                        child: Container(

                          width: 65.0,
                          padding: EdgeInsets.all(fixPadding),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: primaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                          ),

                          child: Text(
                            'Apply',
                            style: whiteSubHeadingStyle,
                          ),
                        ),
                        onTap: () {
                          print(applycoupan);
                          getapplycode();
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(16),
                  ),
                  padding: EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text("Payment Details:"),
                      SizedBox(
                        height: 10,
                      ),
                      DataTable(
                        columns: [
                          DataColumn(
                            label: Text("Name",
                            style:  TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                            ),

                          ),
                          DataColumn(
                            label: Text("Amount",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: "Lato"
                              ),),
                          ),
                        ],
                        rows: [
                          DataRow(
                            cells: [
                              DataCell(Text("Cart Total",
                                style:  TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),)),
                              DataCell(
                                  Text("$cartItemsTotal",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontFamily: "Lato"
                                    ),
                                  )),
                            ],
                          ),
                          DataRow(
                            cells: [
                              DataCell(Text("Coupan",
                                style:  TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                              )),
                              DataCell(Text(
                                  "$applycoupan",
                                style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: "Lato"
                                ),
                              )),
                            ],
                          ),
                          DataRow(
                            cells: [
                              DataCell(Text("Total",
                                style:  TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                              )),
                              DataCell(Text("$total",
                                style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: "Lato"
                                ),)),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),

                // payment options
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                        FlatButton(

                          color: primaryColor,
                          textColor: Colors.black,
                          onPressed: () {
                            openCheckout();
                            },
                          child: Text("Proceed To Buy",
                            style: TextStyle(
                              color: Colors.grey[700],
                              fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),

                      SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }

  checkIfLikedOrNot() async {
    final CollectionReference dbIDS = Firestore.instance.collection(
        'orders');
    QuerySnapshot _query = await dbIDS
        .where('uid', isEqualTo: Uid)
        .getDocuments();

    final CollectionReference Coupan = Firestore.instance.collection('coupans');
    QuerySnapshot _querycoupan = await Coupan
        .where('RandomNumber', isEqualTo: randomNumber)
        .getDocuments();

    final CollectionReference UserCoupan = Firestore.instance.collection('ASSS');
    QuerySnapshot _queryusercoupan = await UserCoupan
        .where('Uid', isEqualTo: Uid)
        .where('flat', isEqualTo: randomflat)
        .getDocuments();


    if (_querycoupan.documents.length > 0 && _query.documents.length > 0) {
      // the ID exists
      onBtnPayOnDeliveryTap();
       print("there are documents");
    }

    else if(_queryusercoupan.documents.length > 0){
      onBtnPayOnDeliveryTap();
      print("AllReady Used Coupan");
    }
    else  {
      scratchCardDialogforrandom(context);
      scratchCardDialogForFirst(context);
      print("No");
    }
  }

  Future<void> scratchCardDialogForFirst(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          title: Align(
            alignment: Alignment.center,
            child: Text(
              'You\'ve won a scratch card',
              style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
          ),
          content: StatefulBuilder(builder: (context, StateSetter setState) {
            return Scratcher(
              accuracy: ScratchAccuracy.low,
              threshold: 25,
              brushSize: 50,
              color: Colors.yellow,
              onThreshold: () {
                onBtnPayOnDeliveryTap();
                AddUserCoupan(
                  Uid,
                  flat,
                );
                setState(() {
                  _opacity = 1;

                });
              },
              child: AnimatedOpacity(
                duration: Duration(milliseconds: 250),
                opacity: _opacity,
                child: Container(
                  height: 300,
                  width: 300,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          'assets/tropy.png'),
                    ),
                  ),
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    flat,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                        color: Colors.grey),
                  ),
                ),
              ),
            );
          }),
        );
      },
    );
  }

  Future<void> scratchCardDialogforrandom(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          title: Align(
            alignment: Alignment.center,
            child: Text(
              'You\'ve won a scratch card',
              style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
          ),
          content: StatefulBuilder(builder: (context, StateSetter setState) {
            return Scratcher(
              accuracy: ScratchAccuracy.low,
              threshold: 25,
              brushSize: 50,
              color: Colors.yellow,
              onThreshold: () {
                onBtnPayOnDeliveryTap();
                AddUserCoupan(
                  Uid,
                  flat,
                );
                setState(() {
                  _opacity = 1;
                });
              },
              child: AnimatedOpacity(
                duration: Duration(milliseconds: 550),
                opacity: _opacity,
                child: Container(
                  height: 300,
                  width: 300,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          'assets/tropy.png'),
                    ),
                  ),
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    flat,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                        color: Colors.grey),
                  ),
                ),
              ),
            );
          }),
        );
      },
    );
  }



}
