import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/helpers/car.dart';
import 'package:all_in_one_app/Admin/helpers/coupans.dart';
import 'package:all_in_one_app/User/helper/OrderConfirm.dart';
import 'package:all_in_one_app/User/helper/Payment.dart';
import 'package:all_in_one_app/User/helper/ServiceOrderConfirm.dart';
import 'package:all_in_one_app/User/pages/confirm_order_add_address/add_new_address.dart';
import 'package:all_in_one_app/User/pages/home/home.dart';
import 'package:all_in_one_app/carWasher/helper/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter/services.dart';
import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:scratcher/scratcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:math';

class ConfirmServiceOrder extends StatefulWidget {


  @override

  _ConfirmServiceOrderState createState() => _ConfirmServiceOrderState();
}

class _ConfirmServiceOrderState extends State<ConfirmServiceOrder> {

  static const platform = const MethodChannel("razorpay_flutter");
  double _opacity = 0.0;
  Razorpay _razorpay;
  String Uid = "";
  String id = "";
  String _centerName;
  String _adress = '';
  String _name = '';
  String _phone = '';
  String _productName = '';
  String _desription = '';
  String _image = '';
  String randomNumber = '';
  double _price;
  double _Washerprice;
  int shipcharge = 10;
  double result = 0;
  String applycoupan = "0";
  String note = "";
  bool _loading = false;
  String DOCID = "";
  String cartypes = '';
  String order = "ordered";
  String WasharPaymentStatus = "Unpaid";
  List<QueryDocumentSnapshot> _categories;


  void initState() {
    getshared();
    getimage();
    loadCategories();
    initPlatformState();
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
    super.initState();
  }
  String _platformVersion = 'Unknown';
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterOpenWhatsapp.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  var coupandocid = '123';
  var flat = '123';
  var randomcoupandocid = '123';
  var randomflat = '123';
  loadCategories() async {
    try {
      setState(() {
        _loading = true;
      });

      QuerySnapshot qsnap = await getCarType();
      setState(() {
        _categories = qsnap.docs;
        cartypes = qsnap.docs[0].id;
        _loading = false;
      });
    } catch (e) {
      print(e);
    }
  }
  Future getimage() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection("coupans").where(
        "First", isEqualTo: "yes")
        .get();
    qn.documents.forEach((qn) {
      coupandocid = qn.documentID;
      flat = qn["flat"];
      print(flat);
    });
    return qn.documents;
  }

  Future getrandomcoupancode() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection("coupans").where(
        "RandomNumber", isEqualTo: randomNumber)
        .get();
    qn.documents.forEach((qn) {
      randomcoupandocid = qn.documentID;
      randomflat = qn["flat"];
    });
    return qn.documents;
  }


  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }

  void openCheckout() async {
    var options = {
      'key': 'rzp_test_NNbwJ9tmM0fbxj',
      'amount': 100.00,
      'name': 'Shaiq',
      'description': 'Payment',
      'prefill': {'contact': '7040131381', 'email': 'test@razorpay.com'},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint(e);
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    //orderConfirm();
    checkIfLikedOrNot();
    paymentConfirm();
    Fluttertoast.showToast(

        msg: "SUCCESS: " + response.paymentId, timeInSecForIos: 4);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Fluttertoast.showToast(
        msg: "ERROR: " + response.code.toString() + " - " + response.message,
        timeInSecForIos: 4);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName, timeInSecForIos: 4);
  }

  paymentConfirm() {
    if (_phone.isNotEmpty != null) {
      print(
          "$_phone \n $_name \n $_desription");

      addPayment(
        _phone,
        _name,
        _adress,
        _productName,
        _Washerprice,
        result,
        order,
        _centerName,
      ).then((v) {
        Navigator.pop(context, "done");
      });
    } else {
      showMyDialog(
        context: context,
        title: "oops",
        description: "Please provide all details.",
      );
    }
  }

  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      Uid = (prefs.getString('Uid') ?? '');
      _centerName = (prefs.getString('CenterName') ?? '');
      _adress = (prefs.getString('CURRENTADRESS') ?? '');
      _name = (prefs.getString('FULLNAME') ?? '');
      _phone = (prefs.getString('PHONENUMBER') ?? '');
      _productName = (prefs.getString('Productname') ?? '');
      _desription = (prefs.getString('ProductDiscription') ?? '');
      _Washerprice = (prefs.getDouble('Washerprice') ?? '');
      _price = (prefs.getDouble('Productprice') ?? '');
      result = _price + shipcharge;
      _image = (prefs.getString('productImage') ?? '');
      randomNumber = (prefs.getString('Randomnumber') ?? '');
      print(randomNumber);
      getrandomcoupancode();
    });
  }

  void getapplycode() {
    result = _price + shipcharge + int.parse(applycoupan);
  }


  orderConfirm() async {
    if (_phone.isNotEmpty && cartypes.isNotEmpty && note.isNotEmpty != null) {
      print(Uid);
      print(
          "$_phone \n $_name \n $_desription");

      addServiceOrder(
        _phone,
        _image,
        _name,
        _adress,
        _productName,
        _desription,
        _price,
        _Washerprice,
        shipcharge,
        result,
        applycoupan,
        note,
          cartypes,
        order,
          WasharPaymentStatus,
          Uid
      ).then((v) {
        Navigator.pop(context, "done");
      });
    } else {
      showMyDialog(
        context: context,
        title: "oops",
        description: "Please provide all details.",
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    oorderPlacedDialog() {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          // return object of type Dialog
          return Dialog(
            elevation: 0.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              height: 175.0,
              padding: EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 70.0,
                    width: 70.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(35.0),
                      border: Border.all(color: primaryColor, width: 1.0),
                    ),
                    child: Icon(
                      Icons.check,
                      size: 40.0,
                      color: primaryColor,
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    "Your order has been placed!",
                    style: greyHeadingStyle,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
            ),
          );
        },
      );

      Future.delayed(const Duration(milliseconds: 3000), () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Home()),
        );
      });
    }

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(40.0),
          child: Padding(
            padding: EdgeInsets.all(fixPadding),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Confirm Order',
                  style: bigHeadingStyle,
                ),
                Column(

                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'ID:',
                      style: greyHeadingStyle,
                    ),
                    Text(
                      DOCID,
                      // widget.docId,
                      style:TextStyle(
                        fontSize: 10.0,
                        color: primaryColor,
                        fontFamily: 'Lato',
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          // Delivert to Start
          deliveryTo(),
          // Delivert to End
          heightSpace,
          // Delivery Time Start
          Container(
            padding: EdgeInsets.all(fixPadding),
            color: Colors.grey.withOpacity(0.1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Delivery Time',
                  style: greyHeadingStyle,
                ),
                widthSpace,
                Text(
                  '45 min',
                  style: headingStyle,
                ),
              ],
            ),
          ),
          // Delivery Time End

          // Order Item Start
          orderItem(),
          // Order Item End

          // Add Voucher Start
          addVoucher(),
          // Add Voucher End
          heightSpace,
          // Note Start
          enterNote(),
          // Note End

          // Payment Method Start
          paymentMethod(),
          // Payment Method End

          // Confirm Button
          Container(
            padding: EdgeInsets.all(fixPadding),
            child: SizedBox(
              height: 50.0,
              width: width - (fixPadding * 2.0),
              child: RaisedButton(
                elevation: 0.0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // ordersService
                onPressed: () async {
                  print(Uid);
                  openCheckout();
                  oorderPlacedDialog();
                  FlutterOpenWhatsapp.sendSingleMessage("+91 9096266950", "Complete Your Car");
                  /*oorderPlacedDialog();
    orderConfirm();*/

                },
                color: primaryColor,
                child: Text(
                  'Confirm',
                  style: wbuttonWhiteTextStyle,
                ),
              ),
            ),
          ),
          // Confirm Button
        ],
      ),
    );
  }

  deliveryTo() {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    return Padding(
      padding: EdgeInsets.all(fixPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Delivery to',
                style: headingStyle,
              ),
              /*InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AddNewAddress()));
                },
                child: Text(
                  'Add New Address',
                  style: blueTextStyle,
                ),
              ),*/
            ],
          ),
          heightSpace,
          Container(
            width: width - fixPadding * 2.0,
            height: 140.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    blurRadius: 1.0,
                    spreadRadius: 1.0,
                    color: Colors.grey[300]),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(fixPadding),
                  child: Container(
                    width: 120.0,
                    height: 120.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      image: DecorationImage(
                        image:
                        AssetImage('assets/user/restaurant_location.jpg'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Container(
                  width: width - 160.0,
                  height: 140.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.location_on,
                            color: blackColor,
                            size: 20.0,
                          ),
                          widthSpace,
                          Container(
                            width: width - 200.0,
                            child: Text(
                              _adress,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              style: TextStyle(
                                fontSize: 16.0,
                                color: blackColor,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ],
                      ),
                      heightSpace,
                      heightSpace,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.person,
                            color: blackColor,
                            size: 20.0,
                          ),
                          widthSpace,
                          Text(
                            _name,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: blackColor,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                      heightSpace,
                      heightSpace,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.phone,
                            color: blackColor,
                            size: 20.0,
                          ),
                          widthSpace,
                          Text(
                            _phone,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: blackColor,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  orderItem() {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    return Container(
      width: width,
      padding: EdgeInsets.all(fixPadding),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 70.0,
                width: 70.0,
                alignment: Alignment.topRight,
                padding: EdgeInsets.all(fixPadding),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  image: DecorationImage(
                    image: NetworkImage(_image,),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              widthSpace,
              widthSpace,
              Container(
                width: width - ((fixPadding * 2) + 90.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      _productName,
                      style: listItemTitleStyle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    heightSpace,
                    Text(
                      _desription,
                      style: listItemSubTitleStyle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
            ],
          ),
          heightSpace,
          heightSpace,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Subtotal (1 item)',
                style: listItemTitleStyle,
              ),
              Text(
                '\Rs$_price',
                style: headingStyle,
              ),
            ],
          ),
          heightSpace,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "Ship Charge",
                style: listItemTitleStyle,
              ),
              Text(
                '\Rs$shipcharge',
                style: headingStyle,
              ),
            ],
          ),
          heightSpace,
          Divider(),
          heightSpace,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Total',
                style: priceStyle,
              ),
              Text(
                /*  doAddition();*/
                '$result',
                /*'$totalPrice',*/
                /* '\$totalPrice',*/
                style: priceStyle,
              ),
            ],
          ),
        ],
      ),
    );
  }

  addVoucher() {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    return Column(
      children: <Widget>[
        Container(
          width: width,
          padding: EdgeInsets.all(fixPadding),
          color: Colors.grey.withOpacity(0.1),
          child: Text(
            'Add Voucher',
            style: greyHeadingStyle,
          ),
        ),
        Container(
          width: width,
          padding: EdgeInsets.all(fixPadding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: width - ((fixPadding * 2) + 75.0),
                child: TextField(
                  onChanged: (val) {
                    setState(() => applycoupan = val);
                  },
                  style: moreStyle,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(15.0),
                    hintText: 'Add Voucher Code',

                    hintStyle: moreStyle,
                    prefixIcon: Icon(
                      Icons.local_activity,
                      color: primaryColor,
                    ),
                  ),
                ),
              ),
              widthSpace,
              InkWell(
                child: Container(

                  width: 65.0,
                  padding: EdgeInsets.all(fixPadding),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: primaryColor,
                    borderRadius: BorderRadius.circular(10.0),
                  ),

                  child: Text(
                    'Apply',
                    style: whiteSubHeadingStyle,
                  ),
                ),
                onTap: () {
                  print(applycoupan);
                  getapplycode();
                },
              )
            ],
          ),
        ),
      ],
    );
  }

  enterNote() {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: width,
          padding: EdgeInsets.all(fixPadding),
          color: Colors.grey.withOpacity(0.1),
          child: Text(
            'CarNo',
            style: greyHeadingStyle,
          ),
        ),
        Container(
          width: width,
          padding: EdgeInsets.all(fixPadding),
          child: TextField(
            onChanged: (val) {
              setState(() => note = val);
            },
            keyboardType: TextInputType.multiline,
            maxLines: 1,
            decoration: InputDecoration(
              hintText: 'Enter Car No Here',
              border: InputBorder.none,
              filled: true,
            ),
          ),
        ),
        heightSpace,
      ],
    );
  }

  paymentMethod() {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    return Container(
        child:Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
        if (_categories != null && _categories.length > 0) ...[

          DropdownButtonFormField(
            decoration: InputDecoration(
              filled: true,
              labelText: "Car Types",
            ),
            items: _categories.map((c) {
              return DropdownMenuItem(
                value: c.id,
                child: Text(c.id),
              );
            }).toList(),
            value: cartypes,
            onChanged: (v) {
              setState(() {
                cartypes = v;
              });
            },
          ),
        ],
            ],
        ),
    );
  }


  Future<void> scratchCardDialogForFirst(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          title: Align(
            alignment: Alignment.center,
            child: Text(
              'You\'ve won a scratch card',
              style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
          ),
          content: StatefulBuilder(builder: (context, StateSetter setState) {
            return Scratcher(
              accuracy: ScratchAccuracy.low,
              threshold: 25,
              brushSize: 50,
              color: Colors.yellow,
              onThreshold: () {
                AddUserCoupan(
                  Uid,
                  randomflat,
                );
                setState(() {
                  _opacity = 1;

                });
              },
              child: AnimatedOpacity(
                duration: Duration(milliseconds: 250),
                opacity: _opacity,
                child: Container(
                  height: 300,
                  width: 300,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          'assets/tropy.png'),
                    ),
                  ),
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    flat,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                        color: Colors.grey),
                  ),
                ),
              ),
            );
          }),
        );
      },
    );
  }

  Future<void> scratchCardDialogforrandom(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          title: Align(
            alignment: Alignment.center,
            child: Text(
              'You\'ve won a scratch card',
              style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
          ),
          content: StatefulBuilder(builder: (context, StateSetter setState) {
            return Scratcher(
              accuracy: ScratchAccuracy.low,
              threshold: 25,
              brushSize: 50,
              color: Colors.yellow,
              onThreshold: () {
                orderConfirm();
                AddUserCoupan(
                  Uid,
                  randomflat,
                );
                setState(() {
                  _opacity = 1;
                });
              },
              child: AnimatedOpacity(
                duration: Duration(milliseconds: 550),
                opacity: _opacity,
                child: Container(
                  height: 300,
                  width: 300,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          'assets/tropy.png'),
                    ),
                  ),
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    randomflat,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                        color: Colors.grey),
                  ),
                ),
              ),
            );
          }),
        );
      },
    );
  }

  checkIfLikedOrNot() async {
    final CollectionReference dbIDS = Firestore.instance.collection(
        'ordersService');
    QuerySnapshot _query = await dbIDS
        .where('uid', isEqualTo: Uid)
        .getDocuments();
    final CollectionReference Coupan = Firestore.instance.collection('coupans');

    QuerySnapshot _querycoupan = await Coupan
        .where('RandomNumber', isEqualTo: randomNumber)
        .getDocuments();

    final CollectionReference UserCoupan = Firestore.instance.collection('ASSS');

    QuerySnapshot _queryusercoupan = await UserCoupan
        .where('Uid', isEqualTo: Uid)
        .where('flat', isEqualTo: flat)
        .getDocuments();


    if (_query.documents.length > 0 && _queryusercoupan.documents.length > 0) {
      // the ID exists
      orderConfirm();
      print("there are documents");
    }else if(_queryusercoupan.documents.length > 0){
      orderConfirm();
      print("No Coupan");
    }
    else  {
      scratchCardDialogForFirst(context);
      scratchCardDialogforrandom(context);
      print("No");
    }

  }
}