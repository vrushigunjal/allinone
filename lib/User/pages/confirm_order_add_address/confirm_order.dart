import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/User/helper/OrderConfirm.dart';
import 'package:all_in_one_app/User/helper/user.dart';
import 'package:all_in_one_app/User/pages/confirm_order_add_address/add_new_address.dart';
import 'package:all_in_one_app/User/pages/home/home.dart';
import 'package:all_in_one_app/carWasher/helper/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'dart:math';
import 'dart:math';


class ConfirmOrder extends StatefulWidget {

  final List cart;
  ConfirmOrder({@required this.cart});
  @override

  _ConfirmOrderState createState() => _ConfirmOrderState();
}

class _ConfirmOrderState extends State<ConfirmOrder> {

  static const platform = const MethodChannel("razorpay_flutter");

  Razorpay _razorpay;
  bool visa = true;
  bool masterCard = false;

  String _adress = '';
  String _name = '';
  String _phone = '';
  String _productName = '';
  String _desription = '';
  String _image = '';
  double _price;
  String _id;



  int shipcharge = 10;
 int randomnumber = 123;
  double result = 0;
  String applycoupan = "0";
  String note = "";
  String order = "ordered";



  void initState() {
    getshared();
    super.initState();
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }

  void openCheckout() async {
    var options = {
      'key': 'rzp_test_NNbwJ9tmM0fbxj',
      'amount': 100.00,
      'name': 'Shaiq',
      'description': 'Payment',
      'prefill': {'contact': '8888888888', 'email': 'test@razorpay.com'},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint(e);
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    Fluttertoast.showToast(
        msg: "SUCCESS: " + response.paymentId, timeInSecForIos: 4);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Fluttertoast.showToast(
        msg: "ERROR: " + response.code.toString() + " - " + response.message,
        timeInSecForIos: 4);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName, timeInSecForIos: 4);
  }
  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _adress = (prefs.getString('CURRENTADRESS') ?? '');
      _name = (prefs.getString('FULLNAME') ?? '');
      _phone = (prefs.getString('PHONENUMBER') ?? '');
      _productName = (prefs.getString('Productname') ?? '');
      _desription = (prefs.getString('ProductDiscription') ?? '');
      _price = (prefs.getDouble('Productprice').toInt() ?? '');
      result = _price+ shipcharge;
      _image = (prefs.getString('productImage') ?? '');
      _id = (prefs.getString('uid') ?? '');

    });
  }
  void getapplycode(){
    result = _price + shipcharge - int.parse(applycoupan);
  }

  static var _random = new Random();
  static var _diceface = _random.nextInt(6) +1 ;
  orderConfirm(){

      if (_phone.isNotEmpty!= null) {


          print(
              "$_phone \n $_name \n $_desription");

          addOrder(
            _phone,
            _image,
            _name,
            _adress,
            _productName,
            _desription,
            _price,
            shipcharge,
            result,
            applycoupan,
            note,
            order,

            ).then((v) {
              Navigator.pop(context, "done");
            });
        } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }

  }
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;


    oorderPlacedDialog() {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          // return object of type Dialog
          return Dialog(
            elevation: 0.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              height: 175.0,
              padding: EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 70.0,
                    width: 70.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(35.0),
                      border: Border.all(color: primaryColor, width: 1.0),
                    ),
                    child: Icon(
                      Icons.check,
                      size: 40.0,
                      color: primaryColor,
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    "Your order has been placed!",
                    style: greyHeadingStyle,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
            ),
          );
        },
      );

      Future.delayed(const Duration(milliseconds: 3000), () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Home()),
        );
      });
    }

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(40.0),
          child: Padding(
            padding: EdgeInsets.all(fixPadding),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Confirm Order',
                  style: bigHeadingStyle,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'ID:',
                      style: greyHeadingStyle,
                    ),
            Text(
             "123",
             // widget.docId,
            style: headingStyle,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          // Delivert to Start
          deliveryTo(),
          // Delivert to End
          heightSpace,
          // Delivery Time Start
          Container(
            padding: EdgeInsets.all(fixPadding),
            color: Colors.grey.withOpacity(0.1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Delivery Time',
                  style: greyHeadingStyle,
                ),
                widthSpace,
                Text(
                  '45 min',
                  style: headingStyle,
                ),
              ],
            ),
          ),
          // Delivery Time End

          // Order Item Start
          orderItem(),
          // Order Item End

          // Add Voucher Start
          addVoucher(),
          // Add Voucher End
          heightSpace,
          // Note Start
          enterNote(),
          // Note End

          // Payment Method Start
          paymentMethod(),
          // Payment Method End

          // Confirm Button
          Container(
            padding: EdgeInsets.all(fixPadding),
            child: SizedBox(
              height: 50.0,
              width: width - (fixPadding * 2.0),
              child: RaisedButton(
                  elevation: 0.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  onPressed: () {
                    updaterandomnumber(_id,randomnumber);
                    oorderPlacedDialog();
                     orderConfirm();


                  },
                  color: primaryColor,
                  child: Text(
                    'Confirm',
                    style: wbuttonWhiteTextStyle,
                  ),
              ),
            ),
          ),
          // Confirm Button
        ],
      ),
    );
  }

  deliveryTo() {
    double width = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.all(fixPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Delivery to',
                style: headingStyle,
              ),
              /*InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AddNewAddress()));
                },
                child: Text(
                  'Add New Address',
                  style: blueTextStyle,
                ),
              ),*/
            ],
          ),
          heightSpace,
          Container(
            width: width - fixPadding * 2.0,
            height: 140.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    blurRadius: 1.0,
                    spreadRadius: 1.0,
                    color: Colors.grey[300]),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(fixPadding),
                  child: Container(
                    width: 120.0,
                    height: 120.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      image: DecorationImage(
                        image:
                            AssetImage('assets/user/restaurant_location.jpg'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Container(
                  width: width - 160.0,
                  height: 140.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.location_on,
                            color: blackColor,
                            size: 20.0,
                          ),
                          widthSpace,
                          Container(
                            width: width - 200.0,
                            child: Text(
                              _adress,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              style: TextStyle(
                                fontSize: 16.0,
                                color: blackColor,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ],
                      ),
                      heightSpace,
                      heightSpace,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.person,
                            color: blackColor,
                            size: 20.0,
                          ),
                          widthSpace,
                          Text(
                            _name,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: blackColor,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                      heightSpace,
                      heightSpace,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.phone,
                            color: blackColor,
                            size: 20.0,
                          ),
                          widthSpace,
                          Text(
                            _phone,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyle(
                              fontSize: 14.0,
                              color: blackColor,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  orderItem() {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      padding: EdgeInsets.all(fixPadding),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 70.0,
                width: 70.0,
                alignment: Alignment.topRight,
                padding: EdgeInsets.all(fixPadding),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  image: DecorationImage(
                    image: NetworkImage(_image),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              widthSpace,
              widthSpace,
              Container(
                width: width - ((fixPadding * 2) + 90.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      _productName,
                      style: listItemTitleStyle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    heightSpace,
                    Text(
                      _desription,
                      style: listItemSubTitleStyle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
            ],
          ),
          heightSpace,
          heightSpace,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Subtotal (1 item)',
                style: listItemTitleStyle,
              ),
              Text(
                '\Rs$_price',
                style: headingStyle,
              ),
            ],
          ),
          heightSpace,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "Ship Charge",
                style: listItemTitleStyle,
              ),
              Text(
                '\Rs$shipcharge',
                style: headingStyle,
              ),
            ],
          ),
          heightSpace,
          Divider(),
          heightSpace,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Total',
                style: priceStyle,
              ),
              Text(
                /*  doAddition();*/
                '$result',
                /*'$totalPrice',*/
                /* '\$totalPrice',*/
                style: priceStyle,
              ),
            ],
          ),
        ],
      ),
    );
  }

  addVoucher() {
    double width = MediaQuery.of(context).size.width;
    return Column(
      children: <Widget>[
        Container(
          width: width,
          padding: EdgeInsets.all(fixPadding),
          color: Colors.grey.withOpacity(0.1),
          child: Text(
            'Add Voucher',
            style: greyHeadingStyle,
          ),
        ),
        Container(
          width: width,
          padding: EdgeInsets.all(fixPadding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: width - ((fixPadding * 2) + 75.0),
                child: TextField(
                  onChanged: (val){
                    setState(() => applycoupan = val);
                  },
                  style: moreStyle,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(15.0),
                    hintText: 'Add Voucher Code',

                    hintStyle: moreStyle,
                    prefixIcon: Icon(
                      Icons.local_activity,
                      color: primaryColor,
                    ),
                  ),
                ),
              ),
              widthSpace,
    InkWell(
    child: Container(

                width: 65.0,
                padding: EdgeInsets.all(fixPadding),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: primaryColor,
                  borderRadius: BorderRadius.circular(10.0),
                ),

                child: Text(
                  'Apply',
                  style: whiteSubHeadingStyle,
                ),
              ),
      onTap: () {
        print(applycoupan);
        getapplycode();
      },
    )
            ],
          ),
        ),
      ],
    );
  }

  enterNote() {
    double width = MediaQuery.of(context).size.width;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: width,
          padding: EdgeInsets.all(fixPadding),
          color: Colors.grey.withOpacity(0.1),
          child: Text(
            'Note',
            style: greyHeadingStyle,
          ),
        ),
        Container(
          width: width,
          padding: EdgeInsets.all(fixPadding),
          child: TextField(
            onChanged: (val){
              setState(() => note = val);
            },
            keyboardType: TextInputType.multiline,
            maxLines: 3,
            decoration: InputDecoration(
              hintText: 'Enter Note Here',
              border: InputBorder.none,
              fillColor: Colors.grey.withOpacity(0.1),
              filled: true,
            ),
          ),
        ),
        heightSpace,
      ],
    );
  }

  paymentMethod() {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      height: 100.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          InkWell(
            onTap: () {
              openCheckout();
            },
            child: Padding(
              padding: EdgeInsets.all(fixPadding),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(
                      width: 1.0,
                      color: (masterCard) ? primaryColor : Colors.grey[300]),
                ),
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: 100.0,
                      padding: EdgeInsets.only(
                          top: fixPadding,
                          bottom: fixPadding,
                          left: fixPadding,
                          right: (masterCard) ? fixPadding * 4 : fixPadding),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: 30.0,
                            child: Image.asset('assets/payment/master_card.png',
                                fit: BoxFit.fitHeight),
                          ),
                          widthSpace,
                          Text(
                            '**** **** **** *316',
                            style: headingStyle,
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      top: 0.0,
                      right: 0.0,
                      child: (masterCard)
                          ? Container(
                              height: 30.0,
                              width: 30.0,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10.0),
                                  bottomLeft: Radius.circular(30.0),
                                ),
                                color: primaryColor,
                              ),
                              alignment: Alignment.center,
                              child: Icon(Icons.check,
                                  color: whiteColor, size: 15.0),
                            )
                          : Container(),
                    ),
                  ],
                ),
              ),
            ),
          ),
          // Mastercard End
        ],
      ),
    );
  }
}
