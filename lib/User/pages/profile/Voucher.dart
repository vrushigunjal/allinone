import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/helpers/coupans.dart';
import 'package:all_in_one_app/User/pages/search/get_search_results.dart';
import 'package:all_in_one_app/carWasher/helper/Services.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Voucher extends StatefulWidget {
  @override
  _VoucherState createState() => _VoucherState();
}

class _VoucherState extends State<Voucher> {
  final List<String> _listItem = [
    'assets/codeback.png',
  ];
  var flat = '0';

  bool _loading = true;
  bool _empty = true;
  List<DocumentSnapshot> orders;
  DocumentSnapshot lastDocument;
  ScrollController _scrollController;

  String Uid = "123";
  int total = 0;

  @override
  void initState() {
    super.initState();
    getshared();


    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more orders");
        }
      });
  }

  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      Uid = (prefs.getString('Uid') ?? '');
      loadMyOrders();
    });
  }


  loadMyOrders() async {
    try {

      QuerySnapshot qSnap = await getAllCoupan(Uid);
      if (qSnap.size > 0) {
        setState(() {
          orders = qSnap.docs;
          _empty = false;
          _loading = false;
        });
      } else {
        setState(() {
          _empty = true;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _empty = true;
        _loading = false;
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    if (_loading == true) {
      return Container(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      );
    }
    if (_empty == true) {
      return Empty(text: "No Coupan!");
    }
    List<dynamic> cart = orders;
     total = cart.map<int>((m) => int.parse(m["flat"])).reduce((a,b )=>a+b) ??'';
    print(total);


    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.yellow,
          elevation: 0,
          title: Text("Voucher",style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),),

        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(

              child: Column(

                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text('$total', style: TextStyle(color: Colors.grey, fontSize: 35, fontWeight: FontWeight.bold),),

                 /* Text("100Rs", style: TextStyle(color: Colors.grey, fontSize: 35, fontWeight: FontWeight.bold),),*/
                  SizedBox(height: 30,),
                  Container(
                        height: 50,
                        margin: EdgeInsets.symmetric(horizontal: 40),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white
                        ),
                        child: Center(child: Text("TotalEarning", style: TextStyle(color: Colors.grey[900], fontWeight: FontWeight.bold),)),
                      ),
                  SizedBox(height: 30,),
                ],

              ),


                  ),

                  Container(
                  height: height,
                    padding: EdgeInsets.only(top: 30.0),
                    child: GridView.count(

                      crossAxisCount: 2 ,
                      children: cart.map((c) {
                        return Container(
                          child: Card(
                            child:  Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.all(fixPadding),
                              padding: EdgeInsets.all(fixPadding),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  image: DecorationImage(
                                    image: AssetImage("assets/tropy.png"),
                                  )
                              ),
                              child:new Text(c["flat"],
                                  textAlign: TextAlign.start,
                                  style: new TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0,

                                  )
                              ),
                            ),
                          ),
                        );

                      }).toList(),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}