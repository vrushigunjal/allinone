import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactUs extends StatefulWidget {

  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  @override

  Widget build(BuildContext context) {
      return Scaffold(
backgroundColor: Colors.grey[400],
        appBar: AppBar(title: Text('Contact Us'),
          backgroundColor: Colors.yellow[400],
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[

            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              width:double.infinity,
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text("reach Us At",style: TextStyle(fontSize: 20)),


                ],

              ),

            ),
            new Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              width: double.infinity,
              height: 90,

              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("LINECHART SOLUTIONS PRIVATE LIMITED \n"
                      "Ram Nagar, Tathawade, Pimpri-Chinchwad,\n "
                      "Maharashtra 411033",style: TextStyle(fontSize: 15)),

                ],

              ),
            ),

            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              width:double.infinity,
              height: 50,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("Contact Us",style: TextStyle(fontSize: 20)),


                  ],

                ),

              ),
            Container(

              width:double.infinity,
              height: 20,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("(+91) 9096 266 950",style: TextStyle(fontSize: 15)),


                  ],

                ),

              ),

            Container(

              width:double.infinity,
              height: 20,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("(+91) 9422 779 972",style: TextStyle(fontSize: 15)),


                ],

              ),

            ),


            new Container(
              width: double.infinity,
              height: 20,

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("(+91) 9049 123 370",style: TextStyle(fontSize: 15)),

                  ],

              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              width:double.infinity,
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text("Email Us At",style: TextStyle(fontSize: 20)),


                ],

              ),

            ),




            new Container(

              width: double.infinity,
              height: 20,

              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("info@linechartsolutions.com",style: TextStyle(fontSize: 15)),

                ],

              ),
            ),
            new Container(

              width: double.infinity,
              height: 20,

              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("sales@linechartsolutions.com",style: TextStyle(fontSize: 15)),

                ],

              ),
            ),


          ],
        ),
      );
    }
  }