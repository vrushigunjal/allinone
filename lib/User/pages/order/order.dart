import 'package:all_in_one_app/User/pages/order/history_order/history.dart';
import 'package:all_in_one_app/User/pages/order/track_order.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter/material.dart';

import 'ongoing_order.dart';


class Order extends StatefulWidget {
  @override
  _OrderState createState() => _OrderState();
}

class _OrderState extends State<Order> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        backgroundColor: scaffoldBgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: whiteColor,
          title: Text(
            'Order',
            style: bigHeadingStyle,
          ),
          bottom: TabBar(
            unselectedLabelColor: Colors.grey.withOpacity(0.3),
            labelColor: primaryColor,
            indicatorColor: primaryColor,
            tabs: [
              Tab(text: 'Ongoing'),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            OngoingOrder(),
          ],
        ),
      ),
    );
  }
}
