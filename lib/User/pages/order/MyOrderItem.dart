import 'package:all_in_one_app/Admin/config/currency.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';

class OrderItem extends StatelessWidget {
  final DocumentSnapshot orderSnapshot;
  final Function onTap;

  OrderItem({@required this.orderSnapshot, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    var order = orderSnapshot.data();
    var id = orderSnapshot.id;
    DateTime createdAt = order['created_at'].toDate();
    String formattedDate =
        "${createdAt.year}/${createdAt.month}/${createdAt.day} ${createdAt.hour}:${createdAt.minute}:${createdAt.second}";
    List<dynamic> cart = order['cart'];
    return InkWell(
      onTap: onTap,
      child: Container(
        child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 10,
                  ),
                  child: Column(
                      children: cart.map((c) {
                      return Container(
                        width: width,
                        height: 150.0,
                        margin: (c == 0)
                            ? EdgeInsets.all(fixPadding)
                            : EdgeInsets.only(
                            right: fixPadding,
                            left: fixPadding,
                            bottom: fixPadding),
                        decoration: BoxDecoration(
                          color: whiteColor,
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children:<Widget> [
                            Container(
                              height: 150.0,
                              width: 100.0,
                              alignment: Alignment.topRight,
                              padding: EdgeInsets.all(fixPadding),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.horizontal(
                                    left: Radius.circular(5.0)),
                                image: DecorationImage(
                                  image: NetworkImage(c['productImage']),
                                  fit: BoxFit.cover,
                                ),
                              ),
                              /*child: FadeInImage(
                                image: NetworkImage(c["productImage"]),
                                placeholder:
                                AssetImage("assets/placeholder.png"),
                                width: 60,
                                height: 60,
                                fit: BoxFit.cover,
                              ),*/
                            ),
                            SizedBox(
                              width: 14,
                            ),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                Text(
                                id,
                                softWrap: true,
                                style: headingWasherStyle,
                              ),

                                  Text(
                                    c["productName"],
                                    softWrap: true,
                                    style: rsStyle,
                                  ),

                                  Column(
                                    children: [
                                      Text(
                                        "$currencySymbol${c['salePrice'].toString()}",
                                        softWrap: true,
                                        style:
                                          rsStyle,
                                      ),
                                    ],
                                  ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                  Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          if (order['orderStatus'] == "ordered") ...[
                                            Row(
                                              children: [
                                                Icon(
                                                  EvaIcons.shoppingCartOutline,
                                                  color: Colors.orange,
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  order['orderStatus'],
                                                  style: TextStyle(
                                                    color: Colors.orange,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ] else if (order['orderStatus'] == "Ship") ...[
                                            Row(
                                              children: [
                                                Icon(
                                                  EvaIcons.carOutline,
                                                  color: Colors.blue,
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  order['orderStatus'],
                                                  style: TextStyle(
                                                    color: Colors.blue,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ] else if (order['orderStatus'] == "OnTheWay") ...[
                                            Row(
                                              children: [
                                                Icon(
                                                  EvaIcons.giftOutline,
                                                  color: Colors.red,
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  order['orderStatus'],
                                                  style: TextStyle(
                                                    color: Colors.deepOrange,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ] else if (order['orderStatus'] == "Diliverd") ...[
                                            Row(
                                              children: [
                                                Icon(
                                                  EvaIcons.arrowCircleRightOutline,
                                                  color: Colors.green,
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  order['orderStatus'],
                                                  style: TextStyle(
                                                    color: Colors.red,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],

                                          OutlineButton(
                                            padding: EdgeInsets.symmetric(
                                              vertical: 5,
                                            ),
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(4),
                                            ),
                                            textColor: Colors.grey,
                                            highlightedBorderColor: Colors.black,
                                            onPressed: onTap,
                                            child: Text("Track"),
                                          ),
                                        ],
                                      ),




                                ],
                              ),
                            ),
                          ],


                        ),
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
              ],


    ),
          ),
      );


  }
}
