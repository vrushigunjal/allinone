

import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/User/helper/OrderConfirm.dart';
import 'package:all_in_one_app/User/pages/order/MyOrderItem.dart';
import 'package:all_in_one_app/User/pages/order/track_order.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';

class OngoingOrder extends StatefulWidget {
  final DocumentSnapshot document;
  OngoingOrder({@required this.document});

  @override
  _OngoingOrderState createState() => _OngoingOrderState();
}

class _OngoingOrderState extends State<OngoingOrder> {

  bool _loading = true;
  bool _empty = true;
  List<DocumentSnapshot> orders;
  DocumentSnapshot lastDocument;

  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    loadMyOrders();
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more orders");

        }
      });
  }


  loadMyOrders() async {
    try {
      QuerySnapshot qSnap = await getAllOrdersOfUser();
      if (qSnap.size > 0) {
        setState(() {

          orders = qSnap.docs ;
            _empty = false;
            _loading = false;
        });
      } else {
        setState(() {
          _empty = true;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _empty = true;
        _loading = false;
      });
    }
  }
  @override
  Widget build(BuildContext context) {

    if (_loading == true) {
      return Container(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      );
    }
    if (_empty == true) {
      return Empty(text: "No orders!");
    }
    return ListView.builder(
      controller: _scrollController,
      itemCount: orders.length,
      itemBuilder: (ctx, i) {
        return OrderItem(
          orderSnapshot: orders[i],
          onTap: () async {
            String result = await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => HistoryOrder(
                  document: orders[i],
                ),
              ),
            );

          },
        );
      },
      /*controller: _scrollController,
      itemCount: orders.length,
      physics: BouncingScrollPhysics(),
      itemBuilder: (context, index) {
        final item = orders[index];

        return InkWell(
          onTap: () {
            Navigator.push(
                context,
                PageTransition(
                    type: PageTransitionType.fade, child: HistoryOrder(  document: orders[index],)));
          },
          child: Container(
            width: width,
            height: 100.0,
            margin: (index == 0)
                ? EdgeInsets.all(fixPadding)
                : EdgeInsets.only(
                    right: fixPadding, left: fixPadding, bottom: fixPadding),
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 100.0,
                  width: 90.0,
                  alignment: Alignment.topRight,
                  padding: EdgeInsets.all(fixPadding),
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.horizontal(left: Radius.circular(5.0)),
                    image: DecorationImage(
                      image: NetworkImage(item['productImage']),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  width: width - ((fixPadding * 2) + 100.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(fixPadding),
                        child: Text(
                          item['productName'],
                          style: listItemTitleStyle,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: fixPadding, right: fixPadding),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 10.0,
                              height: 10.0,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                border:
                                    Border.all(width: 1.0, color: primaryColor),
                              ),
                            ),
                            widthSpace,
                          *//*  Text(
                              item['applyCoupon'],
                              style: listItemSubTitleStyle,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),*//*
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: fixPadding,
                            bottom: fixPadding,
                            left: fixPadding),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                           *//* Text(
                              '${item['orderStatus']}',
                              style: listItemSubTitleStyle,
                            ),*//*
                           *//* Text(
                              item['orderStatus'],
                              style: TextStyle(
                                fontSize: 14.0,
                                color: (item['order'] == 'On the Way')
                                    ? primaryColor
                                    : Colors.blue,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.normal,
                              ),*//*

                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },*/
    );
  }
}
