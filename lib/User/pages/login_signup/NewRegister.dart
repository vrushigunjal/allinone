import 'dart:io';
import 'dart:math';

import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/helpers/car.dart';
import 'package:all_in_one_app/User/helper/messagemodel.dart';
import 'package:all_in_one_app/User/helper/user.dart';
import 'package:all_in_one_app/User/pages/home/home.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}
enum ImagePickCategory { camera, phone }
enum BestTutorSite { NormalUser, OlaUser, UberUser }
class _SignUpScreenState extends State<SignUpScreen> {

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  String userType = "UserSelf";
  bool _loading = false;
  int randomnumber = 123;
  String finalrandom = "123";
  List<QueryDocumentSnapshot> _categories;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final List<Message> messages = [];
  File _productImage;
  String name,dateofBirth,email,currentAddress,carno;
  String cartypes = '';
  String _phone = '';
  BestTutorSite _site = BestTutorSite.NormalUser;
  String Radiovalue  ;
  DateTime _selectedDate;
  TextEditingController _textnameEditingController = TextEditingController();
  TextEditingController _textDateEditingController = TextEditingController();
  TextEditingController _textemailEditingController = TextEditingController();

  _SignUpScreenState({Key key, this.name, this.dateofBirth,this.email,this.currentAddress, this.carno,});

  @override
  void initState() {
    _getCurrentLocation();
    loadCategories();
    getshared();
    super.initState();
  }

  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() async {
        currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('CURRENTADRESS', currentAddress);
      });
    } catch (e) {
      print(e);
    }
  }
  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _phone = (prefs.getString('PHONENUMBER') ?? '');
    });
  }
  loadCategories() async {
    try {
      setState(() {
        _loading = true;
      });

      QuerySnapshot qsnap = await getCarType();
      setState(() {
        _categories = qsnap.docs;
        cartypes = qsnap.docs[0].id;
        _loading = false;
      });
    } catch (e) {
      print(e);
    }
  }
  onCreateuserBtnTap() async {
    try {
       name = _textnameEditingController.text;
      email = _textemailEditingController.text;
      dateofBirth = _textDateEditingController.text;
      if (name.isNotEmpty &&
          dateofBirth.isNotEmpty&&
          email.isNotEmpty && currentAddress.isNotEmpty != null) {
        String productImageName = path.basename(_productImage.path);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('FULLNAME', name);
        prefs.setString('EMAIL', email);

        uploadProductImage(
          productImageName,
          _productImage,
        ).then((downloadURL) {
          editPhoneNumber(
            downloadURL,
            finalrandom,
            name,
            _phone,
            dateofBirth,
            email,
            currentAddress,
            Radiovalue,
            true,
          ).then((v) {
            Navigator.push(context,  MaterialPageRoute(builder: (context) => Home()));
          });
        }).catchError((e) {
          print(e);
          Navigator.pop(context, "failed");
        });

      } else {
        showMyDialog(
          context: context,
          title: "oops",
          description: "Please provide all details.",
        );
      }
    } catch (e) {
      print(e);
    }
  }


  _pickImage() async {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10.0),
          topRight: Radius.circular(10.0),
        ),
      ),
      builder: (ctx) {
        return Container(
          height: 200,
          color: Colors.transparent,
          child: ListView(
            children: [
              ListTile(
                leading: Icon(Icons.camera_alt),
                title: Text("Take a Picture from Camera"),
                onTap: () {
                  _takeImage(ImagePickCategory.camera);

                  Navigator.pop(ctx);
                },
              ),
              ListTile(
                leading: Icon(Icons.image),
                title: Text("Select a Picture from Device"),
                onTap: () {
                  _takeImage(ImagePickCategory.phone);
                  Navigator.pop(ctx);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  _takeImage(ImagePickCategory selection) async {
    try {
      final picker = ImagePicker();

      File _tempFile;

      // pick image
      if (selection == ImagePickCategory.camera) {
        final pickedFile = await picker.getImage(source: ImageSource.camera);
        _tempFile = File(pickedFile.path);
      } else if (selection == ImagePickCategory.phone) {
        final pickedFile = await picker.getImage(source: ImageSource.gallery);
        _tempFile = File(pickedFile.path);
      }

      // crop image

      File croppedFile = await ImageCropper.cropImage(
        sourcePath: _tempFile.path,
        compressQuality: 30,
        compressFormat: ImageCompressFormat.jpg,
        maxHeight: 1920,
        maxWidth: 1920,
      );

      print(croppedFile.lengthSync());

      setState(() {
        _productImage = croppedFile;
      });
    } catch (e) {
      print(e);
    }
  }



  @override
  Widget build(BuildContext context) {
    Random seed = Random();
    const int MAX_VALUE = 10;

    int val1 = seed.nextInt(MAX_VALUE);

    int randomnumber = val1 + MAX_VALUE;
    finalrandom = '$randomnumber';
    print(finalrandom);
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[

          SizedBox(
            height: 20,
          ),
      InkWell(
      onTap: (){
        _pickImage();
      },
      child: _productImage == null
          ? Container(
        height: 280,
        width: 200.0,
        color: Colors.grey,
        child: Icon(Icons.add_a_photo),
      )  : Image.file(
        _productImage,
        height: 280,
        fit: BoxFit.cover,
      ),
      ),

          Padding(
            padding: const EdgeInsets.only(top:10.0,right:20.0,left: 20.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.person), onPressed: null),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 20, left: 10),
                        child: TextField(
                          controller: _textnameEditingController,
                          decoration: InputDecoration(hintText: 'Username'),
                        )))
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top:10.0,right:20.0,left: 20.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.date_range),
                onPressed: () {
                  _selectDate(context);
                },
                ),
                Expanded(

                    child: Container(
                        margin: EdgeInsets.only(right: 20, left: 10),
                        child: TextField(
                          controller: _textDateEditingController,
                          decoration: InputDecoration(hintText: 'Date Of Birth'),
                          keyboardType: TextInputType.datetime,
                        )
                    )

                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top:10.0,right:20.0,left: 20.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.mail), onPressed: null),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 20, left: 10),
                        child: TextField(
                          controller: _textemailEditingController,
                          decoration: InputDecoration(hintText: 'Email'),
                        )))
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top:10.0,right:20.0,left: 20.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.add_location), onPressed: null),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(right: 20, left: 10),
                        child: TextField(
                          decoration: InputDecoration(
                              hintText: currentAddress,
                          ),
                        )))
              ],
            ),
          ),

          ListTile(
            title: const Text('NormalUser'),
            leading: Radio(
              value: BestTutorSite.NormalUser,
              groupValue: _site,
              onChanged: (BestTutorSite value) {
                setState(() {
                  _site = value;
                  Radiovalue = "NormalUser";
                });
              },
            ),
          ),
          ListTile(
            title: const Text('OlaUser'),
            leading: Radio(
              value: BestTutorSite.OlaUser,
              groupValue: _site,
              onChanged: (BestTutorSite value) {
                setState(() {
                  _site = value;
                  Radiovalue = "OlaUser";
                });
              },
            ),
          ),
          ListTile(
            title: const Text('UberUser'),
            leading: Radio(
              value: BestTutorSite.UberUser,
              groupValue: _site,
              onChanged: (BestTutorSite value) {
                setState(() {
                  _site = value;
                  Radiovalue = "UberUser";
                });
              },
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: Container(
                height: 60,
                child: RaisedButton(
                  onPressed: () {
                    onCreateuserBtnTap();
                   // Navigator.pushNamed(context, 'Home');
                  },
                  color: Color(0xFF00a79B),
                  child: Text(
                    'SIGN UP',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),

    );
  }

  _selectDate(BuildContext context) async {
    DateTime newSelectedDate = await showDatePicker(
        context: context,
        initialDate: _selectedDate != null ? _selectedDate : DateTime.now(),
        firstDate: DateTime(2000),
        lastDate: DateTime(2040),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: ColorScheme.dark(
                primary: Colors.deepPurple,
                onPrimary: Colors.white,
                surface: Colors.blueGrey,
                onSurface: Colors.yellow,
              ),
              dialogBackgroundColor: Colors.blue[500],
            ),
            child: child,
          );
        });

    if (newSelectedDate != null) {
      _selectedDate = newSelectedDate;
      _textDateEditingController
        ..text = DateFormat.yMMMd().format(_selectedDate)
        ..selection = TextSelection.fromPosition(TextPosition(
            offset: _textDateEditingController.text.length,
            affinity: TextAffinity.upstream));
    }
  }
}

