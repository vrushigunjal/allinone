import 'dart:async';
import 'dart:io';


import 'package:all_in_one_app/User/helper/auth.dart';
import 'package:all_in_one_app/User/helper/user.dart';
import 'package:all_in_one_app/User/pages/login_signup/otp_screen.dart';
import 'package:all_in_one_app/User/pages/login_signup/register.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:international_phone_input/international_phone_input.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:all_in_one_app/User/pages/home/home.dart';


class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  final auth = FirebaseAuth.instance;
  String phoneNumber, verificationId, smsCode;
  bool codeSent = false;
  String phoneIsoCode;
  DateTime currentBackPressTime;
  String FullphoneNumber;
  String smsOTP;
  bool _initialized = false;
  bool _error = false;
  String Uid = "";
  bool isValid = false;
  String phone = '';

  Future<Null> validate(StateSetter updateState) async {
    await Firebase.initializeApp();
    print("in validate : ${phoneNumber.length}");
    if (phoneNumber.length == 10) {
      updateState(() {
        isValid = true;
      });
    }
  }

  void initState() {

    initializeFlutterFire();
    loadUserDetails();
    super.initState();
  }
  loadUserDetails() async {
    try {
      setState(() async {
      DocumentSnapshot user = await getUser();
      phone = user.data()["PhoneNumber"] ?? '';
      print(phone);
      });
    } catch (e) {
      print(e);
    }
  }

  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      Uid = (prefs.getString('Uid') ?? '');
    });
  }

  void initializeFlutterFire() async {
    try {
      // Wait for Firebase to initialize and set `_initialized` state to true
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
      });
      checkUserAuth();
    } catch (e) {
      // Set `_error` state to true if Firebase initialization fails
      setState(() {
        _error = true;
      });
    }
  }

  void checkUserAuth() async {
    auth.authStateChanges().listen((User user) {
      if (user != null) {
        getUser().then((u) async {
          final bool isUser = u.data()["isUser"];
          if (isUser == true) {

                 Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Home()));
          } else {
            Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => OTPScreen(mobileNumber: FullphoneNumber,)));
          }
        }).catchError((e) {
          print(e);
        });
      } else {
        // user is signed out
      }
    });
  }



  _LoginState({Key key, this.phoneNumber, this.verificationId,});

  void onPhoneNumberChange(String number, String internationalizedPhoneNumber,
      String isoCode) {
    setState(() {
      phoneNumber = number;
      phoneIsoCode = isoCode;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: Text(
          "Continue with phone",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        elevation: 5,
        centerTitle: true,
        textTheme: Theme
            .of(context)
            .textTheme,
      ),
      body: WillPopScope(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(fixPadding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  heightSpace,
                  heightSpace,
                  heightSpace,
                  heightSpace,
                  heightSpace,
                  heightSpace,
                  Image.asset(
                    "assets/holdingphone.png",
                    width: 200.0,
                    fit: BoxFit.fitWidth,
                  ),
                  heightSpace,
                  heightSpace,
                  Text("OTP Verification", style: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.w700,
                    color: Color(0xFF000000),)),
                  heightSpace,
                  heightSpace,
                  Text("You'll receive a 6 digit code to verify next.",
                      style: greyHeadingStyleaa),
                  heightSpace,
                  heightSpace,
                  Container(
                    padding: EdgeInsets.only(left: fixPadding),
                    decoration: BoxDecoration(
                      color: whiteColor,
                      borderRadius: BorderRadius.circular(5.0),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          blurRadius: 1.5,
                          spreadRadius: 1.5,
                          color: Colors.grey[200],
                        ),
                      ],
                    ),
                    child: InternationalPhoneInput(
                      onPhoneNumberChange: onPhoneNumberChange,
                      initialPhoneNumber: phoneNumber,
                      initialSelection: phoneIsoCode,
                      enabledCountries: [
                        '+91',

                      ],
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        hintText: 'Phone Number',
                        hintStyle: greyHeadingStyle,
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                  heightSpace,
                  heightSpace,
                  heightSpace,
                  heightSpace,
                  Container(
                    child: SizedBox(
                      height: 50.0,
                      width: width - (fixPadding * 2.0),
                      child: RaisedButton(
                        elevation: 0.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        onPressed: () async {
                          FullphoneNumber = phoneNumber;
                          SharedPreferences prefs = await SharedPreferences
                              .getInstance();
                          prefs.setString('PHONENUMBER', FullphoneNumber);
                         // initializeFlutterFire();
                           Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OTPScreen(mobileNumber:FullphoneNumber)));


                          /*final CollectionReference dbIDS = Firestore.instance.collection(
                              'usersRegistration');

                          QuerySnapshot _query = await dbIDS
                              .where('uid', isEqualTo: Uid)
                              .getDocuments();
                          if (_query.documents.length < 0) {
                            Navigator.push(context,  MaterialPageRoute(builder: (context) => Home()));
                            print("there are documents");
                          }else{
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OTPScreen(mobileNumber:FullphoneNumber)));
                          }
*/
                          /*  Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OTPScreen(mobileNumber:FullphoneNumber)));*/

                        },
                        color: primaryColor,
                        child: Text(
                          'GET OTP',
                          style: wbuttonWhiteTextStyle,
                        ),
                      ),
                    ),
                  ),


                ],
              ),
            ),
          ],
        ),
        onWillPop: onWillPop,
      ),
    );
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(
        msg: 'Press Back Once Again to Exit.',
        backgroundColor: Colors.black,
        textColor: whiteColor,
      );
      return Future.value(false);
    }
    exit(0);
    return Future.value(true);
  }

  Future<void> verifyPhone(FullphoneNumber) async {
    await Firebase.initializeApp();

    final PhoneVerificationCompleted verified = (
        AuthCredential authResult) async {
      AuthService().signIn(authResult);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('OTP', this.smsCode);
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Register()));
    };


    final PhoneVerificationFailed verificationfailed =
        (FirebaseAuthException authException) {
      print('${authException.message}');
    };
    final PhoneCodeSent smsSent = (String verId, [int forceResend]) {
      this.verificationId = verId;
      setState(() async {
        this.codeSent = true;
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('OTP', this.smsCode);
      });
    };

    final PhoneCodeAutoRetrievalTimeout autoTimeout = (String verId) {
      this.verificationId = verId;
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: FullphoneNumber,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verified,
        verificationFailed: verificationfailed,
        codeSent: smsSent,
        codeAutoRetrievalTimeout: autoTimeout);
  }

  Future<void> checkinfoofuser(phoneNumber) async {
    final CollectionReference dbIDS = Firestore.instance.collection(
        'usersRegistration');

    QuerySnapshot _query = await dbIDS
        .where('PhoneNumber', isEqualTo: "7040131381")
        .getDocuments();

    if (_query.documents.length > 0) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
      print("there are documents");
    } else {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => OTPScreen(mobileNumber: FullphoneNumber)));
    }
  }

  Future<bool> _checkCustomerExist() async {
    final QuerySnapshot result = await Firestore.instance
        .collection('usersRegistration')
        .getDocuments();
    final List<DocumentSnapshot> documents = result.documents;
    if (documents.length > 0) {
      return
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Home()));
    } else {
      return
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    OTPScreen(mobileNumber: FullphoneNumber)));
    }

   /* try {
      DocumentSnapshot user = await getUser();
      String phone = user.data()["PhoneNumber"] ?? '';
      print(phone);
      if (phone == FullphoneNumber)
      {
        Navigator.push(context,  MaterialPageRoute(builder: (context) => Home()));

      } else{
        Navigator.push(
        context,
        MaterialPageRoute(
        builder: (context) => OTPScreen(mobileNumber:FullphoneNumber)));
        }

    } catch (e) {
      print(e);
    }*/
  }
}
