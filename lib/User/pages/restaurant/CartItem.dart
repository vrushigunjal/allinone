import 'package:all_in_one_app/User/pages/home/home_component/hot_sale.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter/material.dart';

class CartItem extends StatelessWidget {
  final dynamic product;
  final Function onDeleteTap;

  CartItem({@required this.product, @required this.onDeleteTap});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10.0,
      color: Colors.grey[200],
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(

              padding: EdgeInsets.all(8.0),
              child: FadeInImage(

                image: NetworkImage(product["productImage"]),
                placeholder: AssetImage("assets/placeholder.png"),
                height: 120.0,
                width: 120.0,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: 12,
            ),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    product["productName"],
                    softWrap: true,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    product["description"],
                    softWrap: true,
                    style: TextStyle(
                      fontSize: 12,
                      fontFamily: "Lato"
                    ),
                  ),
                  /*Text(
                    product["updated_at"]
                        .toDate()
                        .toString(),
                    softWrap: true,

                  ),*/

                  Row(
                    children: [
                      Text(
                        "${product['price'].toString()}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.green[600],
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),

                      if (product["salePrice"] != product["price"]) ...[
                        Text(
                          product["salePrice"].toString(),
                          softWrap: true,
                          style: TextStyle(
                            color: Colors.grey[700],
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                      ],
                    ],
                  ),
                  Row(
                    children: [
                  RaisedButton(

                    color: primaryColor,
                    onPressed: () async {
                      onDeleteTap();
                      },
                    child: Text("Delete"),
                  ),
                      SizedBox(
                        width: 5,
                      ),
                  RaisedButton(
                    padding: EdgeInsets.only(right:10.0),
                    color: primaryColor,
                    onPressed: () async {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => HotSale(),
                        ),
                      );
                    },
                    child: Text("Continue"),
                  ),
],
                  ),
                 /* IconButton(
                    onPressed: () {
                      onDeleteTap();
                    },
                    color: Colors.green,
                    icon: Icon(Icons.arrow_forward),
                  ),
                  IconButton(
                    onPressed: () {
                      onDeleteTap();
                    },
                    color: Colors.red,
                    icon: Icon(Icons.delete),
                  ),*/
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
