import 'package:all_in_one_app/User/pages/restaurant/products_tab_data_component/juice_list.dart';
import 'package:all_in_one_app/User/pages/restaurant/products_tab_data_component/popular_item_list.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter/material.dart';


class ServiceTabData extends StatefulWidget {
  @override
  _ServiceTabDataState createState() => _ServiceTabDataState();
}

class _ServiceTabDataState extends State<ServiceTabData> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: BouncingScrollPhysics(),
      children: <Widget>[
        // Popular Items Start
        Padding(
          padding: EdgeInsets.all(fixPadding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Services',
                style: headingStyle,
              ),
             /* InkWell(
                onTap: () {
                  // Navigator.push(context, PageTransition(type: PageTransitionType.downToUp, child: MoreList()));
                },
                child: Text('See all', style: moreStyle),
              ),*/
            ],
          ),
        ),
        PopularItemList(),
        // Popular Items End
        heightSpace,
        heightSpace,
        // Coffee Start
      /*  Container(
          color: whiteColor,
          child: Padding(
            padding: EdgeInsets.all(fixPadding),
            child: Text(
                  'Juice',
                  style: headingStyle,
                ),
          ),
        ),*/
      /*  Container(
          color: whiteColor,
          child: JuiceList(),
        ),*/

        // Coffee Start
        /*Container(
          color: whiteColor,
          child: Padding(
            padding: EdgeInsets.all(fixPadding),
            child: Text(
                  'Coffee',
                  style: headingStyle,
                ),
          ),
        ),*/
       /* Container(
          color: whiteColor,
          child: JuiceList(),
        ),*/
      ],
    );
  }
}
