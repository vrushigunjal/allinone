import 'package:all_in_one_app/User/pages/order/track_order_map.dart';
import 'package:all_in_one_app/User/pages/restaurant/review_tab_data/directionCenter.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class RestaurantInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return ListView(
      physics: BouncingScrollPhysics(),
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(fixPadding),
          child: Text(
            'Informations',
            style: headingStyle,
          ),
        ),
        Padding(
          padding: EdgeInsets.all(fixPadding),
          child: Text(
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
            style: listItemTitleStyle,
            textAlign: TextAlign.justify,
          ),
        ),
        Container(
          margin: EdgeInsets.all(fixPadding * 1.5),
          width: width - (fixPadding * 3.0),
          height: 180.0,
          decoration: BoxDecoration(
          ),

          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Direction',
                  style: headingStyle,
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(context,  MaterialPageRoute(builder: (context) => TrackOrderMap()));


                  },
                  child: Text('View all', style: moreStyle),
                ),
              ],
            ),
          ),
          // child: Image.asset('assets/restaurant_location.jpg', fit: BoxFit.cover),

      ],
    );
  }
}
