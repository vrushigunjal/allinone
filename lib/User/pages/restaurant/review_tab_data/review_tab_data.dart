import 'package:all_in_one_app/User/pages/restaurant/review_tab_data/rate.dart';
import 'package:all_in_one_app/User/pages/restaurant/review_tab_data/review.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter/material.dart';

import '../../../Rating.dart';


class ReviewTabData extends StatefulWidget {
  @override
  _ReviewTabDataState   createState() => _ReviewTabDataState();
}

class _ReviewTabDataState extends State<ReviewTabData> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return ListView(
      physics: BouncingScrollPhysics(),
      children: <Widget>[
        heightSpace,
        Rating(),
        heightSpace,
        ReviewSection(),
      ],
    );
  }
}