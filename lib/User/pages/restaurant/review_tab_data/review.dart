import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/User/widget/column_builder.dart';
import 'package:all_in_one_app/carWasher/helper/user.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ReviewSection extends StatefulWidget {
  @override
  _ReviewSectionState createState() => _ReviewSectionState();
}

class _ReviewSectionState extends State<ReviewSection> {
  /*final reviewList = [
    {
      'image': 'assets/user/user_profile/user_1.jpg',
      'name': 'Robert Junior',
      'rating': '5',
      'review': 'Very good.',
      'time': 'Yesterday 10:33 PM'
    },
    {
      'image': 'assets/user/user_profile/user_2.jpg',
      'name': 'Mark Lynn',
      'rating': '4',
      'review': 'Awesome Quality.',
      'time': 'Yesterday 9:12 PM'
    },
    {
      'image': 'assets/user/user_profile/user_3.jpg',
      'name': 'Ellison Perry',
      'rating': '3',
      'review': 'Super.',
      'time': 'Yesterday 7:35 PM'
    },
    {
      'image': 'assets/user/user_profile/user_4.jpg',
      'name': 'Emma Watson',
      'rating': '5',
      'review': 'Good.',
      'time': 'Yesterday 6:58 PM'
    },
    {
      'image': 'assets/user/user_profile/user_5.jpg',
      'name': 'Shira Maxwell',
      'rating': '2',
      'review': 'Mind Blowing.',
      'time': 'Yesterday 4:35 PM'
    },
    {
      'image': 'assets/user/user_profile/user_6.jpg',
      'name': 'David Smith',
      'rating': '4',
      'review': 'Fabulous.',
      'time': 'Yesterday 2:51 PM'
    },
    {
      'image': 'assets/user/user_profile/user_7.jpg',
      'name': 'Bill Hussey',
      'rating': '1',
      'review': 'It\'s Good.',
      'time': 'Yesterday 10:33 AM'
    },
    {
      'image': 'assets/user/user_profile/user_8.jpg',
      'name': 'Peter James',
      'rating': '5',
      'review': 'Very nice.',
      'time': 'Yesterday 9:33 AM'
    }
  ];*/

  bool _loading = true;
  bool _empty = true;
  List<DocumentSnapshot> orders;
  DocumentSnapshot lastDocument;
  ScrollController _scrollController;
  String id = "";
  @override
  void initState() {
    super.initState();
    getshared();


    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more orders");

        }
      });

  }

  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      id = (prefs.getString('id') ?? '');
      loadMyOrders();
    });
  }

  loadMyOrders() async {
    try {
      print(id);
      QuerySnapshot qSnap = await getRating(id);
      if (qSnap.size > 0) {
        setState(() {
          orders = qSnap.docs;
          _empty = false;
          _loading = false;
        });
      } else {
        setState(() {
          _empty = true;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _empty = true;
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    if (_loading == true) {
      return Container(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      );
    }
    if (_empty == true) {
      return Empty(text: "No Review!");
    }
    return ColumnBuilder(
      itemCount: orders.length,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      itemBuilder: (context, index) {
        final item = orders[index];
        final DateTime = item["updated_at"];
        return Container(
          width: width,
          height: 100.0,
          margin: EdgeInsets.all(fixPadding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 75.0,
                    height: 75.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      image: DecorationImage(
                        image: NetworkImage(item['image']),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  widthSpace,
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        item['Username'],
                        style: headingStyle,
                      ),
                      heightSpace,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.star, color: Colors.orange, size: 18.0),
                          Icon(Icons.star,
                              color: (item['rating'] == '1')
                                  ? Colors.grey[300]
                                  : Colors.orange,
                              size: 18.0),
                          Icon(Icons.star,
                              color: (item['rating'] == '1' ||
                                      item['rating'] == '2')
                                  ? Colors.grey[300]
                                  : Colors.orange,
                              size: 18.0),
                          Icon(Icons.star,
                              color: (item['rating'] == '1' ||
                                      item['rating'] == '2' ||
                                      item['rating'] == '3')
                                  ? Colors.grey[300]
                                  : Colors.orange,
                              size: 18.0),
                          Icon(Icons.star,
                              color: (item['rating'] == '1' ||
                                      item['rating'] == '2' ||
                                      item['rating'] == '3' ||
                                      item['rating'] == '4')
                                  ? Colors.grey[300]
                                  : Colors.orange,
                              size: 18.0),
                        ],
                      ),
                      heightSpace,
                      Text(
                        item['rating'],
                        style: listItemTitleStyle,
                      ),
                    ],
                  ),
                ],
              ),
              /*Text(
                DateTime,
            // item['updated_at'],
                style: listItemSubTitleStyle,
              ),*/
            ],
          ),
        );
      },
    );
  }
}
