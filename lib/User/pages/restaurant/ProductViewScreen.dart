import 'package:all_in_one_app/User/pages/restaurant/CartScreen.dart';
import 'package:all_in_one_app/User/pages/restaurant/cart.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:status_alert/status_alert.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:all_in_one_app/Admin/helpers/order.dart';

import 'package:shared_preferences/shared_preferences.dart';


class ProductViewScreen extends StatefulWidget {
  final DocumentSnapshot document;
  ProductViewScreen({@required this.document});

  @override

  _ProductViewScreenState createState() => _ProductViewScreenState();
}

class _ProductViewScreenState extends State<ProductViewScreen> {
  var initialItemCount = 1;
  double prize = 12112;
  double  itemPrice = 1212;
  String productid = "123123";

  void initState() {
    productid = widget.document.id;
    itemPrice = widget.document["salePrice"];
    prize = widget.document["salePrice"];
    super.initState();
  }
  incrementItem() {
    setState(() {
      initialItemCount = initialItemCount + 1;
      prize =itemPrice* initialItemCount;
    });
  }

  decrementItem() {
    if (initialItemCount > 1) {
      setState(() {
        initialItemCount = initialItemCount - 1;
      });
    }
  }

  _addProductToCartBtnTap(context) async {
    await addProductToCart(widget.document.id, widget.document.data(),prize);
    Navigator.push(context, MaterialPageRoute(builder: (
        context) => CartScreen()));
    StatusAlert.show(
      context,
      title: "Added to Cart",
      subtitle: "Go to cart for checkout",
      configuration: IconConfiguration(icon: Icons.done),
      duration: Duration(seconds: 1),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.document.data()["productName"]),
      ),
      body: Container(
        width: width,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Stack(
                children: [
                  Container(
                    child: FadeInImage(
                      image: NetworkImage(widget.document.data()["productImage"]),
                      placeholder: AssetImage("assets/placeholder.png"),
                      height: 300,
                      width: width,
                      fit: BoxFit.cover,
                    ),
                  ),
                  if (widget.document.data()["isFeatured"] == true) ...[
                    Positioned(
                      top: 10,
                      right: 10,
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: 5,
                          vertical: 0,
                        ),
                        decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: Shimmer.fromColors(
                          baseColor: Colors.white,
                          highlightColor: primaryColor,
                          child: Row(
                            children: [
                              Icon(
                                Icons.star,
                                color: Colors.white,
                              ),
                              Text("Stock"),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ],
              ),

              // product
              SizedBox(
                height: 20,
              ),

              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      widget.document.data()["productName"],
                      softWrap: true,
                      style: rsStyle,
                    ),
    
                    Text(
                      widget.document.data()["description"],
                      softWrap: true,
                      maxLines: 4,
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),

                    SizedBox(
                      width: 5,
                    ),
                    Row(
                      children: [
                        Text(
                          "${prize.toDouble()}",
                          softWrap: true,
                          style: priseStyle,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        if (prize !=
                            widget.document.data()["salePrice"]) ...[
                          Text(
                            widget.document.data()["price"].toString(),
                            softWrap: true,
                            style: dashpriseStyle
                          ),
                        ]
                      ],
                    ),


                    SizedBox(
                      height: 40,
                    ),
                    Row(
                      crossAxisAlignment:
                      CrossAxisAlignment.center,
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      children: <Widget>[
                        InkWell(
                          onTap:  decrementItem,
                          child: Container(
                            height: 26.0,
                            width: 26.0,
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.circular(
                                  13.0),
                              color: (initialItemCount == 1)
                                  ? Colors.grey[300]
                                  : primaryColor,
                            ),
                            child: Icon(
                              Icons.remove,
                              color: (initialItemCount == 1)
                                  ? blackColor
                                  : whiteColor,
                              size: 15.0,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              right: 8.0, left: 8.0),
                          child: Text('$initialItemCount'),
                        ),
                        InkWell(
                          onTap: incrementItem,
                          child: Container(
                            height: 26.0,
                            width: 26.0,
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.circular(
                                  13.0),
                              color: primaryColor,
                            ),
                            child: Icon(
                              Icons.add,
                              color: whiteColor,
                              size: 15.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RaisedButton(
                      color: primaryColor,
                      onPressed: () async {
                        _addProductToCartBtnTap(context);


                      },
                      child: Text("Add to cart"),
                    ),

                  ],
                ),
              ),

              SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
