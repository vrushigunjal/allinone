
import 'package:all_in_one_app/User/helper/OrderConfirm.dart';
import 'package:all_in_one_app/User/pages/confirm_order_add_address/confirm_order.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'AddCartProduct.dart';

void productDescriptionModalBottomSheet(context, height,docId) {


  String _adress = '';
  String _name = '';
  String _phone = '';
  String _productName = '';
  String _desription = '';
  String _image = '';
  int _price;



  int shipcharge = 10;
  double result = 0;
  int random = 0;
  String applycoupan = "0";
  String note = "";
  String order = "ordered";



  var initialItemCount = 1;

  double itemPrice = docId['price'];
  //var c = int.parse(itemPrice);
  var price = itemPrice * initialItemCount;

  double width = MediaQuery.of(context).size.width;
//  bool s = false, m = false, l = false, option1 = false, option2 = false;

    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext bc) {
        return StatefulBuilder(
          builder: (context, setState) {
            incrementItem() {
              setState(() {
                initialItemCount = initialItemCount + 1;
                price =itemPrice* initialItemCount;
              });
            }

            decrementItem() {
              if (initialItemCount > 1) {
                setState(() {
                  initialItemCount = initialItemCount - 1;
                  price = itemPrice * initialItemCount;
                });
              }
            }




            return Wrap(
              children: <Widget>[
                Container(
                  // height: height - 100.0,
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(16)),
                    color: whiteColor,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(fixPadding),
                        alignment: Alignment.center,
                        child: Container(
                          width: 35.0,
                          height: 3.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            color: greyColor,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(fixPadding),
                        child: Text(
                          'Add New Item',
                          style: headingStyle,
                        ),


                      ),
                      Container(
                        width: width,
                        height: 70.0,
                        margin: EdgeInsets.all(fixPadding),
                        decoration: BoxDecoration(
                          color: whiteColor,
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 70.0,
                              width: 70.0,
                              alignment: Alignment.topRight,
                              padding: EdgeInsets.all(fixPadding),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                image: DecorationImage(
                                  image: NetworkImage(
                                     docId["productImage"]),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Container(
                              width: width - ((fixPadding * 2) + 70.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(
                                        right: fixPadding * 2,
                                        left: fixPadding,
                                        bottom: fixPadding),
                                    child: Text(
                                      docId["productName"],
                                      style: listItemTitleStyle,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),

                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: fixPadding,
                                        right: fixPadding,
                                        left: fixPadding),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(

                                          '\Rs${docId['price']}',
                                         // '\Rs${item['price']}',
                                          style: priceStyle,
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            InkWell(
                                              onTap: decrementItem,
                                              child: Container(
                                                height: 26.0,
                                                width: 26.0,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          13.0),
                                                  color: (initialItemCount == 1)
                                                      ? Colors.grey[300]
                                                      : primaryColor,
                                                ),
                                                child: Icon(
                                                  Icons.remove,
                                                  color: (initialItemCount == 1)
                                                      ? blackColor
                                                      : whiteColor,
                                                  size: 15.0,
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  right: 8.0, left: 8.0),
                                              child: Text('$initialItemCount'),
                                            ),
                                            InkWell(
                                              onTap: incrementItem,
                                              child: Container(
                                                height: 26.0,
                                                width: 26.0,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          13.0),
                                                  color: primaryColor,
                                                ),
                                                child: Icon(
                                                  Icons.add,
                                                  color: whiteColor,
                                                  size: 15.0,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      heightSpace,
                      // Size Start
                      Container(
                        color: scaffoldBgColor,
                        padding: EdgeInsets.all(fixPadding),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Discription',
                              style: listItemSubTitleStyle,
                            ),
                            /*Text(
                              'Price',
                              style: listItemSubTitleStyle,
                            ),*/
                          ],
                        ),
                      ),
                      Container(
                        color: whiteColor,
                        padding: EdgeInsets.all(fixPadding),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            heightSpace,
                            Text(

                              docId['description'],
                              // '\Rs${item['price']}',
                              style: priceStyle,
                            ),
                            heightSpace,
                            /*heightSpace,
                            getSizeListItem('S', '500', '0'),
                            heightSpace,
                            getSizeListItem('M', '750', '0.5'),
                            heightSpace,
                            getSizeListItem('L', '1100', '1.2'),
                            heightSpace,*/
                          ],
                        ),
                      ),
                      // Size End
                      // Options Start
                      Container(
                        width: width,
                        color: scaffoldBgColor,
                        padding: EdgeInsets.all(fixPadding),
                        child: Text(
                          'Featured product',
                          style: listItemSubTitleStyle,
                        ),
                      ),
                      Container(
                        color: whiteColor,
                        padding: EdgeInsets.all(fixPadding),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            heightSpace,
                            Text(

                              docId['isFeatured'],
                              // '\Rs${item['price']}',
                              style: priceStyle,
                            ),
                            heightSpace,
                          ],
                        ),
                      ),
                      //Options End

                      // Add To Cart
                      Padding(
                        padding: EdgeInsets.all(fixPadding),
                        child: InkWell(
                          onTap: () async {
                            var order = "Cart";
                              final prefs = await SharedPreferences.getInstance();

                              _adress = (prefs.getString('CURRENTADRESS') ?? '');
                              _name = (prefs.getString('FULLNAME') ?? '');
                              _phone = (prefs.getString('PHONENUMBER') ?? '');
                              _productName = (prefs.getString('Productname') ?? '');
                              _desription = (prefs.getString('ProductDiscription') ?? '');
                              _image = (prefs.getString('productImage') ?? '');

                              addOrder(
                                _phone,
                                _image,
                                _name,
                                _adress,
                                _productName,
                                _desription,
                                price,
                                shipcharge,
                                result,
                                applycoupan,
                                note,
                                order
                            );
                                //Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: Cart()));
                            Navigator.push(context,  MaterialPageRoute(builder: (context) => Cart()));
                          },

                          child: Container(
                            width: width - (fixPadding * 2),
                            padding: EdgeInsets.all(fixPadding),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              color: primaryColor,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      '$initialItemCount ITEM',
                                      style: TextStyle(
                                        color: darkPrimaryColor,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.w500,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                    SizedBox(height: 3.0),
                                    Text(
                                      '\Rs$price',
                                      style: whiteSubHeadingStyle,
                                    ),
                                  ],
                                ),

                                Text(
                                  'Add To Cart',
                                  style: wbuttonWhiteTextStyle,
                                ),


                              ],
                            ),
                          ),
                        ),
                      ),
                      // Buy Now
                      Padding(
                        padding: EdgeInsets.all(fixPadding),
                        child: InkWell(

                          onTap: () async {
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            prefs.setString('productImage',  docId["productImage"],);
                            prefs.setString('Productname',  docId["productName"],);
                            prefs.setString('ProductDiscription',  docId["description"],);
                            prefs.setDouble('Productprice', price,);
                            prefs.setString('uid', docId);

                            Navigator.push(context,  MaterialPageRoute(builder: (context) => ConfirmOrder()));
                          },

                          child: Container(
                            width: width - (fixPadding * 2),
                            padding: EdgeInsets.all(fixPadding),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              color: primaryColor,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      '$initialItemCount ITEM',
                                      style: TextStyle(
                                        color: darkPrimaryColor,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.w500,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                    SizedBox(height: 3.0),
                                    Text(
                                      '\Rs$price',
                                      style: whiteSubHeadingStyle,
                                    ),
                                  ],
                                ),

                                Text(
                                  'Buy Now',
                                  style: wbuttonWhiteTextStyle,
                                ),


                              ],
                            ),
                          ),
                        ),
                      ),
                      // Add to Cart button row end here
                    ],
                  ),
                ),
              ],
            );
          },
        );
      });
}
