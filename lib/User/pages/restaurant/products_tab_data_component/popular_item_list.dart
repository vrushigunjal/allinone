import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/helpers/category.dart';
import 'package:all_in_one_app/User/pages/restaurant/products_tab_data_component/add_to_cart_bottomsheet.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'add_to_cart_service.dart';

class PopularItemList extends StatefulWidget {
  @override
  _PopularItemListState createState() => _PopularItemListState();
}

class _PopularItemListState extends State<PopularItemList> {


/*  Future getimage() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection("categories").getDocuments();
    return qn.documents;
  }*/
  bool _loading = true;
  bool _empty = true;
  List<DocumentSnapshot> orders;
  DocumentSnapshot lastDocument;
  ScrollController _scrollController;
  String id = "";

  @override
  void initState() {
    super.initState();
    getshared();


    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more orders");

        }
      });

  }

  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
    id = (prefs.getString('id') ?? '');
    loadMyOrders();
    });
  }

  loadMyOrders() async {
    try {
      print(id);
      QuerySnapshot qSnap = await getWashCategories(id);
      if (qSnap.size > 0) {
        setState(() {
          orders = qSnap.docs;
          _empty = false;
          _loading = false;
        });
      } else {
        setState(() {
          _empty = true;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _empty = true;
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    if (_loading == true) {
      return Container(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      );
    }
    if (_empty == true) {
      return Empty(text: "No orders!");
    }
    return Container(
        width: width,
        height: height,
        padding: EdgeInsets.only(top: 10.0, bottom: 20.0),
        child: ListView.builder(
              itemCount: orders.length,
              physics: BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                final item = orders[index];
                return Container(
                  padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                  width: 135.0,
                  height: 120.0,
                  margin: (index != (orders.length - 1))
                      ? EdgeInsets.only(left: fixPadding)
                      : EdgeInsets.only(
                          left: fixPadding,
                          right: fixPadding,
                          bottom: fixPadding),
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
               child: InkWell(
                onTap: () async {

                var docId = orders[index];
                seviceDescriptionModalBottomSheet(context, height,docId);
                },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 100.0,
                        width: 135.0,
                        alignment: Alignment.topRight,
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(5.0)),
                          image: DecorationImage(
                            image: NetworkImage(item['serviceImage']),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Container(
                        width: 135.0,
                        height: 80.0,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Text(
                                item['title'],
                                style: listItemTitleStyle,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 5.0, right: 5.0),
                              child: Text(
                                item['title'],
                                style: listItemSubTitleStyle,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  top: 5.0, right: 5.0, left: 5.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    '\Rs${item['Total']}',
                                    style: priceStyle,
                                  ),
                                  InkWell(
                                    onTap: () async {
                                      var docId = orders[index];
                                      seviceDescriptionModalBottomSheet(context, height,docId);
                                    },
                                    child: Container(
                                      height: 20.0,
                                      width: 20.0,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        color: primaryColor,
                                      ),
                                      child: Icon(
                                        Icons.add,
                                        color: whiteColor,
                                        size: 15.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
               ),
                );
              },
            ),);


  }
}
