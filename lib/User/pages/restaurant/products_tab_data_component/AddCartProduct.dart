import 'package:all_in_one_app/Admin/helpers/order.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class Cart extends StatefulWidget{
  static final String route = "Cart-route";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return CartState();
  }
}

class CartState extends State<Cart>{
  String id = "123";
  Future getimage() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection("orders") .where("order", isEqualTo: "Cart")
        .get();
    qn.documents.forEach((qn){
       id = qn.documentID;
    });
    return qn.documents;
  }@override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(

        appBar:AppBar(
          elevation: 0.0,
          title: Text("Cart List"),
        ),
        backgroundColor: Colors.white,
        body:Container(
          decoration: BoxDecoration(
              border: Border(
                  top: BorderSide(
                      color: Colors.grey[300],
                      width: 1.0
                  )
              )
          ),
          child: FutureBuilder(
            future: getimage(),
            builder: (_, snapshot) {
              return ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    final item = snapshot.data[index];
                    return Container(
                      padding: EdgeInsets.all(5.0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white12,
                            border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey[100],
                                  width: 1.0
                              ),
                              top: BorderSide(
                                  color: Colors.grey[100],
                                  width: 1.0
                              ),
                            )
                        ),
                        height: 100.0,
                        child: Row(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              height: 100.0,
                              width: 100.0,
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black12,
                                        blurRadius: 5.0
                                    )
                                  ],
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(10.0),
                                      bottomRight: Radius.circular(10.0)
                                  ),
                                  image: DecorationImage(image: NetworkImage(item['Image']),fit: BoxFit.fill)
                              ),
                            ),
                            Expanded(
                                child: Padding(
                                  padding: EdgeInsets.only(top: 10.0,left: 15.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Text(item["Name"],style: TextStyle(fontWeight: FontWeight.w600,fontSize: 15.0),),
                                          ),
                                          Container(
                                              alignment: Alignment.bottomRight,
                                              child: InkResponse(
                                                      /*onTap: (){
                                                        item.removeCart();
                                                      },*/
                                                      child: Padding(
                                                        padding: EdgeInsets.only(right: 10.0),
                                                        child: Icon(Icons.confirmation_number,color: Colors.red,),
                                                      )
                                              ),),

                                          Container(
                                            alignment: Alignment.bottomRight,
                                            child: InkResponse(
                                              onTap: (){
                                                print(id);
                                                deleteCart(id);

                                                  },
                                                child: Padding(
                                                  padding: EdgeInsets.only(right: 10.0),
                                                  child: Icon(Icons.delete,color: Colors.red,),
                                                )
                                            ),),

                                        ],
                                      ),

                                      SizedBox(height: 5.0,),
                                      Text("Price ${item["Price"]}"),

                                    ],
                                  ),
                                )
                            )
                          ],
                        ),
                      ),
                    );
                  });
            },
          ),
        )
    );
  }
}
