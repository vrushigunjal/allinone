import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/constant/constant.dart';

class ProductCard extends StatelessWidget {
  final QueryDocumentSnapshot product;
  final Function onTap;

  ProductCard({@required this.product, @required this.onTap});

  int index = 0;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(top: 10.0),
      child: InkWell(
        onTap: onTap,
        child: Card(
          color: Colors.grey[200],
          elevation: 10.0,
          child: Container(
            margin: EdgeInsets.all(4),
            padding: EdgeInsets.all(4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(16),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FadeInImage(
                  image: NetworkImage(product.data()["productImage"]),
                  placeholder: AssetImage("assets/placeholder.png"),
                  width: 120,
                  height: 100,
                  fit: BoxFit.cover,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: Text(
                    product.data()["productName"],
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("${product.data()['salePrice'].toString()}",
                          softWrap: true, style: priseStyle),
                      SizedBox(
                        width: 5,
                      ),
                      if (product.data()["price"] !=
                          product.data()["salePrice"]) ...[
                        Text(
                          product.data()["price"].toString(),
                          softWrap: true,
                          style: TextStyle(
                            color: Colors.grey[700],
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                      ],
                    ],
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
