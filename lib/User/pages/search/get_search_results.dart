
import 'dart:io';

import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/components/MyDialog.dart';
import 'package:all_in_one_app/Admin/helpers/auth.dart';
import 'package:all_in_one_app/User/helper/favourite.dart';
import 'package:all_in_one_app/User/pages/restaurant/restaurant.dart';
import 'package:all_in_one_app/carWasher/helper/user.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path/path.dart' as path;

class GetSearchResults extends StatefulWidget {


 /* final DocumentSnapshot documentcar;

  GetSearchResults({@required this.documentcar});*/
  @override
  _GetSearchResultsState createState() => _GetSearchResultsState();
}
enum SearchBy { email, phone, uid }
int index;
class _GetSearchResultsState extends State<GetSearchResults> {
  FirebaseAuth auth = FirebaseAuth.instance;
  File _productImage;
  String updatestatus = 'saved';
  bool _loading = true;
  bool _empty = true;
  List<DocumentSnapshot> orders;
  DocumentSnapshot lastDocument;
  ScrollController _scrollController;
  @override
  void initState() {
    super.initState();
    getshared();
    loadMyOrders();
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more orders");

        }
      });
  }
  loadMyOrders() async {
    try {
      QuerySnapshot qSnap = await getUserData();
      if (qSnap.size > 0) {
        setState(() {
          orders = qSnap.docs;
          _empty = false;
          _loading = false;
        });
      } else {
        setState(() {
          _empty = true;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _empty = true;
        _loading = false;
      });
    }
  }

/*
  Future getimage() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection("WasherRegistration").getDocuments();
    return qn.documents;
  }
*/

  QueryDocumentSnapshot snapshot;
  List<QueryDocumentSnapshot> document;
  List<QueryDocumentSnapshot> products;


  /* bool status = true;*/
  String phone = '123456789';



  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      phone = (prefs.getString('PHONENUMBER') ?? '');

    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (_loading == true) {
      return Container(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      );
    }
    if (_empty == true) {
      return Empty(text: "No orders!");
    }
      return ListView.builder(
        itemCount: orders.length,
        physics: BouncingScrollPhysics(),
        itemBuilder: (context, index) {
          final item = orders[index];

          return InkWell(
            onTap: () {

              Navigator.push(context,  MaterialPageRoute(builder: (context) => Restaurant( doc: orders[index],)));


            },
            child: Container(
              width: width,
              height: 100.0,
              margin: (index == 0)
                  ? EdgeInsets.all(fixPadding)
                  : EdgeInsets.only(
                  right: fixPadding, left: fixPadding, bottom: fixPadding),
              decoration: BoxDecoration(
                color: whiteColor,
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: Stack(
                children: <Widget>[
                  /*  Positioned(
                    top: fixPadding,
                    right: fixPadding,
             child: InkWell(
             onTap: () {
                 if (item['status'] == 'none') {
                   String key;
                   editstatus( item.id,updatestatus,item["Centername"],item["adreess"]);
                   setState(() {
                     addfavouriteList(item.id,updatestatus,item["userImage"],item["Centername"],item["adreess"],phone);
                     item['status'] = item["status"];
                   });
                   Scaffold.of(context).showSnackBar(SnackBar(
                     content: Text('Added to Favourite'),
                   ));
                 } else {
                   setState(() {
                     item['status'] = 'none';
                   });
                   Scaffold.of(context).showSnackBar(SnackBar(
                     content: Text('Remove from Favourite'),
                   ));
                 }
               },
               child: Icon(
                 (item['status'] == 'none')
                     ? Icons.bookmark_border
                     : Icons.bookmark,
                 size: 22.0,
                 color: greyColor,
               ),
             ),
           ),*/
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 100.0,
                        width: 90.0,
                        alignment: Alignment.topRight,
                        padding: EdgeInsets.all(fixPadding),
                        decoration: BoxDecoration(
                          borderRadius:
                          BorderRadius.horizontal(left: Radius.circular(5.0)),
                          image: DecorationImage(
                            image: NetworkImage(item['userImage']),
                            fit: BoxFit.cover,
                          ),

                        ),

                      ),
                      Container(
                        width: width - ((fixPadding * 2) + 100.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(fixPadding),
                              child: Text(
                                item['Centername'],
                                style: listItemTitleStyle,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),

                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: fixPadding, right: 5.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Icon(
                                    Icons.location_on,
                                    color: primaryColor,
                                    size: 18.0,
                                  ),
                                  Flexible(
                                  child:new Text(
                                    item['adreess'],
                                    style: listItemSubTitleStyle,
                                    maxLines: 4,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(fixPadding),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                    Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.star,
                                        color: primaryColor, size: 18.0),
                                    SizedBox(width: 2.0),
                                    Text(
                                      item['rating'],
                                      style: listItemSubTitleStyle,
                                    ),
                                  ],
                                ),
                                /*Text(
                                  '${item['distance']} km',
                                  style: listItemSubTitleStyle,
                                ),*/
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      );

  }


}
