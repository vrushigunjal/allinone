import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/helpers/offers.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';
import 'package:flutter_carousel_slider/carousel_slider_indicators.dart';
import 'package:flutter_carousel_slider/carousel_slider_transforms.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ImageSliderList extends StatefulWidget {
  @override
  _ImageSliderListState createState() => _ImageSliderListState();
}

class _ImageSliderListState extends State<ImageSliderList> {

/*  Future getimage() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection("Offers").getDocuments();
    return qn.documents;
  }*/

  bool _loading = true;
  bool _empty = true;
  List<DocumentSnapshot> orders;
  DocumentSnapshot lastDocument;

  ScrollController _scrollController;
  @override
  void initState() {
    super.initState();
    loadMyOrders();
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more orders");

        }
      });
  }


  loadMyOrders() async {
    try {
      QuerySnapshot qSnap = await getOffers();
      if (qSnap.size > 0) {
        setState(() {
          orders = qSnap.docs;
          _empty = false;
          _loading = false;
        });
      } else {
        setState(() {
          _empty = true;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _empty = true;
        _loading = false;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    if (_loading == true) {
      return Container(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      );
    }
    if (_empty == true) {
      return Empty(text: "Oops! No Order Found!");
    }
    return Container(
      width: width,
      height: 160.0,

            child:CarouselSlider.builder(
                    slideBuilder: (index) {
                      DocumentSnapshot sliderimage = orders[index];
                      return new Stack(
                        children: <Widget>[
                       Container(
                         constraints: new BoxConstraints.expand(
                             height: 200.0,
                             width: width,
                         ),
                         padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                         decoration: new BoxDecoration(
                           image: new DecorationImage(
                             image: new NetworkImage(sliderimage["OfferImage"]),
                             fit: BoxFit.cover,
                           ),
                         ),
                      child: new Stack(

                          children: <Widget>[
                            new Positioned(
                              left: 0.0,
                              top: 5.0,
                              child: new Text("WashOnClick",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15.0,
                                      color: Colors.white
                                  )
                              ),
                            ),
                            new Positioned(
                              left: 0.0,
                              top: 40.0,
                              child: new Text(sliderimage["OfferName"],
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 30.0,
                                      color: Colors.white
                                  )
                              ),
                            ),
















































                            new Positioned(
                              left: 0.0,
                              top: 80.0,
                              child: new Text("On tyre dressing and \n polishing of your car",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15.0,
                                      color: Colors.white
                                  )
                              ),
                            ),
                            new Positioned(
                              right: 0.0,
                              top: 0.0,
                              child: Image(
                                image: AssetImage("assets/admincoupons.png"),
                                height: 150.0,
                                width: 40.0,
                              ),

                            ),
                          ],
                      ),
                            ),
                          //borderRadius: BorderRadius.circular(10.0),
                        ],

//sliderimage
                          );

                    },
                    slideTransform: CubeTransform(
                      rotationAngle: 0,
                    ),
                    slideIndicator: CircularSlideIndicator(
                      indicatorBackgroundColor: Colors.red,
                      currentIndicatorColor: Colors.green,
                    ),
                    itemCount: orders.length,
                  ),);


  }
}
