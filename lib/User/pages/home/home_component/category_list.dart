import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/helpers/category.dart';
import 'package:all_in_one_app/User/pages/restaurant/products_tab_data_component/popular_item_list.dart';
import 'package:all_in_one_app/User/pages/restaurant/restaurant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:all_in_one_app/carWasher/helper/user.dart';

class CategoryList extends StatefulWidget {
  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
 /* Future getimage() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection("categories").getDocuments();
    return qn.documents;
  }*/
  bool _loading = true;
  bool _empty = true;

  List<DocumentSnapshot> orders;
  DocumentSnapshot lastDocument;
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    loadMyOrders();

    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more orders");

        }
      });
  }
  loadMyOrders() async {
    try {
      QuerySnapshot qSnap = await getUserData();
      if (qSnap.size > 0) {
        setState(() {
          orders = qSnap.docs;
          _empty = false;
          _loading = false;
        });
      } else {
        setState(() {
          _empty = true;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _empty = true;
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    if (_loading == true) {
      return Container(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      );
    }
    if (_empty == true) {
      return Empty(text: "No orders!");
    }
    return Container(
      width: 180.0,
      height:  170.0,


          child:  ListView.builder(
        itemCount: orders.length,
            scrollDirection: Axis.horizontal,
            physics: BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final   item = orders[index] ;
               var Uid = orders[index]["uid"];
              _empty = true;
              _loading = true;
              return InkWell(
                onTap: () {

                  Navigator.push(context, MaterialPageRoute(builder: (context)=>Restaurant(
                    doc: orders[index],

                  )));
                },
                child: Container(
                  width: 140.0,
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  margin: (index == 0)
                      ? EdgeInsets.only(left: fixPadding)
                      : EdgeInsets.only(left: fixPadding, right: fixPadding),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 100.0,
                        width: 100.0,
                        alignment: Alignment.topRight,
                          padding: EdgeInsets.all(fixPadding),
                        decoration: BoxDecoration(
                          borderRadius:
                          BorderRadius.vertical(top: Radius.circular(5.0)),
                          image: DecorationImage(
                            image: NetworkImage(item['userImage']),
                            fit: BoxFit.cover,
                          ),
                        ),

                      ),
                      heightSpace,
                      Text(
                        item['Centername'],
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: listItemTitleStyle,
                      )
                    ],
                  ),
                ),
              );
            },

          ),);


  }
}
