


import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/config/colors.dart';
import 'package:all_in_one_app/Admin/helpers/product.dart';
import 'package:all_in_one_app/User/pages/restaurant/CartScreen.dart';
import 'package:all_in_one_app/User/pages/restaurant/ProductViewScreen.dart';
import 'package:all_in_one_app/User/pages/restaurant/products_tab_data_component/AddCartProduct.dart';
import 'package:all_in_one_app/User/pages/restaurant/products_tab_data_component/UserProductCard.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class HotSale extends StatefulWidget {

  @override
  _HotSaleState createState() => _HotSaleState();
}

class _HotSaleState extends State<HotSale> {
  bool _isLoading = false;
  bool _isEmpty = false;
  String cartcount = "";
  List<QueryDocumentSnapshot> products = List();
  QueryDocumentSnapshot lastDocument;

  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    loadProducts();
    _scrollController
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more uploads");
          loadMoreProducts();
        }
      });
  }

  loadProducts() async {
    setState(() {
      _isLoading = true;
    });

    QuerySnapshot qsnap = await getProducts();
    if (qsnap.size > 0) {
      if (this.mounted) {
        setState(() {
          _isLoading = false;
          _isEmpty = false;

          products = qsnap.docs;
          lastDocument = qsnap.docs.last;
        });
      }
    } else {
      setState(() {
        _isLoading = false;
        _isEmpty = true;
      });
    }
  }

  loadMoreProducts() async {
    QuerySnapshot qsnap = await getMoreProducts(lastDocument);
    if (qsnap.size > 0) {
      setState(() {
        products.addAll(qsnap.docs);
        lastDocument = qsnap.docs.last;
        _isLoading = false;
        _isEmpty = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoading == true) {
      return Container(
        child: Center(
          child: SpinKitChasingDots(
            color: primaryColor,
            size: 50,
          ),
        ),
      );
    } else {
      if (_isEmpty == true) {
        return Container(
          child: Empty(text: "No products found!"),
        );
      } else {
        return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text("ProductList"),
             /* actions: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.shopping_cart,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (
                        context) => CartScreen()));
                  },
                )
              ],*/
          ),
          body: RefreshIndicator(
              onRefresh: () async {
                return loadProducts();

              },
              child: GridView.count(
                  crossAxisCount: 2 ,
                  controller: _scrollController,
                  children: List.generate(products.length,(i)
                  {
                    {
                   return ProductCard(
                     product: products[i],
                     onTap: () {
                       Navigator.push(
                         context,
                         MaterialPageRoute(
                           builder: (context) =>
                               ProductViewScreen(
                                 document: products[i],
                               ),
                         ),
                       );
                     },
                   );
                 }
               }),

    ),),);
      }
    }
  }
}
