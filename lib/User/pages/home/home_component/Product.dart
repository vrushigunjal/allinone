import 'package:all_in_one_app/Admin/components/Empty.dart';
import 'package:all_in_one_app/Admin/helpers/offers.dart';
import 'package:all_in_one_app/Admin/helpers/product.dart';
import 'package:all_in_one_app/User/pages/restaurant/products_tab_data_component/add_to_cart_bottomsheet.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class product extends StatefulWidget {
  @override
  _productState createState() => _productState();
}

class _productState extends State<product> {
/*
  var id = "123456";
  Future getimage() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection("products").getDocuments();
    qn.documents.forEach((qn){
      id = qn.documentID;
    });
    return qn.documents;
  }*/
  bool _loading = true;
  bool _empty = true;
  List<DocumentSnapshot> orders;
  DocumentSnapshot lastDocument;

  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    loadMyOrders();
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          print("loading more orders");
        }
      });
  }

  loadMyOrders() async {
    try {
      QuerySnapshot qSnap = await getProducts();
      if (qSnap.size > 0) {
        setState(() {
          orders = qSnap.docs;
          _empty = false;
          _loading = false;
        });
      } else {
        setState(() {
          _empty = true;
          _loading = false;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _empty = true;
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    if (_loading == true) {
      return Container(
        child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        ),
      );
    }
    if (_empty == true) {
      return Empty(text: "No orders!");
    }

    return Container(
      width: 180.0,

      child: GridView.count(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 2,
        padding: EdgeInsets.only(top: 8, left: 6, right: 6, bottom: 12),
        children: List.generate(
          4,
          (index) {
            return Card(
             elevation: 10.0,
              margin: (index == 0)
                  ? EdgeInsets.only(bottom: fixPadding,left: fixPadding,top: fixPadding)
                  : EdgeInsets.only(bottom: fixPadding, left: fixPadding,right:fixPadding,top: fixPadding),

              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top:20.0),
                    child: FadeInImage(
                      image: NetworkImage(orders[index]["productImage"]),
                      placeholder: AssetImage("assets/placeholder.png"),
                      height: 80.0,
                      width: 100.0,
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    width: 14,
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          orders[index]["productName"],
                          softWrap: true,
                          style: rsStyle,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("${orders[index]['salePrice'].toString()}",
                                softWrap: true, style: priseStyle),
                            SizedBox(
                              width: 5,
                            ),
                            if (orders[index]["price"] !=
                                orders[index]["salePrice"]) ...[
                              Text(
                                orders[index]["price"].toString(),
                                softWrap: true,
                                style: TextStyle(
                                  color: Colors.grey[700],
                                  decoration: TextDecoration.lineThrough,
                                ),
                              ),
                            ]
                          ],
                        ),
                        /* Text(
                        product.data()["category"],
                        softWrap: true,
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),*/
                      ],
                    ),
                  ),
                  /* height: 90.0,
                        width: width,
                        alignment: Alignment.topRight,
                        padding: EdgeInsets.all(fixPadding),
                        decoration: BoxDecoration(
                          borderRadius:
                          BorderRadius.vertical(top: Radius.circular(5.0)),
                          image: DecorationImage(
                     image: NetworkImage(orders[index]['productImage']),
                            fit: BoxFit.cover,
                          ),
                        ),

                      ),*/
                ],
              ),
            );
          },
        ),
      ),
    );
    /*Container(
      width: width,
      height:  200.0,
      child: FutureBuilder(
        future: getimage(),
        builder: (_, snapshot) {
          return  ListView.builder(
            itemCount:  snapshot.data.length,
            scrollDirection: Axis.horizontal,
            physics: BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              final item =  snapshot.data[index];

              return InkWell(
                onTap: () async {
                  */ /*String docId = snapshot.data[index];
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                  prefs.setString('DocId', docId);*/ /*
                },
                child: Container(
                  width: 130.0,
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  margin: (index != ( snapshot.data.length -1))
                      ? EdgeInsets.only(left: fixPadding)
                      : EdgeInsets.only(left: fixPadding, right: fixPadding),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 110.0,
                        width: 130.0,
                        alignment: Alignment.topRight,
                        padding: EdgeInsets.all(fixPadding),
                        decoration: BoxDecoration(
                          borderRadius:
                          BorderRadius.vertical(top: Radius.circular(5.0)),
                          image: DecorationImage(
                            image: NetworkImage(item['productImage']),
                            fit: BoxFit.cover,
                          ),
                        ),
                        child: InkWell(
                          onTap: () {
                            var docId = snapshot.data[index];
                            productDescriptionModalBottomSheet(context, height,docId);
                          },
                        ),
                      ),
                      Container(
                        width: 130.0,
                        height: 80.0,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Text(
                                item['productName'],
                                style: listItemTitleStyle,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            Padding(
                              padding:
                              EdgeInsets.only(top: 5.0, right: 5.0, left: 5.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    '\Rs${item['price']}',
                                    style: priceStyle,
                                  ),
                                  InkWell(
                                    onTap: () async {
                                      var docId = snapshot.data[index];
                                      productDescriptionModalBottomSheet(context, height,docId);
                                    },
                                    child: Container(
                                      height: 20.0,
                                      width: 20.0,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10.0),
                                        color: primaryColor,
                                      ),
                                      child: Icon(
                                        Icons.add,
                                        color: whiteColor,
                                        size: 15.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        },
      ),
    );*/
  }
}
