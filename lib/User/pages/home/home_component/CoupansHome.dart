
import 'package:all_in_one_app/User/pages/profile/Voucher.dart';
import 'package:flutter/material.dart';


class Rewards extends StatefulWidget {
  @override
  _RewardsState createState() => _RewardsState();
}

class _RewardsState extends State<Rewards> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return new InkWell(
      onTap: (){
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => Voucher()));
      },
     child: Stack(
      children: <Widget>[
        Container(
      constraints: new BoxConstraints.expand(
        height: 200.0,
        width: width,
      ),
        padding: EdgeInsets.only(
            top: 6.0, left: 8.0, right: 8.0, bottom: 10),
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage('assets/refferalcode.png'),
            fit: BoxFit.cover,
          ),
        ),
    child: new Stack(
        children: <Widget>[
          new Positioned(
            left: 10.0,
            top: 2.0,
            child: new Text("WashonClick",
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0,
                    color: Colors.black
                )
            ),
          ),
        /*  new Positioned(
            left: 30.0,
            top: 25.0,
            child: new Text("Total Count",
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0,
                    color: Colors.green
                )
            ),
          ),
          new Positioned(
            left: 40.0,
            top: 45.0,
            child: new Text("639",
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0,
                    color: Colors.red
                )
            ),
          ),*/
          new Positioned(
            left: 0.0,
            top:30.0,
            child: new Text("Get a Special \n referral reward!",
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0,
                    color: Colors.green
                )
            ),
          ),

          new Positioned(
            left: 0.0,
            bottom: 70.0,
            child: new Text("Share and invite friends to join\n washonclick now!",
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                    color: Colors.grey
                )
            ),
          ),
        ],
    ),
      ),

    ],
    ),
    );
  }
}