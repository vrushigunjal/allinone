import 'package:all_in_one_app/Agent/login_signup/login.dart';
import 'package:all_in_one_app/User/pages/confirm_order_add_address/add_new_address.dart';
import 'package:all_in_one_app/User/pages/home/home_component/category_list.dart';
import 'package:all_in_one_app/User/pages/home/home_component/favourite_restaurants_list.dart';
import 'package:all_in_one_app/User/pages/home/home_component/hot_sale.dart';
import 'package:all_in_one_app/User/pages/home/home_component/image_slider_list.dart';
import 'package:all_in_one_app/User/pages/home/home_component/product_ordered.dart';
import 'package:all_in_one_app/User/pages/login_signup/ContinueWithPhone.dart';
import 'package:all_in_one_app/User/pages/login_signup/NewRegister.dart';
import 'package:all_in_one_app/User/pages/login_signup/register.dart';
import 'package:all_in_one_app/User/pages/notification.dart';
import 'package:all_in_one_app/User/pages/profile/Voucher.dart';
import 'package:all_in_one_app/User/pages/restaurant/CartScreen.dart';

import 'package:all_in_one_app/constant/constant.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home_component/CoupansHome.dart';
import 'home_component/Product.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:all_in_one_app/Admin/components/Empty.dart';




class HomeMain extends StatefulWidget {
  @override
  _HomeMainState createState() => _HomeMainState();
}

class _HomeMainState extends State<HomeMain> {

  bool address1 = true;
  bool address2 = false;
  bool _loading = true;
  bool _empty = true;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  String currentAddress;
  int cartCount = 0;
  String total = "0";

  @override
  void initState() {
    getshared();
    super.initState();
    _getCurrentLocation();

  }
  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      cartCount = (prefs.getInt('Count') ??  '');
      print(cartCount);
    });
  }
  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }




  @override
  Widget build(BuildContext context) {


    double width = MediaQuery
        .of(context)
        .size
        .width;
    double height = MediaQuery
        .of(context)
        .size
        .height;


    return Container(
      color: primaryColor,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  expandedHeight: 60,
                  pinned: true,
                  floating: true,
                  actions: <Widget>[
                    IconButton(
                      icon: Icon(Icons.notifications),
                      color: Colors.white,
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (
                            context) => Notifications()));
                      },
                      //Notifications
                    ),
                    Stack(
                  children: <Widget>[
                     IconButton(
                      icon: Icon(Icons.shopping_cart),
                      color: Colors.white,
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (
                            context) => CartScreen()));
                      },

                    ),
                    new Positioned(
                      right: 6.0,
                      top: 3.0,
                      child: new Text('$cartCount',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15.0,
                              color: Colors.green
                          )
                      ),
                    ),
                  ],
                  ),
                  ],
                  title: InkWell(
                    onTap: () {
                      //_addressBottomSheet(context, width);
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Delivering To'.toUpperCase(),
                            style: appbarHeadingStyle),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.location_on,
                              color: darkPrimaryColor,
                              size: 16.0,
                            ),
                            Text(currentAddress ?? "", style: appbarSubHeadingStyle),
                            Icon(
                              Icons.arrow_drop_down,
                              color: darkPrimaryColor,
                              size: 16.0,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  /*          flexibleSpace: FlexibleSpaceBar(
                    background: Container(
                      padding: EdgeInsets.all(fixPadding * 2),
                      alignment: Alignment.bottomCenter,
                      decoration: BoxDecoration(
                        color: primaryColor,
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              PageTransition(
                                  type: PageTransitionType.rightToLeft,
                                  child: Search()));
                        },
                        child: Container(
                          width: width,
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: darkPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(fixPadding),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.search,
                                  color: whiteColor,
                                  size: 20.0,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: fixPadding),
                                  child: Text(
                                    'Do you want find something?',
                                    style: searchTextStyle,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),*/
                  automaticallyImplyLeading: false,
                ),
              ];
            },
            body: SafeArea(
              child: Container(
                height: height,
                width: width,
                decoration: BoxDecoration(
                  borderRadius:
                  BorderRadius.vertical(top: Radius.circular(15.0)),
                  color: scaffoldBgColor,
                ),
                child: ListView(
                  physics: BouncingScrollPhysics(),
                  children: <Widget>[
                    // Image Slider List Start
                    Padding(
                      padding: EdgeInsets.all(fixPadding),
                      /*child:    ImageSliderList(),*/
                      child: Text(
                        'offers',
                        style: TextStyle(
                            fontWeight: FontWeight.w700,color: Colors.black,
                            fontSize: 16),
                      ),
                    ),
                    ImageSliderList(),
                    // Image Slider List End
                    heightSpace,

                    // Categories Start
                    Padding(
                      padding: EdgeInsets.all(fixPadding),
                      child: Text(
                        'Services',
                        style: TextStyle(
                            fontWeight: FontWeight.w700,color:Colors.black,
                            fontSize: 16),
                      ),
                    ),
                    CategoryList(),
                    // Categories End
                    heightSpace,
                    Padding(
                      padding: EdgeInsets.all(fixPadding),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Rewards',
                            style: TextStyle(
                                fontWeight: FontWeight.w700,color: Colors.black,
                                fontSize: 16),
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) => Voucher()));

                              // Navigator.push(context,(: HotSale()));
                            },
                            child: Text('View all', style: TextStyle(
                                fontWeight: FontWeight.w700,color: Colors.black,
                                fontSize: 16),),
                          ),
                        ],
                      ),
                    ),


                    //HotSale(),
                    Rewards(),
                    // Products Ordered End

                    heightSpace,


                    // Products Ordered Start
                    Padding(
                      padding: EdgeInsets.all(fixPadding),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Products',
                            style: TextStyle(
                                fontWeight: FontWeight.w700,color: Colors.black,
                                fontSize: 16),
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) => HotSale()));

                             // Navigator.push(context,(: HotSale()));
                            },
                            child: Text('View all', style: TextStyle(
                                fontWeight: FontWeight.w700,color: Colors.black,
                                fontSize: 16),),
                          ),
                        ],
                      ),
                    ),


                    //HotSale(),
                    product(),
                    // Products Ordered End
                    heightSpace,
              /*      Padding(
                      padding: EdgeInsets.all(fixPadding),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Rewards',
                            style: TextStyle(
                                fontWeight: FontWeight.w700,color: Colors.black,
                                fontSize: 16),
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) => Voucher()));

                              // Navigator.push(context,(: HotSale()));
                            },
                            child: Text('View all', style: TextStyle(
                                fontWeight: FontWeight.w700,color: Colors.black,
                                fontSize: 16),),
                          ),
                        ],
                      ),
                    ),*/


                    //HotSale(),
                   // Rewards(),
                    heightSpace,
                    heightSpace,
             /* Padding(
                child: Padding(
                  padding:
                  EdgeInsets.only(top: 6.0, left: 8.0, right: 8.0),
                  child: Image(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/banner-1.png'),
                  ),
                ),
              ),*/
                    // Products Ordered Start
                   /* Padding(
                      padding: EdgeInsets.all(fixPadding),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Favourite Car Wash Centers',
                            style: headingStyle,
                          ),
                          InkWell(
                            onTap: () {
                              // Navigator.push(context, PageTransition(type: PageTransitionType.downToUp, child: MoreList()));
                            },
                            child: Text('View all', style: moreStyle),
                          ),
                        ],
                      ),
                    ),
                    FavouriteRestaurantsList(),*/
                    // Products Ordered End

                    // Hot Sale Start
                   /* Padding(
                      padding: EdgeInsets.all(fixPadding),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Coupans',
                            style: headingStyle,
                          ),
                          InkWell(
                            onTap: () {
                              // Navigator.push(context, PageTransition(type: PageTransitionType.downToUp, child: MoreList()));
                            },
                            child: Text('View all', style: moreStyle),
                          ),
                        ],
                      ),
                    ),*/
                    // HotSale(),
                  //  ProductsOrdered(),
                    // Hot Sale End

                    heightSpace,
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  // Bottom Sheet for Address Starts Here
  void _addressBottomSheet(context, width) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Material(
            elevation: 7.0,
            child: Container(
              color: Colors.white,
              child: Wrap(
                children: <Widget>[
                  Container(
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: width,
                            padding: EdgeInsets.all(fixPadding),
                            alignment: Alignment.center,
                            child: Text(
                              'Select Address'.toUpperCase(),
                              style: headingStyle,
                            ),
                          ),
                          Divider(),
                          InkWell(
                            /*onTap: () {
                              setState(() {
                                currentAddress ?? "";
                                address1 = true;
                                address2 = false;
                              });
                              Navigator.pop(context);
                            },*/
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: 26.0,
                                  height: 26.0,
                                  decoration: BoxDecoration(
                                      color: (address1)
                                          ? primaryColor
                                          : darkPrimaryColor,
                                      borderRadius: BorderRadius.circular(13.0),
                                      border: Border.all(
                                          width: 1.0,
                                          color: greyColor.withOpacity(0.7))),
                                  child: Icon(Icons.check,
                                      color: whiteColor, size: 15.0),
                                ),
                                widthSpace,
                                /*Text(
                                  '76A, New York, US.',
                                  style: listItemTitleStyle,
                                ),*/
                              ],
                            ),
                          ),
                          heightSpace,
                          InkWell(
                          /*  onTap: () {
                              setState(() {
                                currentAddress = '55C, California, US.';
                                address1 = false;
                                address2 = true;
                              });
                              Navigator.pop(context);
                            },*/
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: 26.0,
                                  height: 26.0,
                                  decoration: BoxDecoration(
                                      color: (address2)
                                          ? primaryColor
                                          : whiteColor,
                                      borderRadius: BorderRadius.circular(13.0),
                                      border: Border.all(
                                          width: 1.0,
                                          color: greyColor.withOpacity(0.7))),
                                  child: Icon(Icons.check,
                                      color: whiteColor, size: 15.0),
                                ),
                                widthSpace,
                               /* Text(
                                  '55C, California, US.',
                                  style: listItemTitleStyle,
                                ),*/
                              ],
                            ),
                          ),
                          heightSpace,
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddNewAddress()));
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: 26.0,
                                  height: 26.0,
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.blue,
                                    size: 20.0,
                                  ),
                                ),
                                widthSpace,
                                Text(
                                  'Add New Address',
                                  style: TextStyle(
                                    fontSize: 15.0,
                                    color: Colors.blue,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          heightSpace,
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

}