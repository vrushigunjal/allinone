import 'package:all_in_one_app/carWasher/helper/user.dart';
import 'package:all_in_one_app/constant/constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Rating extends StatefulWidget {
  @override
  _RatingState   createState() => _RatingState();
}

class _RatingState extends State<Rating> {
  String username,centerName,image,phone,id,finalRating;
  bool ratingScreen = false;
  bool confirm = true;
  bool deliveryBoyReach = false;
  bool waiting = false;
  bool onTheWay = false;
  bool delivered = false;
  String time = '0:20';
  bool oneStar = true;
  bool twoStar = false;
  bool threeStar = false;
  bool fourStar = false;
  bool fiveStar = false;
  String ratingText = '1';
  bool enthusiastic = false;
  bool fast = false;
  bool friendly = false;


  @override
  void initState() {
    super.initState();
    getshared();

  }
  getshared() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      id = (prefs.getString('id') ?? '');
      username = (prefs.getString('FULLNAME') ?? '');
      phone = (prefs.getString('PHONENUMBER') ?? '');
      image = (prefs.getString('Image') ?? '');
      centerName = (prefs.getString('Centername') ?? '');

    });
  }




  changeRating(rating) {
    if (rating == '1') {
      setState(() {
        oneStar = true;
        twoStar = false;
        threeStar = false;
        fourStar = false;
        fiveStar = false;
        ratingText = '1';
      });
    }
    if (rating == '2') {
      setState(() {
        oneStar = false;
        twoStar = true;
        threeStar = false;
        fourStar = false;
        fiveStar = false;
        ratingText = '2';
      });
    }
    if (rating == '3') {
      setState(() {
        oneStar = false;
        twoStar = false;
        threeStar = true;
        fourStar = false;
        fiveStar = false;
        ratingText = '3';
      });
    }
    if (rating == '4') {
      setState(() {
        oneStar = false;
        twoStar = false;
        threeStar = false;
        fourStar = true;
        fiveStar = false;
        ratingText = '4';
      });
    }
    if (rating == '5') {
      setState(() {
        oneStar = false;
        twoStar = false;
        threeStar = false;
        fourStar = false;
        fiveStar = true;
        ratingText = '5';
      });
    }

finalRating = ratingText.toString();
  }


  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(

      child:
        (!ratingScreen) ? rating() : ratingScreenData(),
      );

  }


  ratingScreenData() {
    double width = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.arrow_back, color: blackColor),
          onPressed: () {
            setState(() {
              ratingScreen = true;
            });
          },
        ),
        Container(
          width: width - (fixPadding * 2),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text('AWESOME!', style: headingStyle),
              heightSpace,
              heightSpace,
              Text('You rated Devin $ratingText Stars',
                  style: listItemSubTitleStyle),
              heightSpace,
              heightSpace,
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  //1
                  InkWell(
                    onTap: () {
                      changeRating('1');
                    },
                    child: Icon(
                      Icons.star,
                      size: 35.0,
                      color: ((oneStar == true) ||
                          (twoStar == true) ||
                          (threeStar == true) ||
                          (fourStar == true) ||
                          (fiveStar == true))
                          ? Colors.orange
                          : Colors.grey.withOpacity(0.6),
                    ),
                  ),
                  widthSpace,
                  // 2
                  InkWell(
                    onTap: () {
                      changeRating('2');
                    },
                    child: Icon(
                      Icons.star,
                      size: 35.0,
                      color: ((twoStar == true) ||
                          (threeStar == true) ||
                          (fourStar == true) ||
                          (fiveStar == true))
                          ? Colors.orange
                          : Colors.grey.withOpacity(0.6),
                    ),
                  ),
                  widthSpace,
                  // 3
                  InkWell(
                    onTap: () {
                      changeRating('3');
                    },
                    child: Icon(
                      Icons.star,
                      size: 35.0,
                      color: ((threeStar == true) ||
                          (fourStar == true) ||
                          (fiveStar == true))
                          ? Colors.orange
                          : Colors.grey.withOpacity(0.6),
                    ),
                  ),
                  widthSpace,
                  // 4
                  InkWell(
                    onTap: () {
                      changeRating('4');
                    },
                    child: Icon(
                      Icons.star,
                      size: 35.0,
                      color: ((fourStar == true) || (fiveStar == true))
                          ? Colors.orange
                          : Colors.grey.withOpacity(0.6),
                    ),
                  ),
                  widthSpace,
                  // 5
                  InkWell(
                    onTap: () {
                      changeRating('5');
                    },
                    child: Icon(
                      Icons.star,
                      size: 35.0,
                      color: ((fiveStar == true))
                          ? Colors.orange
                          : Colors.grey.withOpacity(0.6),
                    ),
                  ),
                ],
              ),
              Divider(),
              heightSpace,

              // Complete Button
              Container(
                padding: EdgeInsets.all(fixPadding),
                child: SizedBox(
                  height: 50.0,
                  width: width - (fixPadding * 2.0),
                  child: RaisedButton(
                    elevation: 0.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    onPressed: () {

                      AddRating(id,finalRating,username,phone,image,centerName);
                      Navigator.pop(context);
                    },
                    color: primaryColor,
                    child: Text(
                      'Complete',
                      style: wbuttonWhiteTextStyle,
                    ),
                  ),
                ),
              ),
              // Complete Button
            ],
          ),
        ),
      ],
    );
  }

  rating() {
    return Padding(
      padding: EdgeInsets.all(fixPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            'How is your Experiance?'.toUpperCase(),
            style: headingStyle,
          ),
          heightSpace,
          Text(
            'Your feedback will help us improve\ndelivery experience better.',
            textAlign: TextAlign.center,
            style: listItemSubTitleStyle,
          ),
          heightSpace,
          heightSpace,
          InkWell(
            onTap: () {
              setState(() {
                ratingScreen = true;
              });
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.star,
                    size: 35.0, color: Colors.grey.withOpacity(0.6)),
                widthSpace,
                Icon(Icons.star,
                    size: 35.0, color: Colors.grey.withOpacity(0.6)),
                widthSpace,
                Icon(Icons.star,
                    size: 35.0, color: Colors.grey.withOpacity(0.6)),
                widthSpace,
                Icon(Icons.star,
                    size: 35.0, color: Colors.grey.withOpacity(0.6)),
                widthSpace,
                Icon(Icons.star,
                    size: 35.0, color: Colors.grey.withOpacity(0.6)),
              ],
            ),
          ),
        ],
      ),
    );
  }

}