import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseFirestore firestore = FirebaseFirestore.instance;
final FirebaseAuth auth = FirebaseAuth.instance;
final User user = auth.currentUser;


/*Future<String> uploadProductImage(String imageName, File image) async {
  final StorageReference storageReference =
  FirebaseStorage().ref().child("product_images").child(imageName);

  final StorageUploadTask uploadTask = storageReference.putFile(image);

  StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
  return await storageTaskSnapshot.ref.getDownloadURL();
}*/

Future<dynamic> addOrder(

    String phone,
    String image,
    String name,
    String adress,
    String productName,
    String desription,
    double price,
    int shipcharge,
    double result,
    String applycoupan,
    String note,
    String order) async {
  return await firestore.collection("orders").add({
    "uid": user.uid,
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),

    "Phone": phone,
    "Image": image,
    "Name": name,
    "Address": adress,
    "productName": productName,
    "description": desription,
    "Price": price,
    "shipCharge": shipcharge,
    "result": result,
    "applyCoupon": applycoupan,
    "note": note,
    "order": order,
  });
}

Future<dynamic> addcartOrder(
    String phone,
    String image,
    String name,
    String status) async {
  return await firestore.collection("orders").add({
    "uid": user.uid,
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "Phone": phone,
    "Image": image,
    "Name": name,
    "status": status,

  });
}

Future<QuerySnapshot> getAllOrdersOfUser() async {
  return await firestore
      .collection("orders")
      .where("uid", isEqualTo: user.uid)
      .get();
}


/*Future<QuerySnapshot> getAllOrdersOfUser() async {
  return await firestore
      .collection("orders")
      .where("uid", isEqualTo: user.uid)
      .orderBy("created_at", descending: true)
      .get();
}*/

Future<DocumentSnapshot> getUserData() {
  return firestore.collection("usersRegistration").doc(user.uid).get();
}


/*Future<QuerySnapshot> getprchaseOrder(String orderStatus) async {
  return await firestore
      .collection("orders")
      .where("order", isEqualTo: orderStatus)
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}*/

/*Future<QuerySnapshot> getAllOrdersOfUser() async {
  return await firestore
      .collection("orders")
      .orderBy("created_at", descending: true)
      .get();
}*/
Future<QuerySnapshot> getallOrder() async {
  return await firestore
      .collection("orders")
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}

Future<QuerySnapshot> getprchaseOrder(String orderStatus) async {
  return await firestore
      .collection("orders")
      .orderBy("created_at", descending: true)
      .get();
}
Future<QuerySnapshot> getMoreprchaseOrder(
    String orderStatus, DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("orders")
      .where("order", isEqualTo: orderStatus)
      .startAfterDocument(lastDocument)
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}

Future<void> updateOrderStatus(String id, String status) async {
  if (status == "delivered") {
    return await firestore.collection("orders").doc(id).update({
     /* "paymentStatus": "paid",*/
      "orderStatus": status,
    });
  } else {
    return await firestore.collection("orders").doc(id).update({
      "orderStatus": status,
    });
  }
}

/*
Future<QuerySnapshot> getMoreprchaseOrder(DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("orders")
      .orderBy("created_at", descending: true)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}
*/

Future<QuerySnapshot> searchOrder(String searchValue) async {
  return await firestore
      .collection("orders")
      .where("productName", isGreaterThanOrEqualTo: searchValue)
      .where("productName", isLessThanOrEqualTo: searchValue + '\uf8ff')
      .limit(20)
      .get();
}

