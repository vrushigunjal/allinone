import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';

final FirebaseAuth auth = FirebaseAuth.instance;
final FirebaseFirestore firestore = FirebaseFirestore.instance;

final User user = auth.currentUser;




Future<dynamic> addfavouriteList(
    String name,
    String status,
    String image,
    String userphone,
    String adrress,
    String phone,
    ) async {
  return await firestore.collection("favourite").add({
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "name": name,
    "status": status,
    "image": image,
    "userphone": userphone,
    "adrress": adrress,
    "phone": phone,

  });
}


Future<DocumentSnapshot> getaddfavouriteList() {
  return firestore.collection("favourite").doc(user.uid).get();
}

Future<QuerySnapshot> getfavouriteLists() async {
  return await firestore
      .collection("favourite")
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}

/*Future<QuerySnapshot> getMoreUser(DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("users")
      .orderBy("created_at", descending: true)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}*/

/*
Future<DocumentSnapshot> getUserDataById(String uid) {
  return firestore.collection("users").doc(uid).get();
}

Future<QuerySnapshot> searchUser(String searchValue, String searchBy) async {
  return await firestore
      .collection("users")
      .where(searchBy, isEqualTo: searchValue)
      .limit(1)
      .get();
}
*/
