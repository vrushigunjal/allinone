import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';

final FirebaseAuth auth = FirebaseAuth.instance;
final FirebaseFirestore firestore = FirebaseFirestore.instance;

final User user = auth.currentUser;

Future<String> uploadProductImage(String imageName, File image) async {
  final StorageReference storageReference =
  FirebaseStorage().ref().child("user_images").child(imageName);

  final StorageUploadTask uploadTask = storageReference.putFile(image);

  StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
  return await storageTaskSnapshot.ref.getDownloadURL();
}

Future<String>editPhoneNumber( String userImage,String randomnumber,String name,String userphone,String dateofBirth, String email,String adrress, String agentType,bool userRegister) async{
  firestore.collection("usersRegistration").doc(user.uid).set({
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "userImage": userImage,
    "randomnumber": randomnumber,
    "name": name,
    "PhoneNumber": userphone,
    "dateOfBirth": dateofBirth,
    "email": email,
    "adreess": adrress,
    "uid": user.uid,
    "UserType": agentType,
    "isUser":userRegister
  }, SetOptions(merge: true));
  return user != null ? user.uid: null;
}


editAddress(String homeAddress, String officeAddress) {
  return firestore.collection("usersRegistration").doc(user.uid).set(
      {"homeAddress": homeAddress, "officeAddress": officeAddress},
      SetOptions(merge: true));
}
Future<void> updateUser(String docid, String name,
    String dateofBirth, String email, String adrress, String carno,String cartype) async {
  return await firestore.collection("usersRegistration").doc(docid).update({
    "name": name,
    "dateOfBirth": dateofBirth, "email": email, "adreess": adrress, "carNo": carno,"cartype": cartype,
  });
}
Future<void> updateuserUser(String docid, String image,String name,String adrress,String phone, String email,String dateofBirth) async {
  return await firestore.collection("usersRegistration").doc(docid).update({
    "userImage": image,
    "name": name,
    "adreess": adrress,
    "PhoneNumber": phone,
    "email": email,
    "dateOfBirth": dateofBirth,

  });
}

Future<void> updaterandomnumber(
    String docid, int randomnumber) async {
  print(docid);
  return await firestore
      .collection("usersRegistration").doc(docid).update(
      {
        "Favourite": randomnumber,
      }
  );
}
Future<DocumentSnapshot> getUser() {
  return firestore.collection("usersRegistration").doc(user.uid).get();
}
Future<DocumentSnapshot> getUserr( String Uid) {
  return firestore.collection("usersRegistration").doc(user.uid).get();
}
Future<DocumentSnapshot> getUserDataById(String uid) {
  return firestore.collection("usersRegistration").doc(uid).get();
}
Future<DocumentSnapshot> getUserDatadata() {
  return firestore.collection("usersRegistration").doc(user.uid).get();
}
Future<QuerySnapshot> getUserData() async {
  return await firestore
      .collection("usersRegistration")
      .limit(20)
      .get();
}

Future<QuerySnapshot> getMoreUser(DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("usersRegistration")
      .orderBy("created_at", descending: true)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}



Future<QuerySnapshot> searchUser(String searchValue, String searchBy) async {
  print(searchValue);
  return await firestore
      .collection("usersRegistration")
      .where(searchBy, isEqualTo: searchValue)
      .get();
}
