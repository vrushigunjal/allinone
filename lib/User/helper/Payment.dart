import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseFirestore firestore = FirebaseFirestore.instance;
final FirebaseAuth auth = FirebaseAuth.instance;
final User user = auth.currentUser;


/*Future<String> uploadProductImage(String imageName, File image) async {
  final StorageReference storageReference =
  FirebaseStorage().ref().child("product_images").child(imageName);

  final StorageUploadTask uploadTask = storageReference.putFile(image);

  StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
  return await storageTaskSnapshot.ref.getDownloadURL();
}*/

Future<dynamic> addPayment(
    String phone,
    String name,
    String adress,
    String productName,
    double _Washerprice,
    double result,
    String order,
    String centerName
    ) async {
  return await firestore.collection("payments").add({
    "uid": user.uid,
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "Phone": phone,
    "Name": name,
    "Address": adress,
    "productName": productName,
    "result": result,
    "_Washerprice": _Washerprice,
    "order": order,
    "centerName": centerName,
  });
}



Future<QuerySnapshot> getAllpaymentOfUser() async {
  return await firestore
      .collection("payments")
      .where("uid", isEqualTo: user.uid)
      .get();
}

/*Future<QuerySnapshot> getAllOrdersOfUser() async {
  return await firestore
      .collection("orders")
      .where("uid", isEqualTo: user.uid)
      .orderBy("created_at", descending: true)
      .get();
}*/




/*Future<QuerySnapshot> getprchaseOrder(String orderStatus) async {
  return await firestore
      .collection("orders")
      .where("order", isEqualTo: orderStatus)
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}*/

/*Future<QuerySnapshot> getAllOrdersOfUser() async {
  return await firestore
      .collection("orders")
      .orderBy("created_at", descending: true)
      .get();
}*/
Future<QuerySnapshot> getallPayment() async {
  return await firestore
      .collection("payments")
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}

Future<QuerySnapshot> getprchasePayment(String orderStatus) async {
  return await firestore
      .collection("payments")
      .orderBy("created_at", descending: true)
      .get();
}
Future<QuerySnapshot> getMoreprchasePayment(
     DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("payments")
      .startAfterDocument(lastDocument)
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}

Future<void> updatePaymentStatus(String id, String status) async {
  if (status == "delivered") {
    return await firestore.collection("payments").doc(id).update({
      /* "paymentStatus": "paid",*/
      "order": status,
    });
  } else {
    return await firestore.collection("payments").doc(id).update({
      "order": status,
    });
  }
}

/*
Future<QuerySnapshot> getMoreprchaseOrder(DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("orders")
      .orderBy("created_at", descending: true)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}
*/

Future<QuerySnapshot> searchOrder(String searchValue) async {
  return await firestore
      .collection("orders")
      .where("productName", isGreaterThanOrEqualTo: searchValue)
      .where("productName", isLessThanOrEqualTo: searchValue + '\uf8ff')
      .limit(20)
      .get();
}

