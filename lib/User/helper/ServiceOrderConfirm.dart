import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseFirestore firestore = FirebaseFirestore.instance;
final FirebaseAuth auth = FirebaseAuth.instance;
final User user = auth.currentUser;


/*Future<String> uploadProductImage(String imageName, File image) async {
  final StorageReference storageReference =
  FirebaseStorage().ref().child("product_images").child(imageName);

  final StorageUploadTask uploadTask = storageReference.putFile(image);

  StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
  return await storageTaskSnapshot.ref.getDownloadURL();
}*/

Future<dynamic> addServiceOrder(
    String phone,
    String image,
    String name,
    String adress,
    String productName,
    String desription,
    double price,
    double  _Washerprice,
    int shipcharge,
    double result,
    String applycoupan,
    String note,
    String categories,
    String order,
    String WasharPaymentStatus,
    String Uid) async {
  return await firestore.collection("ordersService").add({
    "uid": user.uid,
    "created_at": FieldValue.serverTimestamp(),
    "updated_at": FieldValue.serverTimestamp(),
    "Phone": phone,
    "Image": image,
    "Name": name,
    "Address": adress,
    "productName": productName,
    "description": desription,
    "Price": price,
    "Washerprice":  _Washerprice,
    "shipCharge": shipcharge,
    "result": result,
    "applyCoupon": applycoupan,
    "Carno": note,
    "CarType": categories,
    "order": order,
    "WasharPaymentStatus": WasharPaymentStatus,
    "CenterUid": Uid,
  });
}

Future<QuerySnapshot> getAllServiceOrdersOfUser() async {
  return await firestore
      .collection("ordersService")
      .where("uid", isEqualTo: user.uid)
      .get();
}


/*Future<QuerySnapshot> getAllOrdersOfUser() async {
  return await firestore
      .collection("orders")
      .where("uid", isEqualTo: user.uid)
      .orderBy("created_at", descending: true)
      .get();
}*/

Future<DocumentSnapshot> getUserData() {
  return firestore.collection("ordersService").doc(user.uid).get();
}


/*Future<QuerySnapshot> getprchaseOrder(String orderStatus) async {
  return await firestore
      .collection("orders")
      .where("order", isEqualTo: orderStatus)
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}*/

/*Future<QuerySnapshot> getAllOrdersOfUser() async {
  return await firestore
      .collection("orders")
      .orderBy("created_at", descending: true)
      .get();
}*/
Future<QuerySnapshot> getallServiceOrder() async {
  return await firestore
      .collection("ordersService")
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}
Future<QuerySnapshot> getallServiceOrderforpayment(String id) async {
  return await firestore
      .collection("ordersService")
     .where("CenterUid", isEqualTo: id)
      .get();
}

Future<QuerySnapshot> getprchaseServiceOrder(String orderStatus) async {
  return await firestore
      .collection("ordersService")
      .where("order", isEqualTo: orderStatus)
      .orderBy("created_at", descending: true)
      .get();
}
Future<QuerySnapshot> getMoreprchaseServiceOrder(
    String orderStatus, DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("ordersService")
      .where("order", isEqualTo: orderStatus)
      .startAfterDocument(lastDocument)
      .orderBy("created_at", descending: true)
      .limit(20)
      .get();
}

Future<void> updatereasonServiceOrderStatus(String id, String status,String reson) async {

    return await firestore.collection("ordersService").doc(id).update({
      "order": status,
      "note": reson,
    });

}
Future<void> updatereasonWasherPayment(String id, String status) async {

  return await firestore.collection("ordersService").doc(id).update({
    "WasharPaymentStatus": status,

  });

}


Future<void> updateServiceOrderStatus(String id, String status) async {

  return await firestore.collection("ordersService").doc(id).update({
    "order": status,
  });

}
Future<void> updateServiceOrder(String id, String status) async {

  return await firestore.collection("ordersService").doc(id).update({
    "order": status,
  });

}

/*
Future<QuerySnapshot> getMoreprchaseOrder(DocumentSnapshot lastDocument) async {
  return await firestore
      .collection("orders")
      .orderBy("created_at", descending: true)
      .startAfterDocument(lastDocument)
      .limit(20)
      .get();
}
*/

Future<QuerySnapshot> searchServiceOrder(String searchValue) async {
  return await firestore
      .collection("ordersService")
      .where("productName", isGreaterThanOrEqualTo: searchValue)
      .where("productName", isLessThanOrEqualTo: searchValue + '\uf8ff')
      .limit(20)
      .get();
}

